# Gowee

A social network Flutter mobile application mainly focused on travelers and traveling, named Gowee,
developed by a group of students for a university capstone project.

## Project Description
Gowee is a traveling-focused social network on mobile application who has a love for backpacking tourism or wants to have a new
experience with trekking and adventure travel.  
This application will help users can find and connect to people with similar interests, make a team, and travel together.

![screenshot_home_page.png](/../homepage/goweefrontend/assets/images/screenshot_home_page.png) ![screenshot_map_screen.png](/../homepage/goweefrontend/assets/images/screenshot_map_screen.png)
## Technology used

* Flutter: UI for the users (This repository)
* NodeJS: Web Server, REST API ([Bitbucket](https://bitbucket.org/capstone-p1/alice/src/homepage/backend/))
* SocketIO: Real-time Web Server
* PostgreSQL: Database
* Google Maps API: Geolocation, Locations, etc.

![software_architecture_diagram.png](/../homepage/goweefrontend/assets/images/software_architecture_diagram.png)

## Features

#### Home Page showing journey posts with photos, number of likes, comments and details of the journey
  * Sort/Filter journeys
  * Search for journeys
#### Sign up / Login or Login using Google account
  * Like journeys
  * Comment on journeys
  * Join journeys
#### Create journeys
  * Upload images from phone library
  * Add milestones for journeys
#### User Profile Pages
  * Follow / Unfollow Users
  * Edit/Add more milestones to existing journeys
  * Accept/Reject join requests
#### Map Screen
  * View journey details
  * Search for location
  * View other travelers locations in the same group
  * Send SOS signal to other travelers

## Future Work
- [ ] Notificaitons for likes, comments, follows and join requests.
- [ ] Direct Messaging
- [ ] Improve Performance

# Demo

### Apk Release

[Download the release apk for Gowee application](https://github.com/danh1215/gowee/blob/master/app-release.apk).


# Credits

#### Nguyen Phuoc Cuong
  * Role: Backend Developer
  * Contribution: Database, NodeJS server
  * Social:

#### Truong Phu Cuong
  * Role: Frontend Developer
  * Contribution: HomePage,Detail Screen, Login/Signup, Map Screen
  * Social:

#### Le Ngoc Danh
  * Role: Frontend Developer
  * Contribution: Create/Edit Journeys,Map Screen, SocketIO server
  * Social:

#### Tran Cong Phuc
  * Role: Developer
  * Contribution: Sample Data, User Profile
  * Social:



