// import 'dart:html';
//
// import 'package:flutter/material.dart';
// import 'dart:convert';
//
// import 'package:goweefrontend/Model/Journey.dart';
// import 'package:goweefrontend/Model/LoggedUser.dart';
// import 'package:goweefrontend/Model/Place.dart';
// import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
// import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
// import 'package:http/http.dart' as http;
// import '../../Constants.dart';
//
// class TestMap extends StatefulWidget{
//   @override
//   _TestMapState createState() => new _TestMapState();
// }
// class _TestMapState extends State<TestMap>{
//   List<Location> locations = [];
//   TextEditingController _addressController = TextEditingController();
//   List<Journey> journeyList = [];
//   List<Place> placeList = [];
//
//   Future loadUser() async {
//     String jsonString = await storage.read(key: "jwt");
//     final jsonResponse = json.decode(jsonString);
//     loggedUser = new LoggedUser.fromJson(jsonResponse);
//     print(loggedUser.token);
//   }
//   addPlace(){
//     if(journeyList.isNotEmpty){
//       for(int i=0; i<journeyList.length;i++){
//         for(int y=0;y<journeyList[i].places.length;y++){
//           setState(() {
//             placeList.add(journeyList[i].places[y]);
//           });
//         }
//       }
//     }
//     print(placeList.length);
//   }
//   getJourneyByUserId() async {
//     var res = await http.get(
//       Uri.parse("$baseUrl/journeys/userid=${loggedUser.user.userId}"),
//       headers: {
//         'Content_Type': 'application/json; charset=UTF-8',
//         'Authorization': 'Bearer ${loggedUser.token}',
//       },
//     );
//     if (res.statusCode == 200) {
//       print("Get journeys successfully");
//     }
//     List data = jsonDecode(res.body);
//     List idList = [];
//     for (var i in data) {
//       idList.add(i["journeyId"]);
//     }
//     for (var i in idList) {
//       var res = await http.get(
//         Uri.parse("$baseUrl/journeys/$i"),
//       );
//       var data = jsonDecode(res.body);
//       Journey userJourney = new Journey.fromJson(data);
//       setState(() {
//         journeyList.add(userJourney);
//       });
//     }
//     print(journeyList.length);
//     addPlace();
//   }
//
//   addLocation(address) async{
//     List<Location> result = await locationFromAddress(address);
//     if(result.isNotEmpty){
//       for(var i in result){
//         setState(() {
//           locations.add(i);
//         });
//       }
//       print(locations[0].latitude);
//       print(locations[0].longitude);
//     }
//   }
//   showLatLong(){
//     setState(() {
//
//     });
//     if(locations.isNotEmpty){
//       return Text("Longtitude: ${locations[0].longitude} - Latitude: ${locations[0].latitude}");
//     }else{
//       return Text("Nothing to show");
//     }
//   }
//   @override
//   void initState() {
//     super.initState();
//     loadUser();
//     getJourneyByUserId();
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text("Map")),
//       body: SingleChildScrollView(
//         child: Padding(
//           padding: EdgeInsets.fromLTRB(5, 5, 0, 10),
//           child: Column(
//             children: [
//               Text("Enter address to find coordinate"),
//               TextFormField(
//                 controller: _addressController,
//                 style: TextStyle(fontSize: 15),
//               ),
//               TextButton(onPressed: (){
//                 addLocation(_addressController.text);
//               }, child: Text("Submit")),
//               Text("Location Coordinate"),
//               showLatLong()
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }