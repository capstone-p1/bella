import 'package:flutter/material.dart';
import 'package:goweefrontend/View/Map/SocketUITestInside.dart';


class SocketUITest extends StatefulWidget {
  @override
  _SocketUITestState createState() => _SocketUITestState();
}

class _SocketUITestState extends State<SocketUITest> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Socket UI Testing"
          ),
        ),
      body: Container(
        child: Center(
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: TextButton(
                child: Text("Debug"),
                onPressed: ()=>{
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => SocketUITestInside()))
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
                    foregroundColor: MaterialStateProperty.all<Color>(Colors.black)
                ),
              ),
            )
        ),
      ) ,
    );
  }
}
