import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import 'package:flutter/material.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';


import'dart:io' show Platform;

Random random = new Random();

class SocketUITestInside extends StatefulWidget {
  @override
  _SocketUITestInsideState createState() => _SocketUITestInsideState();
}

class MySosStream{
  final IO.Socket socket;
  StreamController sosMessageController = new StreamController<String>();
  Stream get sosMessageStream => sosMessageController.stream;

  MySosStream(this.socket){
    socket.connect();

    socket.on("sos receiver", (data){
      refreshClientSos(data);
      print("SOS id: $data");
    });

  }

  void refreshClientSos(String data) {
    data = '$data';
    sosMessageController.sink.add(data);
  }

  void dispose() {

  }



}
class MyStream {
  final IO.Socket socket;
  String data = '';
  StreamController clientLocationController = new StreamController<String>();
  Stream get clientLocationStream => clientLocationController.stream;


  StreamController sosMessageController = new StreamController<String>();
  Stream get sosMessageStream => sosMessageController.stream;


  MyStream(this.socket){
    socket.connect();

    socket.on("user return", (data) {
      refreshClientLocation(data);
      print("Received data from user return event: $data");
    });

    socket.on("sos receiver", (data){
      refreshClientSos(data);
      print("SOS 2 id: $data");
    });


    socket.on("user close stream", (_){
      print("Event user close stream received");
      disposeClientLocationSink();
    });
  }

  void refreshClientSos(String data) {
    data = '$data';
    sosMessageController.sink.add(data);
  }


  void refreshClientLocation(String data) {
    data = '$data';
    clientLocationController.sink.add(data);
  }

  void disposeClientLocationSink() {
    clientLocationController.close();
  }
}

class _SocketUITestInsideState extends State<SocketUITestInside> {

  IO.Socket socket;

  String testingStuff = '';

  String position = '';

  List<String> positions = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    createSocketConnection();
  }



  void createSocketConnection() {
    //127.0.0.1
    socket = IO.io(socketUrl, <String, dynamic>{
      'transports': ['websocket'],
    });

    this.socket.on("connect", (_){
      print('Connected, ID ${socket.id}');
      setState(() {
        testingStuff = 'Client with ID ${socket.id} has connected';
      });
    });

    this.socket.on("disconnect", (_){
      print('Disconnected');
    });
    myStream = new MyStream(socket);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    this.socket.dispose();
    print("dispose on override dispose");
    super.dispose();
  }

  MyStream myStream;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
              "Socket UI Testing Inside"
          ),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: Colors.white),
            onPressed: (){
              this.socket.dispose();
              print("dispose on back button");
              Navigator.of(context).pop();
            },
          )
      ),
      body: Container(
        child: Center(
          child: Column(
              children: [
                Text('${testingStuff ?? 'No connection'}')
                ,
                Text('$position')
                ,
                StreamBuilder(
                    stream: myStream.clientLocationStream,

                    builder: (context, snapshot) => Text(
                      snapshot.hasData ? snapshot.data.toString() : "0",
                    ) )
                ,
                TextButton(
                  child: Text("Debug"),
                  onPressed: (){
                    double randomLat = -90 + random.nextDouble() * 90 * 2;
                    double randomLng = -180 + random.nextDouble() * 180 * 2;
                    String str = '{"socketID" : "${socket.id}","lat": $randomLat, "lng": $randomLng}';
                    socket.emit("user", str);
                  },
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
                      foregroundColor: MaterialStateProperty.all<Color>(Colors.black)
                  ),
                )
              ]
          ),
        ),
      ) ,
    );
  }
}
