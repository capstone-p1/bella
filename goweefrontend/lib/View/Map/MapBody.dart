import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;
import 'dart:math';
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'dart:ui';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:goweefrontend/View/AddJourney/GoogleMapApi.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../../Constants.dart';
import 'SocketUITestInside.dart';

class MapBody extends StatefulWidget {
  const MapBody(
      {Key key,
        @required this.panelHeightClosed,
        @required this.journey,
        })
      : super(key: key);

  final double panelHeightClosed;
  final Journey journey;

  @override
  _MapBodyState createState() => _MapBodyState();

  static void disConnect(BuildContext context) {
    context.findAncestorStateOfType<_MapBodyState>().dispose();
  }
}


class _MapBodyState extends State<MapBody> with SingleTickerProviderStateMixin {
  /// Testing
  IO.Socket socket;
  MyStream myStream;
  MySosStream mySosStream;
  String testingStuff = '';
  Random random = new Random();
  String position = '';
  List<String> positions = [];
  final Set<Marker> markers = new Set();
  String milestoneDropdownValue;
  String placeDropdownValue;
  List<String> milestoneList = [];
  List<String> placesList = [];
  double km = 0;
  bool navbarVisible = true;
  Set<Polyline> _polyline = Set<Polyline>();
  List<LatLng> polylineCoordinates = [];
  LatLng currentLocation;
  LatLng destinationLocation;
  List<LatLng> latlng = List();


  ///emit user dispose here
  @override
  void dispose() {
    // TODO: implement dispose
    //this.socket.dispose();
    this.socket.emit("user dispose",this.socket.id);
    print("dispose on override dispose");
    super.dispose();
  }

  void createSocketConnection() {
    //127.0.0.1
    socket = IO.io(socketUrl, <String, dynamic>{
      'transports': ['websocket'],
    });

    this.socket.on("connect", (_) {
      print('Connected, ID ${socket.id}');
      setState(() {
        testingStuff = 'Client with ID ${socket.id} has connected';
      });
    });


    this.socket.on("disconnect", (_) {
      this.socket.dispose();
      print('Disconnected');
    });

    myStream = new MyStream(socket);
    mySosStream = new MySosStream(socket);

  }

  Widget _UserMarker() {
    setState(() async {
      latlng = [];
      double randomLat = 10.745463 + random.nextInt(100) * 0.01;
      double randomLng = 106.699082 + random.nextInt(100) * 0.01;
      final http.Response response =
          await http.get(Uri.parse(loggedUser.user.userAvatar.imageLink));
      final Uint8List markerIcon = await getBytesFromCanvasUser(
          // Size(100.0, 210.0),
          Size(150.0,150.0),
          response.bodyBytes);
      socket.emit('user','{"socketID" : "${socket.id}","lat": $randomLat, "lng": $randomLng, "userId": ${loggedUser.user.userId}}');
      markers.add(Marker(
          markerId: MarkerId("${socket.id}"),
          infoWindow: const InfoWindow(title: "Origin"),
          icon:
          BitmapDescriptor.fromBytes(markerIcon),
          position: LatLng(randomLat, randomLng))) ;
      _goToTheUser(randomLat, randomLng);
      print("$randomLat,$randomLng");
      latlng.add(LatLng(randomLat, randomLng));
    });
  }

  Future<void> _goToTheUser(uLat, uLng) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(uLat, uLng), zoom: 10)));
    setState(() {});
  }

  /// End Test

  final Set<Marker> listUserMarkers = {};
  AnimationController controller;
  Animation _animation;
  int index = 0;
  bool sosClient = false;
  bool sosOn = false;

  /// Caculate Distane

  double calculateDistane(List<LatLng> latlng) {
    double totalDistance = 0;
    for (int i = 0; i < latlng.length; i++) {
      if (i < latlng.length - 1) {
        // skip the last index
        totalDistance += getStraightLineDistance(latlng[i + 1].latitude,
            latlng[i + 1].longitude, latlng[i].latitude, latlng[i].longitude);
      }
    }
    // print(totalDistance.toString());
    return km = totalDistance;
  }

  double getStraightLineDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);
    var dLon = deg2rad(lon2 - lon1);
    var a = math.sin(dLat / 2) * math.sin(dLat / 2) +
        math.cos(deg2rad(lat1)) *
            math.cos(deg2rad(lat2)) *
            math.sin(dLon / 2) *
            math.sin(dLon / 2);
    var c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d; //in m
  }

  dynamic deg2rad(deg) {
    return deg * (math.pi / 180);
  }

  void _changed(bool visibility) {
    setState(() {
      sosOn = !sosOn;
    });
  }

  void _changedSoS(bool visibility){
    sosClient =! sosClient;
  }

  double _fabHeight = 100.0;
  Completer<GoogleMapController> _controller = Completer();
  Marker _origin;
  LatLng _target = LatLng(
    10.85636,
    106.6543,
  );

  /// Function move to user location

  // static final CameraPosition _myLocation = CameraPosition(
  //     // bearing: 192.8334901395799,
  //     target: LatLng(10.745463, 106.699082),
  //     // tilt: 59.440717697143555,
  //     zoom: 15);
  static final CameraPosition _myLocation = CameraPosition(
      // bearing: 192.8334901395799,
      target: LatLng(10.745463, 106.699082),
      // tilt: 59.440717697143555,
      zoom: 10);




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    addMilestonesToList();

    createSocketConnection();
    GoogleMapApi.locationController.stream.listen(
          (place) async {
        GoogleMapController mapController = await _controller.future;
        mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: LatLng(
                place.lat,
                place.lng,
              ),
              zoom: 15.0,
            ),
          ),
        );
      },
    );

  }

  Future<Uint8List> getBytesFromCanvasUser(Size size, Uint8List dataBytes) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final Radius radius = Radius.circular(size.width / 2);

    final Paint tagPaint = Paint()..color = Colors.blue;
    final double tagWidth = 40.0;
    final Paint shadowPaint = Paint()
      ..color = sosOn ? Colors.red : Colors.blue.withAlpha(100);
    final double shadowWidth = 15.0;

    final Paint borderPaint = Paint()..color = Colors.white;
    final double borderWidth = 3.0;

    final double imageOffset = shadowWidth + borderWidth;

    /// border Image


    // Add shadow circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              0.0,
              0.0,
              size.width,
              size.height),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        shadowPaint);

    // Add border circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              shadowWidth,
              shadowWidth,
              size.width - (shadowWidth * 2),
              size.height - (shadowWidth * 2)),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        borderPaint);


    // Oval for the image
    Rect oval = Rect.fromLTWH(
        imageOffset,
        imageOffset,
        size.width - (imageOffset * 2),
        size.height - (imageOffset * 2));


    // Add path for oval image
    canvas.clipPath(Path()..addOval(oval));

    /// Add image avatar
    ui.Image imaged = await loadImage(dataBytes.buffer.asUint8List());
    paintImage(canvas: canvas, image: imaged, rect: oval, fit: BoxFit.fitWidth);

    final img = await pictureRecorder
        .endRecording()
        .toImage(size.width.toInt(), size.height.toInt());
    final data = await img.toByteData(format: ui.ImageByteFormat.png);
    return data.buffer.asUint8List();
  }

  Future<Uint8List> getBytesFromCanvasClient(Size size, Uint8List dataBytes) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final Radius radius = Radius.circular(size.width / 2);

    final Paint tagPaint = Paint()..color = Colors.blue;
    final double tagWidth = 40.0;
    final Paint shadowPaint = Paint()
      ..color = Colors.blue.withAlpha(100);
    final double shadowWidth = 15.0;

    final Paint borderPaint = Paint()..color = Colors.white;
    final double borderWidth = 3.0;

    final double imageOffset = shadowWidth + borderWidth;

    // Add shadow circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              0.0,
              0.0,
              size.width,
              size.height),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        shadowPaint);

    // Add border circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              shadowWidth,
              shadowWidth,
              size.width - (shadowWidth * 2),
              size.height - (shadowWidth * 2)),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        borderPaint);

    // Oval for the image
    Rect oval = Rect.fromLTWH(
        imageOffset,
        imageOffset,
        size.width - (imageOffset * 2),
        size.height - (imageOffset * 2));

    // Add path for oval image
    canvas.clipPath(Path()..addOval(oval));

    /// Add image avatar
    ui.Image imaged = await loadImage(dataBytes.buffer.asUint8List());
    paintImage(canvas: canvas, image: imaged, rect: oval, fit: BoxFit.fitWidth);

    final img = await pictureRecorder
        .endRecording()
        .toImage(size.width.toInt(), size.height.toInt());
    final data = await img.toByteData(format: ui.ImageByteFormat.png);
    return data.buffer.asUint8List();
  }

  Future<Uint8List> getBytesFromCanvasClientSOS(Size size, Uint8List dataBytes) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final Radius radius = Radius.circular(size.width / 2);

    final Paint tagPaint = Paint()..color = Colors.blue;
    final double tagWidth = 40.0;
    final Paint shadowPaint = Paint()..color = Colors.red;
    final double shadowWidth = 15.0;

    final Paint borderPaint = Paint()..color = Colors.white;
    final double borderWidth = 3.0;

    final double imageOffset = shadowWidth + borderWidth;

    // Add shadow circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              0.0,
              0.0,
              size.width,
              size.height),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        shadowPaint);

    // Add border circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(
              shadowWidth,
              shadowWidth,
              size.width - (shadowWidth * 2),
              size.height - (shadowWidth * 2)),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        borderPaint);

    // Oval for the image
    Rect oval = Rect.fromLTWH(
        imageOffset,
        imageOffset,
        size.width - (imageOffset * 2),
        size.height - (imageOffset * 2));

    // Add path for oval image
    canvas.clipPath(Path()..addOval(oval));

    /// Add image avatar
    ui.Image imaged = await loadImage(dataBytes.buffer.asUint8List());
    paintImage(canvas: canvas, image: imaged, rect: oval, fit: BoxFit.fitWidth);

    final img = await pictureRecorder
        .endRecording()
        .toImage(size.width.toInt(), size.height.toInt());
    final data = await img.toByteData(format: ui.ImageByteFormat.png);
    return data.buffer.asUint8List();
  }

  Future<void> _goToPlace(uLat, uLng) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(uLat, uLng), zoom: 15)));
    setState(() {});
  }

  Future<void> _goToClient(uLat, uLng) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(uLat, uLng), zoom: 15)));
    setState(() {});
  }

  Future<void> _goToSOS(uLat, uLng) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(uLat, uLng), zoom: 15)));
    setState(() {});
  }


  Future<ui.Image> loadImage(List<int> img) async {
    final Completer<ui.Image> completer = new Completer();
    ui.decodeImageFromList(img, (ui.Image img) {
      return completer.complete(img);
    });
    return completer.future;
  }

  zoomToSelectedPlace(placeName){
    for(int i=0;i<widget.journey.milestones.length;i++){
      for(int y=0;y<widget.journey.milestones[i].places.length;y++){
        if(widget.journey.milestones[i].places[y].placeName == placeName){
            _goToPlace(widget.journey.milestones[i].places[y].address.lat, widget.journey.milestones[i].places[y].address.long);
        }
      }
    }
  }

  addAllMarkersInMilestone(milestoneName){
    setState(() {
      markers.removeWhere((item) => item.markerId.toString().contains("Place"));
    });
      for(int i=0;i<widget.journey.milestones.length;i++){
        if(widget.journey.milestones[i].milestoneName == milestoneName){
          for(int y=0;y<widget.journey.milestones[i].places.length;y++){
            setState(() {
              markers.add(Marker(
                  markerId: MarkerId("${widget.journey.milestones[i].places[y].placeName}"),
                  infoWindow: const InfoWindow(title: "client"),
                  icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
                  position: LatLng(
                      widget.journey.milestones[i].places[y].address.lat,
                      widget.journey.milestones[i].places[y].address.long
                  )));
            });

            print(markers.length);
            print(widget.journey.milestones[i].places[y].address.addressLine);
          }
        }

      }
  }

  addMilestonesToList(){
    for(int i =0;i<widget.journey.milestones.length;i++){
      setState(() {
        milestoneList.add(widget.journey.milestones[i].milestoneName);
      });
    }
  }

  addPlacesToList(milestoneName){
    setState(() {
      placesList.clear();
    });
    for(int i=0;i<widget.journey.milestones.length;i++){
      if(widget.journey.milestones[i].milestoneName == milestoneName){
        for(int y = 0;y<widget.journey.milestones[i].places.length;y++){
          setState(() {
            placesList.add(widget.journey.milestones[i].places[y].placeName);
          });
        }
      }
    }
  }
  assignCurrentLocation(){
    Iterator checker = markers.iterator;
    Marker marker;
    while(checker.moveNext()){
       marker = checker.current;
      if(marker.markerId.value == socket.id){
        setState(() {
          currentLocation = new LatLng(marker.position.latitude,marker.position.longitude);
          print("${currentLocation.latitude} - ${currentLocation.longitude}");
        });
      }
    }
  }

  assignDestinationLocation(placeName){
    Iterator checker = markers.iterator;
    Marker marker;
    while(checker.moveNext()){
      marker = checker.current;
      if(marker.markerId.value == placeName){
        setState(() {
          destinationLocation = new LatLng(marker.position.latitude,marker.position.longitude);
          latlng.add(LatLng(
            marker.position.latitude,
            marker.position.longitude,
          ));
          calculateDistane(latlng);
          print("${destinationLocation.latitude} - ${destinationLocation.longitude}");
        });
      }
    }
  }

  setPolyline() {
    polylineCoordinates.clear();
    polylineCoordinates.add(currentLocation);
    polylineCoordinates.add(destinationLocation);
    setState(() {
      _polyline.clear();
      _polyline.add(Polyline(
          polylineId: PolylineId("Polyline"),
          color: Colors.blue,
          points: polylineCoordinates,
        width:5
      ));
    });
  }

  navBar(){
    return Visibility(
      visible: navbarVisible,
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height*0.22,
          decoration: BoxDecoration(
              color: Colors.grey[350],
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(20))
          ),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: Column(
                    children: [
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                            MapBody.disConnect(context);
                          },
                          child: Container(
                            width: 100,
                            height: 30,
                            decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(24.0),
                            ),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.arrow_back,
                                  size: 30,
                                  color: Colors.white,
                                ),
                                Text("Back",style: TextStyle(fontSize: 20,color: Colors.white))
                              ]),
                          )),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          Icon(Icons.directions,size: 35),
                          SizedBox(width: 10),
                          km < 1
                              ? Text(
                            "${(km * 1000).round()} m",
                            style: TextStyle(fontSize: 20),
                          )
                              : Text("${km.round()} km")
                        ],
                      )
                    ],
                  )),
              Padding(
                padding: EdgeInsets.only(top: 40),
                child: Column(
                  children: [
                    Container(
                      height: 25,
                      width: 25,
                      child: Row(
                        children: [
                          Container(
                            height: 25,
                            width: 25,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    width: 5, color: Colors.white)),
                            child: Center(
                              child: Icon(
                                Icons.circle,
                                size: 10,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Dash(
                        direction: Axis.vertical,
                        length: 50,
                        dashLength: 5,
                        dashThickness: 3.0,
                        dashColor: Colors.grey),
                    Container(
                      height: 30,
                      width: 30,
                      child: Row(
                        children: [
                          Icon(
                            Icons.location_pin,
                            size: 30,
                            color: Colors.red,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
                child: Column(
                  children: [
                    Container(
                      width: 200,
                      height: 50,
                      child: DropdownButtonFormField<String>(
                        hint: Text("Choose Milestone"),
                        value: milestoneDropdownValue,
                        style: const TextStyle(color: Colors.black),
                        onChanged: (String newValue) {
                          setState(() {
                            addAllMarkersInMilestone(newValue);
                            placeDropdownValue=null;
                            addPlacesToList(newValue);
                            milestoneDropdownValue = newValue;
                          });
                        },
                        items: milestoneList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: AutoSizeText(value,overflow: TextOverflow.ellipsis,),
                          );
                        }).toList(),
                      ),
                      //_buildDepartureTF(),
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: 200,
                      height: 50,
                      child: DropdownButtonFormField<String>(
                        value: placeDropdownValue,
                        isDense: true,
                        isExpanded: true,
                        hint: Text("Choose Place"),
                        style: const TextStyle(color: Colors.black),
                        onChanged: (String newValue) {
                          setState(() async {
                            await assignDestinationLocation(newValue);
                            placeDropdownValue = newValue;
                            zoomToSelectedPlace(newValue);
                            setPolyline();
                          });
                        },
                        items: placesList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: AutoSizeText(value,overflow: TextOverflow.ellipsis,),
                          );
                        }).toList(),
                      ),
                      //_buildDepartureTF(),
                    ),
                    //_buildDestinationTF(),
                  ],
                ),
              )
            ],
          )),
    );
  }





  @override
  Widget build(BuildContext context) {
    double latSOS = 1.1;
    double lngSOS = 1.1;
    List<LatLng> latlngClient = List();
    var titleSocketsSOS = "";
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(150),
        child: navBar(),
      ),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            child: SizedBox(
              height: SizeConfig.screenHeight,
              width: SizeConfig.screenWidth,
            ),
            decoration: sosOn
                ? BoxDecoration(
                    border: Border(
                        left: BorderSide(
                            width: 10,
                            // color: _animation.value
                            color: Colors.red),
                        right: BorderSide(
                            width: 10,
                            // color: _animation.value
                            color: Colors.red)))
                : null,
          ),
          StreamBuilder(
              stream: myStream.clientLocationStream,
              builder: (context, snapshot) {
                return StreamBuilder(
                    stream: mySosStream.sosMessageStream,
                    builder: (context, snapshotSOS){
                      if (snapshotSOS.hasData){
                        if (snapshotSOS.data != socket.id){
                          print(snapshotSOS.data);
                          _changedSoS(true);
                        }
                      }
                        if(snapshot.hasData) {
                            var str = snapshot.data.toString();
                            var valueMap = json.decode(str);
                            List.generate(valueMap.length, (index) async {
                              if (valueMap[index]["socketID"] != socket.id) {
                                if (valueMap[index]['socketID'] !=
                                    snapshotSOS.data) {
                                  for (int i = 0; i <=
                                      widget.journey.companion.length; i++) {
                                    if (widget.journey.companion[i].userId ==
                                        valueMap[index]["userId"]) {
                                      latlngClient.add(LatLng(valueMap[index]["lat"],valueMap[index]["lng"]));
                                      http.Response response =
                                      await http.get(Uri.parse(
                                          widget.journey.companion[i].userAvatar
                                              .imageLink));
                                      Uint8List markerIconClient = await getBytesFromCanvasClient(
                                          Size(150.0, 150.0),
                                          response.bodyBytes);
                                      return markers.add(Marker(
                                          markerId: MarkerId(
                                              valueMap[index]["socketID"]),
                                          infoWindow: const InfoWindow(
                                              title: "client"),
                                          icon: BitmapDescriptor.fromBytes(
                                              markerIconClient),
                                          position: LatLng(
                                              valueMap[index]["lat"],
                                              valueMap[index]["lng"]
                                          )));
                                    }
                                  }
                                } else {
                                  Marker marker = markers.firstWhere((marker) =>
                                  marker.markerId.value == snapshotSOS.data,
                                      orElse: () => null);
                                  markers.remove(marker);
                                  print(markers.toString());
                                  for (int i = 0; i <=
                                      widget.journey.companion.length; i++) {
                                    if (widget.journey.companion[i].userId ==
                                        valueMap[index]["userId"]) {
                                      titleSocketsSOS = "${widget.journey.companion[i].lastName} need help !! ";
                                      latSOS = valueMap[index]["lat"];
                                      lngSOS = valueMap[index]["lng"];
                                      latlngClient.add(LatLng(valueMap[index]["lat"],valueMap[index]["lng"]));
                                      http.Response response =
                                      await http.get(Uri.parse(
                                          widget.journey.companion[i].userAvatar
                                              .imageLink)
                                      );
                                      Uint8List markerIconClient = await getBytesFromCanvasClientSOS(
                                          Size(150.0, 150.0),
                                          response.bodyBytes);
                                      return markers.add(Marker(
                                          markerId: MarkerId(
                                              valueMap[index]["socketID"] +
                                                  "SOS"),
                                          infoWindow: const InfoWindow(
                                              title: "client"),
                                          icon:
                                          BitmapDescriptor.fromBytes(
                                              markerIconClient),
                                          position: LatLng(
                                              valueMap[index]["lat"],
                                              valueMap[index]["lng"]
                                          )));
                                    }
                                  }
                                }
                              }
                            });

                            return Container(
                              child: Stack(
                                children: [
                                  GoogleMap(
                                      polylines: _polyline,
                                      onTap: (LatLng latLng) {
                                        setState(() {
                                          navbarVisible = !navbarVisible;
                                        });
                                      },
                                      initialCameraPosition: CameraPosition(
                                        target: _target,
                                        zoom: 5,
                                      ),
                                      myLocationButtonEnabled: false,
                                      myLocationEnabled: true,
                                      zoomControlsEnabled: true,
                                      onMapCreated: (
                                          GoogleMapController controller) {
                                        _controller.complete(controller);
                                        assignCurrentLocation();
                                      },
                                      markers: markers
                                  ),
                                  sosClient  == true ?
                                  AlertDialog(
                                    backgroundColor: Colors.red.withAlpha(100),
                                    title: Center(child: Column(
                                      children: [
                                        Text(titleSocketsSOS, style: TextStyle(color: Colors.white),),
                                        Text("Go to help", style: TextStyle(color: Colors.white),),
                                      ],
                                    )),
                                    actions: <Widget>[
                                      Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            TextButton(
                                              onPressed: (){ _changedSoS(false);
                                              setState(() {
                                              });},
                                              child: const Text('No',style: TextStyle(fontSize: 20, color: Colors.black),),
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                _goToSOS(latSOS, lngSOS);
                                                print(latSOS.toString());
                                                setState(() {
                                                });
                                              },
                                              child: const Text('Yes',style: TextStyle(fontSize: 20, color: Colors.black)),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  )
                                      : Container()
                                ],
                              ),
                            );
                        }else {
                          return CircularProgressIndicator();
                        }
                    }
                );
              }),
        // TextButton(
        //         onPressed: () {
        //           setState(() {
        //             _UserMarker();
        //           });
        //         },
        //         child: Text("Debug")),

          /// Dangerous Message
          Positioned(
            left: 20.0,
            bottom: _fabHeight,
            child: FloatingActionButton(
              child: Icon(
                Icons.people,
                color: Theme.of(context).primaryColor,
              ),
              onPressed: () {
                for (int i = 0; i < latlng.length; i++) {
                    _goToClient(latlngClient[i].latitude,latlngClient[i].longitude);
                  }





              },
              backgroundColor: Colors.white,
            ),
          ),

          /// User Location
          Positioned(
            right: 20.0,
            bottom: _fabHeight,
            child: FloatingActionButton(
              child: Icon(
                Icons.gps_fixed,
                color: Theme.of(context).primaryColor,
              ),
              onPressed: () async {
                _UserMarker();
                setState(() {});
              },
              backgroundColor: Colors.white,
            ),
          ),

          /// SOS
          Positioned(
            bottom: widget.panelHeightClosed + 5,
            child: FlatButton(
              onPressed: () async {
                markers.clear();
                _changed(true);
                // MyLocation();
                socket.emit("sos message",socket.id);
                setState(() {

                });
              },
              child: Container(
                padding: const EdgeInsets.fromLTRB(24.0, 18.0, 24.0, 18.0),
                child: Text(
                  "SOS",
                  style: TextStyle(
                      fontSize: 50,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                ),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(24.0),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, .25), blurRadius: 16.0)
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

showAlertDialog(BuildContext context) {

  // set up the button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () { },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("SOS Message"),
    content: Text("This is SOS message."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
