import 'dart:math' as math;
import 'dart:ui';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/Model/Place.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/Map/MapBody.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class InProgressScreen extends StatefulWidget {
  const InProgressScreen({Key key, @required this.journey}) : super(key: key);
  final Journey journey;
  @override
  _InProgressScreenState createState() => _InProgressScreenState();
}

class _InProgressScreenState extends State<InProgressScreen> {
  bool _showAppbar = true;

  // void getCurrentLocation() async {
  //
  //   var position = await Geolocator
  //       .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  //   lastPosition = await Geolocator.getLastKnownPosition();
  //   print(lastPosition);
  //   final coordinates = new Coordinates(position.latitude, position.longitude);
  //   convertCoordinateToAddress(coordinates).then((value) => _address = value);
  //
  //   setState(() {
  //   });
  //
  // }
  //
  List<Milestones> milestoneList = [];
  final double _initFabHeight = 120.0;
  double _fabHeight = 0;
  double _panelHeightOpen = 0;
  double _panelHeightClosed = 95.0;



  static LatLng _center;
  LatLng _lastMapPosition = _center;
  final Set<Polyline> _polyline = {};
  List<LatLng> latlng = List();
  final Set<Marker> _markers = {};

  @override
  void initState() {
    super.initState();
    _fabHeight = _initFabHeight;
    addMilestonesToList();
  }

  @override
  Widget build(BuildContext context) {
    _panelHeightOpen = MediaQuery.of(context).size.height * .80;
    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          MapBody(
            journey: widget.journey,
            panelHeightClosed: _panelHeightClosed,
          ),
          SlidingUpPanel(
            maxHeight: _panelHeightOpen - 200,
            minHeight: _panelHeightClosed,
            parallaxEnabled: true,
            parallaxOffset: .5,
            // body: _body(),
            panelBuilder: (sc) => _panel(sc),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18.0),
                topRight: Radius.circular(18.0)),
            onPanelSlide: (double pos) => setState(() {
              _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
                  _initFabHeight;
            }),
          ),

          //the SlidingUpPanel Title
        ],
      ),
    );
    ;
  }

  Widget _panel(ScrollController sc) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(
          controller: sc,
          children: <Widget>[
            SizedBox(
              height: 12.0,
            ),
            Center(
              child: Container(
                width: 30,
                height: 5,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
              ),
            ),
            SizedBox(
              height: 18.0,
            ),
            Center(
              child: Text(
                "My Trip Plan",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 24.0,
                ),
              ),
            ),
            SizedBox(height: getHeight(0.055)),
            Row(),
            showMilestone(),
          ],
        ));
  }

  addMilestonesToList() {
    for (var i in widget.journey.milestones) {
      setState(() {
        milestoneList.add(i);
      });
    }
  }

  showMilestone() {
    List<Widget> list = [];
    for (int i = 0; i < milestoneList.length; i++) {
      if (i == 0) {
        list.add(milestones(milestoneList[i]));
      } else {
        list.add(Padding(
          padding: EdgeInsets.only(left: 13),
          child: Dash(
              direction: Axis.vertical,
              length: 15,
              dashLength: 3,
              dashThickness: 2.0,
              dashColor: Colors.grey),
        ));
        list.add(milestones(milestoneList[i]));
      }
    }
    return Padding(
      padding: EdgeInsets.only(left: getWidth(0.1)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: list,
      ),
    );
  }


  Widget milestones(Milestones milestone) {
    return Column(
      children: [
        Row(
          children: [
            Icon(
              Icons.circle,
              color: Colors.grey,
              size: 30,
            ),
            SizedBox(width: getWidth(0.02)),
            AutoSizeText(
              "${milestone.milestoneName}",
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
        showPlace(milestone)
      ],
    );
  }

  showPlace(Milestones milestone) {
    List<Widget> list = [];
    for (int i = 0; i < milestone.places.length; i++) {
      list.add(Padding(
        padding: EdgeInsets.only(left: 13),
        child:
        Dash(
            direction: Axis.vertical,
            length: 15,
            dashLength: 3,
            dashThickness: 2.0,
            dashColor: Colors.grey),
      ));
      list.add(places(milestone.places[i]));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: list,
    );
  }

  Widget places(Place place) {
    return Padding(
      padding: EdgeInsets.only(left: 4),
      child: Row(
        children: [
          Icon(
            Icons.circle,
            color: Colors.grey[300],
            size: 20,
          ),
          SizedBox(width: getWidth(0.02)),
          AutoSizeText("${place.placeName}")
        ],
      ),
    );
  }
}
