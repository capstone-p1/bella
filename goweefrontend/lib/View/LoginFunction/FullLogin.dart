import 'dart:convert' show json, base64, ascii;
import 'package:http/http.dart' as http;
import '../../Constants.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:goweefrontend/View/UserPage/UserPage.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';




class FullLogin extends StatelessWidget {


  Future<String> get jwtOrEmpty async {
    var jwt = await storage.read(key: "jwt");
    if(jwt == null) return "";
    return jwt;
  }

// var listInforUser = [];

  logOut() async {
    var res = await http.post(
      Uri.parse("$baseUrl/logout/${loggedUser.user.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
    storage.deleteAll();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Authentication Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder(
          future: jwtOrEmpty,
          builder: (context, snapshot) {
            if(!snapshot.hasData) return CircularProgressIndicator();
            if(snapshot.data != "") {
              var str = snapshot.data;
              Map valueMap = json.decode(str);
              final jwt = valueMap["token"].split(".");
              var payload = json.decode(ascii.decode(base64.decode(base64.normalize(jwt[1]))));
              if(jwt.length !=3) {
                return LoginScreen();
              } else {
                if(DateTime.fromMillisecondsSinceEpoch(payload["exp"]*1000).isAfter(DateTime.now())) {
                  return UserPage();
                } else {
                  logOut();
                  return LoginScreen();
                }
              }
            } else {
              return LoginScreen();
            }
          }
      ),
    );
  }
}