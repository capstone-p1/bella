import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/SectionTitle.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginGoogle.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/SignupScreen.dart';
import 'package:http/http.dart' as http;
import '../../../Constants.dart';


final storage = FlutterSecureStorage();

class LoginScreen extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );

  Future<String> attemptLogIn(String email, String password) async {
    var res = await http.post(Uri.parse("$baseUrl/login"),
        body: {"email": email, "password": password});
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return null;
    }
  }

  Widget _buildUserTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: TextStyle(color: Color(0xffEEEEEE), fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFormFieldDecoration,
          height: 60.0,
          child: TextField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              hintText: 'Enter your email',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: TextStyle(color: Color(0xffEEEEEE), fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFormFieldDecoration,
          height: 60.0,
          child: TextField(
            controller: _passwordController,
            obscureText: true,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Color(0xffEEEEEE),
              ),
              hintText: 'Enter your Password',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () => print('Forgot Password Button Pressed'),
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Forgot Password?',
          style: kLabelStyle,
        ),
      ),
    );
  }

  Widget _buildSignInWithText() {
    return Column(
      children: <Widget>[
        Text(
          '- OR -',
          style: TextStyle(
            fontSize: 15,
            color: Color(0xffEEEEEE),
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(height: 20.0),
        Text('Sign in with',
            style: TextStyle(
              fontSize: 15,
              color: Color(0xffEEEEEE),
              fontWeight: FontWeight.w500,
            )),
      ],
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: kBackgroundColor,
      body: SingleChildScrollView(
        child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Column(
                children: <Widget>[
                  Padding(
                    // physics: AlwaysScrollableScrollPhysics(),
                    padding: EdgeInsets.fromLTRB(40, 30, 40, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Login',
                          style: TextStyle(
                            color: Color(0xffEEEEEE),
                            fontFamily: 'OpenSans',
                            fontSize: 50.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 15.0),
                        _buildUserTF(),
                        SizedBox(height: 15.0),
                        _buildPasswordTF(),
                        // _buildForgotPasswordBtn(),
                        SizedBox(height: 25),
                        Container(
                          width: double.infinity,
                          child: RaisedButton(
                            elevation: 5.0,
                            onPressed: () async {
                              var email = _emailController.text;
                              var password = _passwordController.text;
                              var jwt = await attemptLogIn(email, password);
                              if (jwt != null) {
                                storage.write(key: "jwt", value: jwt);
                                displayDialog(context, "Notification",
                                    "Login Successfully");
                                RestartWidget.restartApp(context);

                              } else {
                                displayDialog(context, "An Error Occurred",
                                    "No account was found matching that email and password");
                              }
                            },
                            padding: EdgeInsets.all(15.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            color: Color(0xff548CA8),
                            child: Text(
                              'LOGIN',
                              style: TextStyle(
                                color: Color(0xffEEEEEE),
                                letterSpacing: 1.5,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'OpenSans',
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        _buildSignInWithText(),
                        SizedBox(height: 15),
                        LoginGoogle(),
                        SizedBox(height: 30),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RegisterScreen()));
                                },
                                child: Text("Don\' t have an Account ? Sign Up",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xffEEEEEE)
                                ),))
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
      ),
    );}
}
