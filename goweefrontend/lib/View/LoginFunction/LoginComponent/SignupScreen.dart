import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/SectionTitle.dart';

import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:http/http.dart' as http;

import '../../../Constants.dart';


class RegisterScreen extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFormFieldDecoration,
          height: 60.0,
          child: TextField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              hintText: 'Enter your Email',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildUserFirstNameTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'First Name',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFormFieldDecoration,
          height: 60.0,
          child: TextField(
            controller: _firstNameController,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              hintText: 'Enter your first name',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildUserLastNameTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Last Name',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFormFieldDecoration,
          height: 60.0,
          child: TextField(
            controller: _lastNameController,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              hintText: 'Enter your last name',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFormFieldDecoration,
          height: 60.0,
          child: TextField(
            controller: _passwordController,
            obscureText: true,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Enter your Password',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  // Future<int> attemptSignUp(
  //     String email, String password, String name) async {
  //   var res = await http.post(Uri.parse('$baseUrl/register'),
  //       body: {"email": email, "password": password, "name": name});
  //   return res.statusCode;
  // }

  Future<int> attemptSignUp(
      String email, String password, String firstName , String lastName) async {
    var res = await http.post(Uri.parse('$baseUrl/register'),
        body: {"email": email, "password": password, "firstName": firstName, "lastName": lastName});
    return res.statusCode;
  }

  Future<String> attemptLogIn(String email, String password) async {
    var res = await http.post(Uri.parse("$baseUrl/login"),
        body: {"email": email, "password": password});
    if (res.statusCode == 200) return res.body;
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xff334257),
      body: SingleChildScrollView(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Column(
              children: <Widget>[
                Padding(
                    // physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.fromLTRB(40, 30, 40, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Sign Up',
                          style: TextStyle(
                            color: Color(0xffEEEEEE),
                            fontFamily: 'OpenSans',
                            fontSize: 50.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 15.0),
                        _buildEmailTF(),
                        SizedBox(
                          height: 15.0
                        ),
                        _buildUserFirstNameTF(),
                        SizedBox(
                          height: 15.0
                        ),
                        _buildUserLastNameTF(),
                        SizedBox(
                            height: 15.0
                        ),
                        _buildPasswordTF(),
                        // _buildForgotPasswordBtn(),
                        SizedBox(height: 25),
                        Container(
                          width: double.infinity,
                          child: RaisedButton(
                            elevation: 5.0,
                            onPressed: () async {
                              var email = _emailController.text;
                              var password = _passwordController.text;
                              var firstName = _firstNameController.text;
                              var lastName = _lastNameController.text;

                            if (EmailValidator.validate(email) == false)
                              displayDialog(context, "Invalid Email",
                                  "Your Email form is wrong");
                            // else if (password.length < 7)
                            //   displayDialog(context, "Invalid Password",
                            //       "The password should be at least 7 characters long");
                            else {
                              var res = await attemptSignUp(email, password, firstName,lastName);
                              print("$res");
                              // displayDialog(context, titleDialog, textDialog);
                              // if (storage != null) {
                              //  RestartWidget.restartApp(context);
                              // }
                              if (res == 201) {
                                displayDialog(context, "Success",
                                    "The user was created. Log in now.");
                                var resLog = await http.post(Uri.parse("$baseUrl/login"),
                                           body: {"email": email, "password": password});
                                       if (resLog.statusCode == 200){
                                         if (resLog.body != null) {
                                           storage.write(key: "jwt", value: resLog.body);}}
                                RestartWidget.restartApp(context);
                              } else if (res == 500)
                                displayDialog(
                                    context,
                                    "That email is already registered",
                                    "Please try to sign up using another email or log in if you already have an account.");
                              else {
                                displayDialog(context, "Error",
                                    "An unknown error occurred.");
                              }
                            }
                          },
                          padding: EdgeInsets.all(15.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          color: kColorPalette3,
                          child: Text(
                            'SIGN UP',
                            style: TextStyle(
                              color: kColorPalette4,
                              letterSpacing: 1.5,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans',
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: getHeight(0.02)),
                      GestureDetector(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginScreen())),
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Have an Account? ',
                                style: TextStyle(
                                  color: kColorPalette4,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              TextSpan(
                                text: 'Sign In',
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
