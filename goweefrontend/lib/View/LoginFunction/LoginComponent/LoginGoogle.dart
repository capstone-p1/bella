
import 'package:flutter/material.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/WebLoginGoogle.dart';


class LoginGoogle extends StatefulWidget {
  const LoginGoogle({Key key}) : super(key: key);

  @override
  _LoginGoogleState createState() => _LoginGoogleState();
}

class _LoginGoogleState extends State<LoginGoogle> {


  @override
  Widget build(BuildContext context) {
    return FlatButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
              builder: (context) => SignInDemo()));

        },
        child: Container(
          height: 60.0,
          width: 60.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                offset: Offset(0, 2),
                blurRadius: 6.0,
              ),
            ],
            image: DecorationImage(
              image: AssetImage(
                'assets/images/google.jpg',
              ),
            ),
          ),
        )
    );
  }
}
