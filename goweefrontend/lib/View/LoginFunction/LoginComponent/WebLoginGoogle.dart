import 'dart:async';

import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/SectionTitle.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';


String selectedUrl = '$baseUrl/google';

// ignore: prefer_collection_literals
final Set<JavascriptChannel> jsChannels = [
  JavascriptChannel(
      name: 'Print',
      onMessageReceived: (JavascriptMessage message) {
        print(message.message);
      }),
].toSet();

final storage = FlutterSecureStorage();

class SignInDemo extends StatefulWidget {
  @override
  _SignInDemoState createState() => _SignInDemoState();
}

class _SignInDemoState extends State<SignInDemo> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();



  void getHttp(val) async {
    try {
      var response = await Dio().get(val);
      print(response);
        var jwt = response.toString();
        storage.write(key: "jwt", value: jwt);
        RestartWidget.restartApp(context);

    } catch (e) {
      print("false");
      print(e);
    }
  }


  // Instance of WebView plugin

  // On destroy stream
  StreamSubscription _onDestroy;

  // On urlChanged stream
  StreamSubscription<String> _onUrlChanged;

  // On urlChanged stream
  StreamSubscription<WebViewStateChanged> _onStateChanged;

  StreamSubscription<WebViewHttpError> _onHttpError;

  StreamSubscription<double> _onProgressChanged;


  final _urlCtrl = TextEditingController(text: selectedUrl);



  final _history = [];

  @override
  void initState() {
    super.initState();

    flutterWebViewPlugin.close();

    _urlCtrl.addListener(() {
      selectedUrl = _urlCtrl.text;
    });

    // Add a listener to on destroy WebView, so you can make came actions.
    _onDestroy = flutterWebViewPlugin.onDestroy.listen((_) {
      if (mounted) {
        // Actions like show a info toast.
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Webview Destroyed')));
      }
    });

    _onStateChanged =
        flutterWebViewPlugin.onStateChanged.listen((WebViewStateChanged state) {
          if (mounted) {
            if(state.url.startsWith("$baseUrl/google/redirect?code=4")) {
              // if (state.type == WebViewState.shouldStart) {
                setState(() {
                  print(state.type);
                  print(state.url);
                  getHttp(state.url);
                  _history.add(
                      'onStateChanged: ${state.type} ${state.url}');
                });
              // }
            }
          }
        });

    _onHttpError =
        flutterWebViewPlugin.onHttpError.listen((WebViewHttpError error) {
          if (mounted) {
            setState(() {
              _history.add('onHttpError: ${error.code} ${error.url}');
            });
          }
        });
  }


  @override
  void dispose() {
    // Every listener should be canceled, the same should be done with this stream.
    _onDestroy.cancel();
    _onUrlChanged.cancel();
    // _onStateChanged.cancel();
    _onHttpError.cancel();
    _onProgressChanged.cancel();

    flutterWebViewPlugin.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
      child: WebviewScaffold(
        appBar: AppBar(
          title: Text("Login With Google"),
        ),
        userAgent: Platform.isIOS ? 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15' +
            ' (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1' :
        'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) ' +
            'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36',

        url: selectedUrl,
        javascriptChannels: jsChannels,
        mediaPlaybackRequiresUserGesture: false,
        resizeToAvoidBottomInset: false,
        withZoom: true,
        withLocalStorage: true,
        hidden: true,
        initialChild: Center(
          child: Column(
            children: [
              CircularProgressIndicator(),
              Text('Waiting.....'),
            ],
          ),
        ),
      ),
    );
  }
}
