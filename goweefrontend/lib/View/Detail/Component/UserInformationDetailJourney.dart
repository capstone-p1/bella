import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';

import '../../../SizeConfig.dart';

class UserInformationDetailJourney extends StatelessWidget {
  const UserInformationDetailJourney({Key key, @required this.journey})
      : super(key: key);
  final Journey journey;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            width: getWidth(0.9),
            child: Row(
              children: [
                Container(
                  width: getWidth(0.1),
                  height: getWidth(0.1),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image:
                            NetworkImage(journey.author.userAvatar.imageLink),
                        fit: BoxFit.cover),
                  ),
                ),
                SizedBox(
                  width: getWidth(0.015),
                ),
                Text(
                  "${journey.author.firstName + " " + journey.author.lastName}",
                  style: TextStyle(
                    fontSize: getWidth(0.065),
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
