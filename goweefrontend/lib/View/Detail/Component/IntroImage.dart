import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/View/Detail/Component/CarouselDetailJourney.dart';

import '../../../SizeConfig.dart';

class IntroImage extends StatelessWidget {
  const IntroImage({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;

  @override
  Widget build(BuildContext context) {
    if (journey.images.length > 1) {
      return Padding(
        padding: EdgeInsets.only(left: getWidth(0.02), right: getWidth(0.02)),
        child: CarouselDetailJourney(journey: journey),
      );
    } else {
      return Center(
          child: Container(
        width: getWidth(0.95),
        height: getWidth(0.5),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: journey.images.isEmpty
                    ? AssetImage(
                        "assets/images/Travel-WordPress-Themes.jpg.webp")
                    : NetworkImage(journey.images[0].imageLink),
                fit: BoxFit.cover)),
      ));
    }
  }
}
