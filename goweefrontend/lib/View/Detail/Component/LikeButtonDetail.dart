import 'dart:async';
import 'dart:convert';


import 'package:goweefrontend/SizeConfig.dart';


import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';

import '../../../Constants.dart';

class LikeButtonDetail extends StatefulWidget {
  // Lấy data từ ListJourney
  const LikeButtonDetail({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;


  @override
  _LikeButtonDetailState createState() => _LikeButtonDetailState();
}


class _LikeButtonDetailState extends State<LikeButtonDetail> {


  bool isLiked =false;
  Future fetchLike() async {

    // var authorIdLike =[];
    final response = await http.get(Uri.parse("$baseUrl/likes"));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return jsonDecode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Stream getLike(Duration refreshTime) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await fetchLike();
    }
  }



  //HTTP
  // Create Like
  Future<void> postLike(userId, journeyId) async {
    String token = loggedUser.token; // how to get token?
    String url = '$baseUrl/likes/${widget.journey.journeyId}';
    final response = await http.post(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode({
          'userId': userId,
          'journeyId': journeyId,
        }));

    print(response.statusCode);
    // print(response.body);
    fetchLike();
    setState(() {
    });
  }

  // Delete like
  Future<http.Response> deleteLike(int likeId) async {
    String token = loggedUser.token; // how to get token?
    final http.Response response = await http.delete(
      Uri.parse('$baseUrl/likes/$likeId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
    );
    setState(() {
    });
    fetchLike();
    setState(() {
    });
  }
  Future futureLike;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureLike = fetchLike();
  }


  @override
  Widget build(BuildContext context) {


    return StreamBuilder(
        stream: getLike(Duration(milliseconds: 1)),
        builder: (context, snapshot) {
          if(snapshot.hasData){
            var authorIdLike =[];
            for (var i in snapshot.data) {
              if(i["journey"]["journeyId"]== widget.journey.journeyId){
                authorIdLike.add(i["authorId"]);
              }
            }
            return Container(
              child: Row(
                children: [
                  IconButton(
                    icon: Center(
                      child: Container(
                        child: loggedUser.token == null ?
                        Icon(
                          Icons.favorite_sharp,
                          size: getProportionateScreenWidth(25),
                          color: Colors.grey,
                        ) :
                        authorIdLike.contains(loggedUser.user.userId) == true
                            ? SvgPicture.asset(
                          "assets/images/loved_icon.svg",
                          width: getProportionateScreenWidth(25),
                        )
                            : Icon(
                          Icons.favorite_sharp,
                          size: getProportionateScreenWidth(25),
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    onPressed: () async{
                      if (loggedUser.token == null){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    LoginScreen()));
                      }
                      if (authorIdLike.contains(loggedUser.user.userId) == true){
                        List.generate(snapshot.data.length, (index) {
                          if (snapshot.data[index]["journey"]["journeyId"]== widget.journey.journeyId) {
                            deleteLike(snapshot.data[index]["likeId"]);
                          }
                        });
                      }
                      else {
                        postLike(loggedUser.user.userId, widget.journey.journeyId);
                      }

                    },
                  ),
                ],
              ),
            );
          }
          return Container();
        }
    );

  }
}
