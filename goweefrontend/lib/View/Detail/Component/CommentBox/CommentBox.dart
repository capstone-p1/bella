import 'dart:async';
import 'dart:convert';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/CommentDetail.dart';
import 'package:goweefrontend/View/Detail/DetailJourney.dart';
import 'package:http/http.dart' as http;
import 'package:comment_box/comment/comment.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import '../../../../Services.dart';
import '../../../../SizeConfig.dart';


class TestMe extends StatefulWidget {
  const TestMe({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;
  @override
  _TestMeState createState() => _TestMeState();
}

class _TestMeState extends State<TestMe> {
  Timer timer;

  final formKey = GlobalKey<FormState>();

  final TextEditingController commentController = TextEditingController();

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
  }

  var commentDetail = new List<CommentDetail>();
  _getComment() {
    CommentAPI.getComment().then((response) {
      setState(() {
        Iterable list = json.decode(response.body);
        commentDetail = list.map((model) => CommentDetail.fromJson(model)).toList();
      });
    });
  }

  var journeysComment = new List<Journey>();

  void _getJourney() {
    JourneyAPI.getJourney().then((response) {
      setState(() {
        Iterable list = json.decode(response.body);
        journeysComment = list.map((model) => Journey.fromJson(model)).toList();
      });
    });
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    _getComment();
    _getJourney();
  }



  @override
  Widget build(BuildContext context) {
    Future<void> postComment(userId, journeyId, content) async {
      String token = loggedUser.token; // how to get token?
      String url = '$baseUrl/comments/${widget.journey.journeyId}';
      final response = await http.post(Uri.parse(url),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          },
          body: jsonEncode({
            'userId': userId,
            'journeyId': journeyId,
            'content' : content,
          }));

      print(response.statusCode);
      print(response.body);
      _getJourney();
      _getComment();
      DetailsJourneyScreen.restartData(context);
    }



    Widget CommentList(){
      List commentList = [];
      List.generate(journeysComment.length, (index){
        if(journeysComment[index].journeyId == widget.journey.journeyId){
          commentList = new List.generate(journeysComment[index].comments.length, (ix) =>
                    journeysComment[index].comments[ix].cmtId);
        }
      });

      var comment = new List<CommentDetail>();
      List.generate(commentDetail.length, (index) async{
        if(commentList.contains(commentDetail[index].cmtId)){
            comment.add(commentDetail[index]);
        }
      });


      return SingleChildScrollView(
        child: Container(
            child: Column(
              children: [
                  ...List.generate(comment.length, (index) {
                    // if (commentList.contains(commentDetail[index].cmtId)) {
                      return Padding(
                        padding: const EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 0.0),
                        child: ListTile(
                          leading: GestureDetector(
                            onTap: () async {
                              // Display the image in large form.
                              print("Comment Clicked");
                            },
                            child: Container(
                              height: getWidth(0.1),
                              width: getWidth(0.1),
                              decoration: new BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius:
                                  new BorderRadius.all(Radius.circular(50))),
                              child: CircleAvatar(
                                  radius: 50,
                                  backgroundImage: NetworkImage(
                                      comment[index].author.userAvatar.imageLink)),
                            ),
                          ),
                          title: Text(
                            comment[index].author.firstName +" "+ comment[index].author.lastName,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(comment[index].content),
                        ),
                      );
                }),
              ],
            )),
      );
    }



    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
            // vertical: 10,
          ),
        ),
         Container(
                height: getHeight(0.5),
              child: CommentBox(
                  userImage: loggedUser.token != null ?
                  loggedUser.user.userAvatar.imageLink : "http://lofrev.net/wp-content/photos/2017/05/user_black_logo.jpg",
                  child: CommentList(),
                  labelText: 'Write a comment...',
                  withBorder: false,
                  errorText: 'Comment cannot be blank',
                  sendButtonMethod: () {
                    if(loggedUser.token != null) {
                      if (formKey.currentState.validate()) {
                        print(commentController.text);
                        setState(() {
                          postComment(
                              loggedUser.user.userId,
                              widget.journey.journeyId,
                              commentController.text);
                          commentDetail.insert(0, _getComment());
                        });

                        commentController.clear();
                        FocusScope.of(context).unfocus();
                        // RestartWidget.restartApp(context);
                        // CommentList();
                      } else {
                        print("Not validated");
                      }
                    }else{
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                LoginScreen()),
                      );
                    }
                  },
                  formKey: formKey,
                  commentController: commentController,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  sendWidget:
                      Icon(Icons.send_sharp, size: getWidth(0.08), color: Colors.white),
                ),
            ),
      ],
    );
  }

}

