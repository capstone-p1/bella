import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/Detail/Component/ManageTeam/ListTeamMember.dart';
import 'package:goweefrontend/View/UserPage(Guest)/GuestView.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../../Constants.dart';

class ManageTeam extends StatefulWidget {
  const ManageTeam({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;

  @override
  _ManageTeamState createState() => _ManageTeamState();
}

class _ManageTeamState extends State<ManageTeam> {
  Future fetchRequest() async {
    String token = loggedUser.token;
    final response = await http.get(
      Uri.parse("$baseUrl/requests"),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return jsonDecode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Stream getRequest(Duration refreshTime) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await fetchRequest();
    }
  }

  /// HTTP Post request
  Future<void> postRequest(int ownerId, int senderId) async {
    String token = loggedUser.token; // how to get token?
    String url = '$baseUrl/requests/${widget.journey.journeyId}';
    final response = await http.post(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode({
          "ownerId": ownerId,
          "senderId": senderId,
        }));

    print(response.statusCode);
    // print(response.body);
  }

  /// Http Delete Request
  Future<http.Response> deleteRequest(int requestId) async {
    String token = loggedUser.token; // how to get token?
    final http.Response response = await http.delete(
      Uri.parse('$baseUrl/requests/$requestId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
    );
    print(response.statusCode);
    setState(() {
    });
    setState(() {});
  }

  Future futureRequest;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureRequest = fetchRequest();
  }

  @override
  Widget build(BuildContext context) {
    List companionId = [];
    List.generate(widget.journey.companion.length,
        (index) => companionId.add(widget.journey.companion[index].userId));

    return Container(
      padding: EdgeInsets.only(left: getWidth(0.05)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Companions",
            style: TextStyle(
                fontSize: getWidth(0.04),
                fontWeight: FontWeight.bold,
                color: Colors.black45),
          ),
          Row(
            children: [
              Container(
                width: getWidth(0.45),
                child: Row(
                  children: [
                    ...List.generate(
                      widget.journey.companion.length < 3
                          ? widget.journey.companion.length
                          : 3,
                      (index) => GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GuestView(
                                      userId: widget
                                          .journey.companion[index].userId,
                                    )),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: getWidth(0.015)),
                          width: getWidth(0.09),
                          height: getWidth(0.09),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(widget.journey
                                    .companion[index].userAvatar.imageLink),
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                    ),
                    widget.journey.companion.length > 3
                        ? Container(
                            width: getWidth(0.1),
                            height: getWidth(0.1),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.black12,
                            ),
                            child: IconButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ListTeamMember(journey: widget.journey,)),
                                );
                              },
                              icon:
                                  Icon(Icons.more_horiz, size: getWidth(0.06)),
                            ))
                        : Container(),
                  ],
                ),
              ),
              SizedBox(
                width: getWidth(0.2),
              ),

              /// Join Button
              loggedUser.token != null ?
              loggedUser.user.userId != widget.journey.author.userId
                  ? StreamBuilder(
                      stream: getRequest(Duration(milliseconds: 1)),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          var senderIdIdRequest = [];
                          for (var i in snapshot.data) {
                            if (i["journeyRequesting"]["journeyId"] ==
                                widget.journey.journeyId) {
                              senderIdIdRequest.add(i["senderId"]);
                            }
                          }

                          /// join button on
                          return Container(
                            width: getWidth(0.25),
                            height: getWidth(0.08),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[300],
                                  blurRadius: 4,
                                  offset: Offset(0, 8), // Shadow position
                                ),
                              ],
                              gradient:

                              companionId.contains(loggedUser.user.userId)== true ?
                              LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomRight,
                                colors: [
                                  // Colors.greenAccent,
                                  // Color.fromARGB(255, 31, 64, 55),

                                  Colors.grey,
                                  Colors.black
                                ],
                              ):
                              senderIdIdRequest
                                          .contains(loggedUser.user.userId) ==
                                      true
                                  ? LinearGradient(
                                      begin: Alignment.topRight,
                                      end: Alignment.bottomRight,
                                      colors: [
                                        // Colors.greenAccent,
                                        // Color.fromARGB(255, 31, 64, 55),
                                        Colors.grey,
                                        Colors.black
                                      ],
                                    )
                                  : LinearGradient(
                                      begin: Alignment.topRight,
                                      end: Alignment.bottomRight,
                                      colors: [
                                        // Colors.greenAccent,
                                        // Color.fromARGB(255, 31, 64, 55),

                                        Color(0xFFFDC830),
                                        Color(0xFFF37335)
                                      ],
                                    ),
                            ),
                            child:
                                companionId.contains(loggedUser.user.userId) ?
                                FlatButton(
                                  child: Center(
                                      child: Text(
                                        "Joined",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: getWidth(0.03),
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      )),
                                  onPressed: () {
                                    showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) => CupertinoAlertDialog(
                                        title: const Text('Are you sure to leave our trip ?'),
                                        actions: <Widget>[
                                          TextButton(
                                            onPressed: () => Navigator.pop(context, 'Cancel'),
                                            child: const Text('Cancel'),
                                          ),
                                          TextButton(
                                            onPressed: () {
                                              List.generate(snapshot.data.length,
                                                      (index) {
                                                    if (snapshot.data[index]["journeyRequesting"]["journeyId"] == widget.journey.journeyId) {
                                                      deleteRequest(snapshot.data[index]["id"]);
                                                    }
                                                  });

                                              final snackBar = SnackBar(
                                                content: Text(
                                                  'Dumb Selection',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(fontSize: 18),
                                                ),
                                              );

                                              // Find the ScaffoldMessenger in the widget tree
                                              // and use it to show a SnackBar.
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(snackBar);
                                            },
                                            child: const Text('Leave'),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ):
                            senderIdIdRequest
                                        .contains(loggedUser.user.userId) ==
                                    true
                                ? FlatButton(
                                    child: Center(
                                        child: Text(
                                      "Cancel Request",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: getWidth(0.03),
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    )),
                                    onPressed: () {
                                      List.generate(snapshot.data.length,
                                          (index) {
                                        if (snapshot.data[index]["journeyRequesting"]["journeyId"] == widget.journey.journeyId) {
                                          deleteRequest(snapshot.data[index]["id"]);
                                        }
                                      });

                                      final snackBar = SnackBar(
                                        content: Text(
                                          'You have cancelled the request',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 18),
                                        ),
                                      );

                                      // Find the ScaffoldMessenger in the widget tree
                                      // and use it to show a SnackBar.
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    },
                                  )
                                : FlatButton(
                                    child: Center(
                                        child: Text(
                                      "Join",
                                      style: TextStyle(
                                          fontSize: getWidth(0.04),
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    )),
                                    onPressed: () {
                                      postRequest(widget.journey.author.userId,
                                          loggedUser.user.userId);
                                      final snackBar = SnackBar(
                                        content: Text(
                                          'You have send the request',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 18),
                                        ),
                                      );

                                      // Find the ScaffoldMessenger in the widget tree
                                      // and use it to show a SnackBar.
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    },
                                  ),
                          );
                        }
                        return Text("");
                      })
                  : Container()
                  : Container()
            ],
          )
        ],
      ),
    );
  }
}
