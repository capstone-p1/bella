import 'package:flutter/material.dart';


class ShortInforOfMember extends StatefulWidget {
  @override
  _ShortInforOfMemberState createState() => _ShortInforOfMemberState();
}

class _ShortInforOfMemberState extends State<ShortInforOfMember> {
  bool display = false;

  void _changed(bool visibility) {
    setState(() {
      display = !display;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 320, top: 5),
          child: IconButton(
            onPressed: () {
              _changed(true);
            },
            icon: display
                ? Icon(Icons.remove)
                : Icon(Icons.remove_red_eye_outlined),
          ),
        ),
        display
            ? Padding(
                padding: EdgeInsets.only(left: 50, top: 5),
                child: Container(
                    width: 350,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 4,
                          offset: Offset(4, 8), // Shadow position
                        ),
                      ],
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: 20, top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Phone Number:",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Address:",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    )),
              )
            : Container()
      ],
    );
  }
}
