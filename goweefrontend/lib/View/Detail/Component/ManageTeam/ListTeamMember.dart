import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/UserPage(Guest)/GuestView.dart';
import 'package:goweefrontend/View/UserPage/UserPage.dart';

import '../../../../SizeConfig.dart';

class ListTeamMember extends StatefulWidget {
  const ListTeamMember({Key key, @required this.journey}) : super(key: key);
  final Journey journey;
  @override
  _ListTeamMemberState createState() => _ListTeamMemberState();
}

class _ListTeamMemberState extends State<ListTeamMember> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text(
          "List Companion",
          style: TextStyle(color: Colors.grey[300], fontSize: 25),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.blue[800],
                // Color(0x78FFD6),
                Colors.greenAccent
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Center(
                child: Column(
                  children: [
                    ...List.generate(widget.journey.companion.length, (index) {
                      return Column(
                        children: [
                          FlatButton(
                            onPressed: () {
                              widget.journey.companion[index].userId ==
                                      loggedUser.user.userId
                                  ? Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => UserPage()))
                                  : Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => GuestView(
                                                userId: widget.journey
                                                    .companion[index].userId,
                                              )));
                            },
                            child: Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Container(
                                      width: getWidth(0.25),
                                      height: getWidth(0.25),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: NetworkImage(widget
                                                .journey
                                                .companion[index]
                                                .userAvatar
                                                .imageLink),
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                    SizedBox(
                                      width: getWidth(0.05),
                                    ),
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            widget.journey.companion[index]
                                                    .firstName +
                                                " " +
                                                widget.journey.companion[index]
                                                    .lastName,
                                            style: TextStyle(
                                                fontSize: getWidth(0.05),
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: getWidth(0.02),
                                          ),
                                          Text(
                                            "${widget.journey.companion[index].age} years old",
                                            style: TextStyle(
                                                fontSize: getWidth(0.04),
                                                fontWeight: FontWeight.w400,
                                                color: Colors.grey),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getHeight(0.02),
                          ),
                        ],
                      );
                    }),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
