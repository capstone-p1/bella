

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/Detail/Component/DescriptionJourney.dart';
import 'package:goweefrontend/View/Detail/Component/LikeButtonDetail.dart';
import 'package:goweefrontend/View/Detail/Component/ManageTeam/ManageTeam.dart';
import 'package:goweefrontend/View/Detail/Component/Milestone/MilestoneCard.dart';
import 'package:goweefrontend/View/Detail/Component/UserInformationDetailJourney.dart';
import 'package:goweefrontend/View/Map/InProgressScreen.dart';
import 'package:goweefrontend/View/UserPage(Guest)/GuestView.dart';
import 'package:goweefrontend/View/UserPage/UserPage.dart';
import 'package:table_calendar/table_calendar.dart';
import '../../../SizeConfig.dart';
import 'CommentBox/CommentBox.dart';
import 'IntroImage.dart';

class DetailBody extends StatefulWidget {
  const DetailBody({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;

  @override
  _DetailBodyState createState() => _DetailBodyState();
}

final dataKey = new GlobalKey();

class _DetailBodyState extends State<DetailBody> {


  bool displayComment = false;

  void _changed(bool visibility) {
    setState(() {
      displayComment = !displayComment;
    });
  }

  bool displayMilestone = false;

  void _showMilestone(bool visibility) {
    setState(() {
      displayMilestone = !displayMilestone;
    });
  }


  _buildPopupCalendar(BuildContext context) {
    return new AlertDialog(
      title: Text(" Date Time "),
      content: new Container(
        width: getWidth(0.7),
        height: getHeight(0.5),
        child: Flexible(
          child: TableCalendar(
              rangeStartDay: DateTime(widget.journey.startDate.year,
                  widget.journey.startDate.month, widget.journey.startDate.day),
              rangeEndDay: DateTime(widget.journey.endDate.year,
                  widget.journey.endDate.month, widget.journey.endDate.day),
              focusedDay: DateTime(widget.journey.startDate.year,
                  widget.journey.startDate.month, widget.journey.startDate.day),
              firstDay: DateTime.utc(2010, 10, 16),
              lastDay: DateTime.utc(2030, 3, 14)),
        ),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: getHeight(0.02),
          ),
          /// IMAGE REVIEW
          IntroImage(
            journey: widget.journey,
          ),
          SizedBox(height: getHeight(0.01),),
          /// USER INFORMATION
          FlatButton(
              child: UserInformationDetailJourney(journey: widget.journey),
              onPressed:() {
                widget.journey.author.userId == loggedUser.user.userId
                    ?
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => UserPage()))
                    :
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    GuestView(userId: widget.journey.author.userId,)))
                ;
              }),
          /// DAY START DAY END
          Row(
            children: [
              SizedBox(
                width: getWidth(0.02),
              ),
              FlatButton(onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      _buildPopupCalendar(context),
                );
              },
                  child: Row(
                    children: [
                      Icon(Icons.date_range,size: getWidth(0.055),),
                      SizedBox(width: getWidth(0.01),),
                      Text(
                        "${widget.journey.startDate.day}/${widget.journey.startDate.month}/${widget.journey.startDate.year}"
                            " - "
                            "${widget.journey.endDate.day}/${widget.journey.endDate.month}/${widget.journey.endDate.year}",
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.5),
                            fontSize: getWidth(0.032),
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  )),
              SizedBox(
                width: getWidth(0.2),
              ),
              if (widget.journey.status == "In progress")
                Container(
                  width: getWidth(0.25),
                  height: getWidth(0.075),
                  decoration: kBoxDecorationInProgress,
                  child: Center(
                    child: Text("In Progress",
                        style: TextStyle(
                            fontSize: getWidth(0.03),
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[850])),
                  ),
                ),
              if (widget.journey.status == "Finished")
                Container(
                  width: getWidth(0.25),
                  height: getWidth(0.075),
                  decoration: kBoxDecorationFinished,
                  child: Center(
                    child: Text("Finished",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[850])),
                  ),
                ),
              if (widget.journey.status == "Upcoming")
                Container(
                  width: getWidth(0.25),
                  height: getWidth(0.075),
                  decoration: kBoxDecorationUpcoming,
                  child: Center(
                      child: Text(
                        "Upcoming",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.grey[850]),
                      )),
                ),
            ],
          ),
          /// DESCRIPTION FOR THE JOURNEY
          SizedBox(height: getHeight(0.01),),
          Padding(
            padding: EdgeInsets.only(left: getWidth(0.05)),
            child: DescriptionJourney(journey: widget.journey),
          ),
          SizedBox(
            height: getHeight(0.01),
          ),


          if (widget.journey.status == "In progress")
            FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                    builder: (context)=>InProgressScreen(journey: widget.journey,)));
              },
              child: Center(
                child: Container(
                  width: getWidth(0.5),
                  height: getWidth(0.08),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Colors.blue[800],
                        Colors.greenAccent
                      ],
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 4,
                        offset: Offset(0, 8), // Shadow position
                      ),
                    ],
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.map),
                      Center(
                        child: Text("Location Map",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          SizedBox(
            height: getHeight(0.02),
          ),
          /// TEAMMATE
          ManageTeam(journey: widget.journey,),
          /// MILESTONE CARD
          Padding(
              padding: EdgeInsets.only(
                left: SizeConfig.screenWidth * 0.06,
                bottom: getHeight(0.005),
                top: getHeight(0.005),
              ),
              child: Container(
                  child: widget.journey.milestones.length < 3
                      ? Column(children: [
                          ...List.generate(widget.journey.milestones.length,
                              (index) {
                                    return MilestoneCard(
                                      journey: widget.journey,
                                      milestones: widget.journey.milestones[index],
                                    );
                          }),
                        ],
                      )
                      : Column(
                          children: [
                            ...List.generate(displayMilestone?
                            widget.journey.milestones.length: 2, (index) {
                                  return MilestoneCard(
                                    milestones: widget.journey.milestones[index],
                                  );
                            } ),
                            FlatButton(
                              onPressed: () {
                                _showMilestone(true);
                                WidgetsBinding.instance.addPostFrameCallback(
                                    (_) => Scrollable.ensureVisible(
                                        dataKey.currentContext));
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                      displayMilestone
                                          ? "Short View"
                                          : " More Milestones ",
                                      style: TextStyle(
                                          fontSize: getWidth(0.036),
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold)),
                                  displayMilestone
                                      ? Icon(
                                          Icons.keyboard_arrow_up,
                                          size: 20,
                                          color: Colors.grey,
                                        )
                                      : Icon(
                                          Icons.keyboard_arrow_down,
                                          size: 20,
                                          color: Colors.grey,
                                        ),
                                ],
                              ),
                            ),
                          ],
                        ))),
          // LIKE , COMMENT , SHARE BUTTON
          Padding(
            padding: EdgeInsets.only(
              bottom: getHeight(0.001),
              left: SizeConfig.screenWidth * 0.06,
            ),
            child: Row(
              children: <Widget>[
                LikeButtonDetail(
                  journey: widget.journey,
                ),
                widget.journey.likes.length > 0
                    ? Text("${widget.journey.likes.length} " "likes")
                    : Text("0 like"),
                // SizedBox(width: 20,),
                FlatButton(
                  onPressed: () {
                    _changed(true);
                    WidgetsBinding.instance.addPostFrameCallback((_) =>
                        Scrollable.ensureVisible(dataKey.currentContext));
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        "assets/images/comment_icon.svg",
                        color: Colors.black,
                        width: getWidth(0.06),
                      ),
                      Text(
                          " ${widget.journey.comments.length.toString()} Comment ")
                    ],
                  ),
                ),
              ],
            ),
          ),
          // DISPLAY COMMENT
          displayComment
              ? Container(
                  key: dataKey,
                  child: TestMe(
                    journey: widget.journey,
                  ))
              : Container(),
        ],
      ),
    );
  }
}
