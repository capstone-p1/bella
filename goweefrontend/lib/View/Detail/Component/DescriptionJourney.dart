import 'dart:async';

import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';

import '../../../SizeConfig.dart';

class DescriptionJourney extends StatefulWidget {
  const DescriptionJourney({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;

  @override
  _DescriptionJourneyState createState() => _DescriptionJourneyState();
}

class _DescriptionJourneyState extends State<DescriptionJourney> {
  bool longDiscription = true;

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: getWidth(0.9),
      child: widget.journey.description.length <= 100
          ? Row(
              children: [
                // Flexible(
                //   child: Text(
                //     "Description: ",
                //     style: TextStyle(fontWeight: FontWeight.w600,fontSize: getWidth(0.035)),
                //   ),
                // ),
                Flexible(
                  child: new Text(
                    widget.journey.description,
                    textAlign: TextAlign.start,
                    style: TextStyle(fontSize: getWidth(0.035), fontWeight: FontWeight.normal),
                  ),
                ),
              ],
            )
          : new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Flexible(
                //   child: Text(
                //     "Description: ",
                //     style: TextStyle(fontWeight: FontWeight.w600,fontSize: getWidth(0.035)),
                //   ),
                // ),
                new Text(
                  longDiscription
                      ? (widget.journey.description.substring(0, 100) + "...")
                      : (widget.journey.description),
                  style: TextStyle(fontWeight: FontWeight.w500,fontSize: getWidth(0.035)),
                ),
                new InkWell(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new Row(
                        children: [
                          Text(
                            longDiscription
                                ? "See More Description"
                                : "Show less",
                            style: TextStyle(
                                fontSize: getWidth(0.035),
                                fontWeight: FontWeight.w600,
                                color: Colors.blue),
                          ),
                          SizedBox(width: getWidth(0.02)),
                          longDiscription
                              ? Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 20,
                                  color: Colors.green,
                                )
                              : Icon(
                                  Icons.keyboard_arrow_up,
                                  size: 20,
                                  color: Colors.green,
                                ),
                        ],
                      ),
                    ],
                  ),
                  onTap: () {
                    setState(() {
                      longDiscription = !longDiscription;
                    });
                  },
                ),
              ],
            ),
    );
  }
}
