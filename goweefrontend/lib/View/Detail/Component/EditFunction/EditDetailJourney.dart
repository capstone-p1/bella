import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/AddJourney/AddMilestone.dart';
import 'package:goweefrontend/Model/Image.dart' as Gowee;

import 'package:goweefrontend/View/AddJourney/MockVietnamCityApi.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/Detail/Component/EditFunction/EditAddMilestone.dart';
import 'package:goweefrontend/View/Detail/Component/EditFunction/EditMilestoneJourney.dart';
import 'package:goweefrontend/View/Detail/DetailJourney.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/SectionTitle.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../../../../SizeConfig.dart';

//import 'DescriptionJourney.dart';



class EditDetailJourney extends StatefulWidget {

  const EditDetailJourney({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;

  @override
  _EditDetailJourneyState createState() => _EditDetailJourneyState();
}

class _EditDetailJourneyState extends State<EditDetailJourney> {

  ///States
  final _departureTextFieldController = TextEditingController();
  final _destinationTextFieldController = TextEditingController();
  final _descriptionTextFieldController = TextEditingController();
  String _selectedStatus = '';
  DateTime selectedStartDate;
  DateTime selectedEndDate;
  var milestoneList = <Milestones>[];
  var imageList = <Gowee.Image>[];
  bool showLoader = false;

  ///State Functions
  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedStartDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedStartDate)
      setState(() {
        selectedStartDate = picked;
      });
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    print(picked);
    if (picked != null && picked != selectedEndDate)
      setState(() {
        selectedEndDate = picked;
      });
  }

  _awaitReturnValueFromEditAddMilestone(BuildContext context) async{
    // start the AddMilestone and wait for it to finish with a result
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EditAddMilestone(milestones: milestoneList,journey: widget.journey),
        ));

    // after the AddMilestone result comes back update the milestoneList
    if (result.places.isNotEmpty || result.milestoneName.isNotEmpty){ //have places or have milestone name
      setState(() {
        milestoneList.add(result);
      });
    }
  }

  //TODO: need to add image and places edit
  _awaitReturnValueFromEditMilestoneJourney(BuildContext context, Milestones milestone) async{
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EditMilestoneJourney(milestone: milestone),
        ));

    if (result.milestoneName.isNotEmpty){ //have places or have milestone name
      setState(() {
        milestone.milestoneName = result.milestoneName;
        milestone.places = result.places;
        milestone.images = result.imageList;
      });
    }

  }


  ///Http Functions
  Future<void> saveJourney(String departure, String destination, String startDate, String endDate, String description, String status,List<Milestones> milestones, List<Gowee.Image> imageList) async{
    String token = loggedUser.token;
    String url = '$baseUrl/journeys/${widget.journey.journeyId}';

    Journey journey = new Journey();
    journey.departure = departure;
    journey.destination = destination;
    journey.milestones = milestones;

    final response = await http.patch(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode({
          'departure' : departure,
          'destination': destination,
          'startDate': startDate,
          'endDate': endDate,
          'description': description,
          'status': status,
          //'images': jsonDecode(imageList.toString())
          // 'milestones' : jsonDecode(milestones.toString())
        })
    );
    print(journey.toString());

    print(response.statusCode);
    print(response.body);
  }

  Future<void> deleteMilestone(int milestoneId) async{
    String token = loggedUser.token;
    String url = '$baseUrl/milestones/$milestoneId';

    final response = await http.delete(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        }
    );
    print(response.statusCode);
    print(response.body);
  }

  Future<void> addImageToJourney(int journeyId, String imageLink) async{
    String token = loggedUser.token;
    String url = '$baseUrl/images/$journeyId';

    final response = await http.post(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        }
        , body: jsonEncode({
          'imageLink' : imageLink
        })
    );

    print(response.statusCode);
    print(response.body);
    print("add image to journey id $journeyId");
  }

  Future<void> deleteImage(int imageId) async{
    String token = loggedUser.token;
    String url = '$baseUrl/images/$imageId';
    final response = await http.delete(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        }
    );
    print(response.statusCode);
    print(response.body);
    print("delete imageId: $imageId");
  }

  uploadImage() async{
    final _storage = FirebaseStorage.instance;
    final _picker = ImagePicker();
    PickedFile image;
    //Check permissions
    await Permission.photos.request();

    var permissionStatus = Permission.photos.status;

    if (permissionStatus.isGranted != null){
      //Select image
      image = await _picker.getImage(source: ImageSource.gallery);
      var file = File(image.path);
      String filename = basename(file.path);

      setState(() {
        print("showLoader = true");
        showLoader = true;
      });

      if(image != null){
        //upload to firebase
        var snapshot = await _storage.ref().child('journeyImage/$filename').putFile(file);

        var downloadUrl = await snapshot.ref.getDownloadURL();

        setState(() {
          // journeyImageUrl = downloadUrl;
          showLoader = false;
          Gowee.Image image = new Gowee.Image.noID(downloadUrl);
          addImageToJourney(widget.journey.journeyId, downloadUrl);
          imageList.add(image);
        });
      }
      else{
        print("No path received");
      }
    }
    else{
      print("permission not granted");
    }
  }


  ///onLoad call
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _departureTextFieldController.text = widget.journey.departure;
    _destinationTextFieldController.text = widget.journey.destination;
    _descriptionTextFieldController.text = widget.journey.description;
    selectedStartDate = widget.journey.startDate;
    selectedEndDate = widget.journey.endDate;
    _selectedStatus = widget.journey.status;
    milestoneList = widget.journey.milestones;
    imageList = widget.journey.images;
    print(milestoneList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            "Editing ${widget.journey.departure} " "- " "${widget.journey.destination}",
            style: TextStyle(
                color: Colors.grey[300], fontFamily: "Concert", fontSize: 25),
          )
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ///Departure
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                    child: Text(
                      "Departure"
                      ,style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  )
                  ,
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                    child: TypeAheadField(
                      textFieldConfiguration: TextFieldConfiguration(
                        controller: _departureTextFieldController,
                        autofocus: false,
                        decoration: new InputDecoration(
                            border: OutlineInputBorder(),
                            // border: InputBorder.none,
                            // focusedBorder: InputBorder.none,
                            // enabledBorder: InputBorder.none,
                            // errorBorder: InputBorder.none,
                            // disabledBorder: InputBorder.none,
                            hintText: "Departure name"
                        ),
                      ),
                      suggestionsCallback: (pattern) async {
                        // Here you can call http call
                        return await MockVietnamCityApi.getSuggestions(pattern);
                      },
                      itemBuilder: (context,City suggestion){
                        final city = suggestion;
                        return ListTile(
                            leading: Icon(Icons.location_city_rounded),
                            title: Text(city.name)
                        );
                      },
                      onSuggestionSelected: (City suggestion){
                        final city = suggestion;
                        _departureTextFieldController.text = city.name;
                      },
                    ),
                  )
                  ])
            ),

            ///Destination
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                    child: Text(
                      "Destination"
                      ,style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  )
                  ,
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                    child: TypeAheadField(
                      textFieldConfiguration: TextFieldConfiguration(
                        controller: _destinationTextFieldController,
                        autofocus: false,
                        decoration: new InputDecoration(
                            border: OutlineInputBorder(),
                            // border: InputBorder.none,
                            // focusedBorder: InputBorder.none,
                            // enabledBorder: InputBorder.none,
                            // errorBorder: InputBorder.none,
                            // disabledBorder: InputBorder.none,
                            hintText: "Destination name"
                        ),
                      ),
                      suggestionsCallback: (pattern) async {
                        // Here you can call http call
                        return await MockVietnamCityApi.getSuggestions(pattern);
                      },
                      itemBuilder: (context,City suggestion){
                        final city = suggestion;
                        return ListTile(
                            leading: Icon(Icons.location_city_rounded),
                            title: Text(city.name)
                        );
                      },
                      onSuggestionSelected: (City suggestion){
                        final city = suggestion;
                        _destinationTextFieldController.text = city.name;
                      },
                    ),
                  )
                ]
              )
            )
            ,

            ///Description
            Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                        child: Text(
                          "Description"
                          ,style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      )
                      ,
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                        child: TextFormField(
                          controller: _descriptionTextFieldController,
                          minLines: 2,
                          maxLines: 5,
                          keyboardType: TextInputType.multiline,
                          decoration: InputDecoration(
                            hintText: 'Description',
                            hintStyle: TextStyle(
                                color: Colors.grey
                            ),
                            border: OutlineInputBorder(),
                          ),
                        ),
                      )

                    ]
                )
            )
            ,
            ///Start date and End date
            Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                child: Container(
                  child:
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Stack(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            TextButton(
                              onPressed: () => {
                                _selectStartDate(context)
                              },
                              child: Center(
                                child: Container(
                                  width: getWidth(0.45),
                                  height: getHeight(0.05),
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      colors: [
                                        Colors.blue[800],
                                        // Color(0x78FFD6),
                                        Colors.greenAccent
                                      ],
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        blurRadius: 4,
                                        offset: Offset(0, 8), // Shadow position
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: Text('From: ${selectedStartDate.toLocal().toString().split(' ')[0]}',
                                            style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                            ,
                          ],
                        )
                        ,
                        Stack(
                          children: <Widget>[
                            SizedBox(
                              height: 10.0,
                            ),
                            TextButton(
                              onPressed: () => {
                                _selectEndDate(context)
                              },
                              child: Center(
                                child: Container(
                                  width: getWidth(0.40),
                                  height: getHeight(0.05),
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      colors: [
                                        Colors.blue[800],
                                        // Color(0x78FFD6),
                                        Colors.greenAccent
                                      ],
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        blurRadius: 4,
                                        offset: Offset(0, 8), // Shadow position
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: Text('to: ${selectedEndDate.toLocal().toString().split(' ')[0]}',
                                            style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ]
                  ),
                )
            )
            ,
            ///Status
            Container(
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: Text(
                        "Journey status :"
                        ,style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                    ,
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: DropdownButton<String>(
                        hint: Text(_selectedStatus, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: getProportionateScreenHeight(14)),),
                        items: <String>['Upcoming', 'In progress', 'Finished'].map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: getProportionateScreenHeight(14)),),
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            _selectedStatus = value;
                          });
                        },
                      ),
                    )
                  ],
                )
            )
            ,

            ///List of milestones
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: Text(
                        "Milestones"
                        ,style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                    ,
                    Spacer()
                  ],
                )
                ,
                ...List.generate(milestoneList.length, (milestoneIndex) =>
                    Column(
                      children: [
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                            child: Container(
                              child: ListTile(
                                leading: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    milestoneList[milestoneIndex].images.length == 0 ?
                                    Icon(
                                      Icons.flag_rounded,
                                      color: Colors.greenAccent,
                                    )
                                        :Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                            width: getWidth(0.11),
                                            height: getHeight(0.025),
                                            child: Image.network(milestoneList[milestoneIndex].images[0].imageLink)
                                        )
                                        ,
                                        milestoneList[milestoneIndex].images.length > 1 ? Text("+${milestoneList[milestoneIndex].images.length - 1}") : SizedBox()
                                      ],
                                    )
                                    ,
                                  ],
                                ),
                                trailing: IconButton(
                                  icon: Icon(
                                    Icons.highlight_remove_rounded,
                                    color: Colors.redAccent,
                                  ),
                                  onPressed: (){
                                    ///Delete milestone
                                    deleteMilestone(milestoneList[milestoneIndex].milestoneId);
                                    setState((){milestoneList.removeAt(milestoneIndex);});
                                  },
                                )
                                  ,
                                title: Container(
                                  //margin: EdgeInsets.only(bottom: getProportionateScreenHeight(7)),
                                  child: Row(
                                    children: [
                                      Text(
                                        milestoneList[milestoneIndex].milestoneName,
                                        style: TextStyle(
                                          fontSize: getProportionateScreenHeight(17),
                                          color: Colors.black,
                                        ),
                                      ),

                                      IconButton(
                                        icon: Icon(
                                          Icons.edit,
                                          color: Colors.blueAccent,
                                        ),
                                        onPressed: (){
                                          ///Edit milestone
                                          _awaitReturnValueFromEditMilestoneJourney(context, milestoneList[milestoneIndex]);
                                        },
                                      )
                                    ],
                                  ),
                                ),
                                ///List of places
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ...List.generate(milestoneList[milestoneIndex].places.length, (placeIndex) =>
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(bottom: getProportionateScreenHeight(5),top: getProportionateScreenHeight(5)),
                                              child: Icon(
                                                Icons.location_on,
                                                color: Colors.redAccent,
                                                size: getProportionateScreenWidth(15),
                                              ),
                                            )
                                            ,
                                            Text(milestoneList[milestoneIndex].places[placeIndex].placeName ?? "",
                                                style: TextStyle(
                                                  fontSize: getProportionateScreenHeight(15),
                                                  color: Colors.black,
                                                )
                                            )
                                            ,
                                            Text(milestoneList[milestoneIndex].places[placeIndex].formattedAddress ?? ""),
                                          ],
                                        )
                                    )
                                  ],
                                ),
                              ),
                            )
                        )
                        ,
                        Divider()
                      ],
                    )
                )
              ],
            )

            ,

            ///New milestone
            Center(
                child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                    child: TextButton(
                      onPressed: () => {
                        _awaitReturnValueFromEditAddMilestone(context)
                      },
                      child: Center(
                        child: Container(
                          width: getWidth(0.5),
                          height: getHeight(0.05),
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              colors: [
                                Colors.blue[800],
                                // Color(0x78FFD6),
                                Colors.greenAccent
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[300],
                                blurRadius: 4,
                                offset: Offset(0, 8), // Shadow position
                              ),
                            ],
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.add_box_outlined,color : Colors.black)
                              ,
                              Center(
                                child: Text(milestoneList.length == 0 ? "Add milestone" : "Add more milestones",
                                    style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                )
            )
            ,

            ///List of Images
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: Text(
                    "Images"
                    ,style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )
                ,
                Column(
                  children: [
                    ...List.generate(imageList.length, (index) =>
                        Column(
                          children: <Widget>[
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                      width: getProportionateScreenWidth(300),
                                      child: Image.network(imageList[index].imageLink)
                                  )
                                  ,
                                  Center(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.highlight_remove_rounded,
                                          color: Colors.redAccent,
                                        ),
                                        onPressed: (){
                                          //Remove that image
                                          deleteImage(imageList[index].imageId);
                                          setState((){imageList.removeAt(index);});
                                        },
                                      )
                                  )
                                ]
                            )
                            ,Divider()
                          ],
                        )
                    )
                    ,

                    Center(child: Visibility(visible: showLoader,child: CircularProgressIndicator()))
                  ],
                )
                ,
              ],
            )
            ,
            ///Add photos button
            Center(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TextButton(
                    onPressed: () => {
                      uploadImage()
                    },
                    child: Center(
                      child: Container(
                        width: getWidth(0.5),
                        height: getHeight(0.05),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              Colors.blue[800],
                              // Color(0x78FFD6),
                              Colors.greenAccent
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              blurRadius: 4,
                              offset: Offset(0, 8), // Shadow position
                            ),
                          ],
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.add_photo_alternate_outlined,color: Colors.black)
                            ,
                            Center(
                              child: Text(imageList.length == 0 ? "Add photo" : "Add more photos",
                                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
            )
            ,
            ///Save journey
            Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: TextButton(
                onPressed: (){
                  saveJourney(_departureTextFieldController.text,
                      _destinationTextFieldController.text,
                      selectedStartDate.toLocal().toString().split(' ')[0],
                      selectedEndDate.toLocal().toString().split(' ')[0],
                      _descriptionTextFieldController.text,
                      _selectedStatus,
                      milestoneList,
                      imageList
                  );
                  Navigator.pop(context);
                },
                child: Center(
                  child: Container(
                    width: getWidth(0.45),
                    height: getHeight(0.05),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          Colors.blue[800],
                          // Color(0x78FFD6),
                          Colors.greenAccent
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[300],
                          blurRadius: 4,
                          offset: Offset(0, 8), // Shadow position
                        ),
                      ],
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.check_circle_outline, color: Colors.black),
                                  Text('Save journey',
                                      style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                ]
                            )
                        ),
                      ],
                    ),
                  ),
                ),
              )
              ,
            )
        )
            ,
            Center(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TextButton(
                    child: Text("Debug"),
                    onPressed: ()=>{
                    },
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.black)
                    ),
                  ),
                )
            )

          ],
        ),
      )
    );
  }
}
