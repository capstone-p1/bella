import 'dart:convert';
import 'dart:io';

import 'package:goweefrontend/Model/Address.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Image.dart' as Gowee;
import 'package:flutter_typeahead/flutter_typeahead.dart';

import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/Model/Place.dart';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart';



import 'package:goweefrontend/View/AddJourney/MockVietnamCityApi.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';

import '../../../../Constants.dart';
import '../../../../SizeConfig.dart';


// class Milestone{
//   int milestoneId;
//   String milestoneName;
//   List<Image> images;
//   List<Place> places;
//
//   Milestone(this.milestoneName, this.images, this.places);
//
//   @override
//   String toString() {
//     return '{"milestoneName": "$milestoneName", "images": $images, "places": $places}';
//   }
// }

class EditAddMilestone extends StatefulWidget {


  final Journey journey;
  final List<Milestones> milestones;

  const EditAddMilestone({Key key, this.milestones, this.journey}) : super(key: key);

  @override
  _EditAddMilestoneState createState() => _EditAddMilestoneState();
}

class _EditAddMilestoneState extends State<EditAddMilestone> {

  List places = <Place>[];
  var imageList = <Gowee.Image>[];
  final _milestoneNameTextFieldController = TextEditingController();
  final _placeTextFieldController = TextEditingController();
  final _imageURLTextFieldController = TextEditingController();
  final _placeNameManualTextFieldController = TextEditingController();
  final _placeAddressManualTextFieldController = TextEditingController();
  bool showLoader = false;


  Future<void> addMilestone(int journeyId) async{
    String token = loggedUser.token;
    String url = '$baseUrl/milestones/$journeyId';

    final response = await http.post(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        }
        , body: jsonEncode({
          'milestoneName' : _milestoneNameTextFieldController.text,
          'places' : jsonDecode(places.toString()),
          'images': jsonDecode(imageList.toString()),
        })
    );

    print(response.statusCode);
    print(response.body);
  }

  uploadImage() async{
    final _storage = FirebaseStorage.instance;
    final _picker = ImagePicker();
    PickedFile image;
    //Check permissions
    await Permission.photos.request();

    var permissionStatus = Permission.photos.status;

    if (permissionStatus.isGranted != null){
      //Select image
      image = await _picker.getImage(source: ImageSource.gallery);
      var file = File(image.path);
      String filename = basename(file.path);

      setState(() {
        print("show loader true");
        showLoader = true;
      });

      if(image != null){
        //upload to firebase
        var snapshot = await _storage.ref().child('milestoneImage/$filename').putFile(file);

        var downloadUrl = await snapshot.ref.getDownloadURL();

        setState(() {
          // journeyImageUrl = downloadUrl;
          showLoader = false;
          Gowee.Image image = new Gowee.Image.noID(downloadUrl);
          imageList.add(image);
        });
      }
      else{
        print("No path received");
      }
    }
    else{
      print("permission not granted");
    }
  }

  void _sendDataBackToJourneyForm(BuildContext context) {
    Milestones returnMilestone = new Milestones.noID(_milestoneNameTextFieldController.text,imageList,places);
    if (returnMilestone.places.isNotEmpty || returnMilestone.milestoneName.isNotEmpty){
      addMilestone(widget.journey.journeyId);
      Navigator.pop(context, returnMilestone);
    }
    else{
      Navigator.pop(context);
    }
  }

  void _addPlaceManually(){
    var placeName = _placeNameManualTextFieldController.text;
    var placeFormattedAddress = _placeAddressManualTextFieldController.text;
    print('how is this? $placeFormattedAddress');
    if (placeName != '' && placeFormattedAddress != ''){
      var addressLine = '';
      var city = '';
      var country= '';
      try {
        var strArray = placeFormattedAddress.split(',');
        print('Splitting array $strArray');
        for( var i = 0; i < strArray.length - 2; i++){
          addressLine += strArray[i];
          if (i != strArray.length - 3){
            addressLine += ',';
          }
        }
        addressLine = addressLine;
        country = strArray[strArray.length - 1].trim();
        city = strArray[strArray.length - 2].trim();
      } on Exception catch(_){
        print("error");
      }

      Place place = new Place.noID(placeName, new Address.noID(addressLine,city,country));
      place.formattedAddress = placeFormattedAddress;

      //Place place = new Place.noID(placeName, new Address.fromFormattedAddress(placeFormattedAddress));
      print('created place ${place.toString()}');
      setState(() {
        places.add(place);
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(
            title: Text(
                "Add a new milestone"
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ///Start of milestone
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: TypeAheadField(
                        textFieldConfiguration: TextFieldConfiguration(
                          controller: _milestoneNameTextFieldController,
                          autofocus: false,
                          decoration: new InputDecoration(
                              border: OutlineInputBorder(),
                              // border: InputBorder.none,
                              // focusedBorder: InputBorder.none,
                              // enabledBorder: InputBorder.none,
                              // errorBorder: InputBorder.none,
                              // disabledBorder: InputBorder.none,
                              hintText: "Milestone name"
                          ),
                        ),
                        suggestionsCallback: (pattern) async {
                          // Here you can call http call
                          return await MockVietnamCityApi.getSuggestions(pattern);
                        },
                        itemBuilder: (context,City suggestion){
                          final city = suggestion;
                          return ListTile(
                              leading: Icon(Icons.location_city_rounded),
                              title: Text(city.name)
                          );
                        },
                        onSuggestionSelected: (City suggestion){
                          final city = suggestion;
                          _milestoneNameTextFieldController.text = city.name;
                        },
                      ),
                    )

                  ],
                )
                ,

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: Text(
                    "Places"
                    ,style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )
                ,
                ///Start of list of places
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...List.generate(places.length, (index) =>
                        Column(
                          children: [
                            Padding(
                                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                                child: Container(
                                  child: ListTile(
                                    leading: Icon(
                                      Icons.location_on,
                                      color: Colors.redAccent,
                                    ),
                                    trailing: IconButton(
                                        icon: Icon(
                                          Icons.highlight_remove_rounded,
                                          color: Colors.redAccent,
                                        ),
                                        onPressed: ()=>{
                                          //Remove that place
                                          setState((){places.removeAt(index);})
                                        }),
                                    title: Container(
                                      margin: EdgeInsets.only(bottom: getProportionateScreenHeight(7)),
                                      child: Text(
                                        places[index].placeName ?? '',
                                        style: TextStyle(
                                          fontSize: getProportionateScreenHeight(17),
                                          color: Colors.black,
                                        ),
                                      ),
                                    ),
                                    subtitle: Text(places[index].formattedAddress ?? '${places[index].address.addressLine},${places[index].address.city},${places[index].address.country}' ?? ''),
                                  ),
                                )
                            )
                            ,
                            Divider()
                          ],
                        )
                    )
                  ],
                )
                ,
                ///Start of place search bar
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: TypeAheadField(
                    textFieldConfiguration: TextFieldConfiguration(
                      controller: _placeTextFieldController,
                      autofocus: false,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Search Google for places',
                          suffixIcon: Icon(Icons.search)
                      ),
                    ),
                    suggestionsCallback: (pattern) async {
                      // Here you can call http call
                      return await GoogleMapApi.search(pattern);
                    },
                    itemBuilder: (context,Place suggestion){
                      final place = suggestion;
                      return Container(
                        child: ListTile(
                          leading: Icon(
                            Icons.location_on,
                            color: Colors.redAccent,
                          ),
                          title: Container(
                            margin: EdgeInsets.only(bottom: 7),
                            child: Text(
                              place.placeName,
                              style: TextStyle(
                                fontSize: 17,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          subtitle: Text(place.formattedAddress),
                        ),
                      );
                    },
                    onSuggestionSelected: (Place suggestion){
                      final location = suggestion;
                      setState(() {
                        places.add(location);
                      });
                      //_placeTextFieldController.text = "${location.name} ${location.formattedAddress}";
                    },
                    noItemsFoundBuilder: (context) => Container(
                      height: 50,
                      child: Center(
                        child: Text(
                          'No location found',
                          style: TextStyle(fontSize: 24),
                        ),
                      ),
                    ),
                  ),
                )
                ,

                ///Start of manual place enter
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: Text(
                        "OR"
                        ,style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                    ,
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: Text(
                        "Add places manually"
                        ,style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                    ,

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: Text(
                          "Place name"
                      ),
                    )
                    ,
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: TextField(
                        controller: _placeNameManualTextFieldController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: "Place name"
                        ),
                      ),
                    )
                    ,

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: Text(
                          "Formatted address"
                      ),
                    )
                    ,
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: TextField(
                        controller: _placeAddressManualTextFieldController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: "e.g. 123 Tran Hung Dao, Phan Thiet, Vietnam"
                        ),
                      ),
                    )
                  ],
                )
                ,

                Center(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: TextButton(
                        onPressed: () => {
                          _addPlaceManually()
                        },
                        child: Center(
                          child: Container(
                            width: getWidth(0.5),
                            height: getHeight(0.05),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Colors.blue[800],
                                  // Color(0x78FFD6),
                                  Colors.greenAccent
                                ],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[300],
                                  blurRadius: 4,
                                  offset: Offset(0, 8), // Shadow position
                                ),
                              ],
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.add_location_alt_outlined,color: Colors.black)
                                ,
                                Center(
                                  child: Text(places.length == 0 ? "Add place" : "Add more places",
                                      style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                )
                ,

                ///List of Images
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: Text(
                    "Images"
                    ,style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )
                ,
                Column(
                  children: [
                    ...List.generate(imageList.length, (index) =>
                        Column(
                          children: <Widget>[
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                      width: getProportionateScreenWidth(300),
                                      child: Image.network(imageList[index].imageLink))
                                  ,
                                  Center(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.highlight_remove_rounded,
                                          color: Colors.redAccent,
                                        ),
                                        onPressed: ()=>{
                                          //Remove that image
                                          setState((){imageList.removeAt(index);})
                                        },
                                      )
                                  )
                                ]
                            )
                            ,Divider()
                          ],
                        )
                    )
                    ,
                    Center(child: Visibility(visible: showLoader,child: CircularProgressIndicator()),)
                  ],
                )
                ,
                ///Add photos
                Center(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: TextButton(
                        onPressed: () => {
                          uploadImage()
                        },
                        child: Center(
                          child: Container(
                            width: getWidth(0.5),
                            height: getHeight(0.05),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Colors.blue[800],
                                  // Color(0x78FFD6),
                                  Colors.greenAccent
                                ],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[300],
                                  blurRadius: 4,
                                  offset: Offset(0, 8), // Shadow position
                                ),
                              ],
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.add_photo_alternate_outlined,color: Colors.black)
                                ,
                                Center(
                                  child: Text(imageList.length == 0 ? "Add photo" : "Add more photos",
                                      style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                )
                ,

                ///Start of upload images
                // Column(
                //   crossAxisAlignment: CrossAxisAlignment.start,
                //   children: [
                //     Padding(
                //       padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                //       child: Text(
                //         "Image links"
                //         ,style: TextStyle(fontWeight: FontWeight.bold),
                //       ),
                //     )
                //     ,
                //     Padding(
                //       padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                //       child: TextField(
                //         controller: _imageURLTextFieldController,
                //         decoration: InputDecoration(
                //             border: OutlineInputBorder(),
                //             hintText: "Image URL here",
                //             suffixIcon: IconButton(
                //               icon: Icon(Icons.upload_rounded),
                //               onPressed: () => {
                //                 //Do something?
                //               },
                //             )
                //         ),
                //       ) ,
                //     )
                //   ],
                // )
                // ,
                ///Add milestone button
                Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: TextButton(
                        onPressed: () => {
                          _sendDataBackToJourneyForm(context)
                        },
                        child: Center(
                          child: Container(
                            width: getWidth(0.45),
                            height: getHeight(0.05),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Colors.blue[800],
                                  // Color(0x78FFD6),
                                  Colors.greenAccent
                                ],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[300],
                                  blurRadius: 4,
                                  offset: Offset(0, 8), // Shadow position
                                ),
                              ],
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Center(
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Icon(Icons.add_circle_outline, color: Colors.black),
                                          Text('Add milestone',
                                              style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                        ]
                                    )
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                      ,
                    )
                )
                ,
                SizedBox(height: getHeight(0.05))
              ],
            ),
          )
      );
  }
}
