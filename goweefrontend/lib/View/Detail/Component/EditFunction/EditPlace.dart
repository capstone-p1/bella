import 'dart:convert';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:goweefrontend/Model/Address.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Place.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';

import '../../../../Constants.dart';
import '../../../../SizeConfig.dart';

class EditPlace extends StatefulWidget {
  const EditPlace({
    Key key,
    @required this.place
  }) : super(key: key);
  final Place place;

  @override
  _EditPlaceState createState() => _EditPlaceState();
}

class _EditPlaceState extends State<EditPlace> {
  final _placeTextFieldController = TextEditingController();
  final _placeNameManualTextFieldController = TextEditingController();
  final _placeAddressManualTextFieldController = TextEditingController();



  ///onLoad call
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _placeTextFieldController.text = widget.place.formattedAddress;
    _placeNameManualTextFieldController.text = widget.place.placeName;
    _placeAddressManualTextFieldController.text = "${widget.place.address.addressLine},${widget.place.address.city},${widget.place.address.city}";

  }

  void _sendDataBackToEditDetailMilestone(BuildContext context) {
    var placeFormattedAddress = _placeAddressManualTextFieldController.text;
    var addressLine = '';
    var city = '';
    var country= '';

    try {
      var strArray = placeFormattedAddress.split(',');
      print('Splitting array $strArray');
      for( var i = 0; i < strArray.length - 2; i++){
        addressLine += strArray[i];
        if (i != strArray.length - 3){
          addressLine += ',';
        }
      }
      addressLine = addressLine;
      country = strArray[strArray.length - 1].trim();
      city = strArray[strArray.length - 2].trim();
    } on Exception catch(_){
      print("error");
    }
    Place returnPlace = new Place.noID(_placeNameManualTextFieldController.text, new Address.noID(addressLine,city,country));
    if (returnPlace.placeName.isNotEmpty){
      Navigator.pop(context, returnPlace);
    }
    else{
      Navigator.pop(context);
    }
  }

  /// Http
  Future<void> editPlace(int placeId) async{
    String token = loggedUser.token;
    String url = '$baseUrl/places/$placeId';

    ///Address stuff
    var placeFormattedAddress = _placeAddressManualTextFieldController.text;
    var addressLine = '';
    var city = '';
    var country= '';
    try {
      var strArray = placeFormattedAddress.split(',');
      print('Splitting array $strArray');
      for( var i = 0; i < strArray.length - 2; i++){
        addressLine += strArray[i];
        if (i != strArray.length - 3){
          addressLine += ',';
        }
      }
      addressLine = addressLine;
      country = strArray[strArray.length - 1].trim();
      city = strArray[strArray.length - 2].trim();
    } on Exception catch(_){
      print("error");
    }

    final response = await http.patch(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        }
        ,
        body: jsonEncode({
          'placeName' : _placeNameManualTextFieldController.text,
          'address': jsonDecode(Address.noID(addressLine, city, country).toString())
        })
    );
    print(response.statusCode);
    print(response.body);

    _sendDataBackToEditDetailMilestone(context);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("edit Place"),
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
            child: Text(
              "Places"
              ,style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
          ,
          Padding(
            padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
            child: TypeAheadField(
              textFieldConfiguration: TextFieldConfiguration(
                // controller: _placeTextFieldController,
                autofocus: false,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Search Google for places',
                    suffixIcon: Icon(Icons.search)
                ),
              ),
              suggestionsCallback: (pattern) async {
                // Here you can call http call
                return await GoogleMapApi.search(pattern);
              },
              itemBuilder: (context,Place suggestion){
                final place = suggestion;
                return Container(
                  child: ListTile(
                    leading: Icon(
                      Icons.location_on,
                      color: Colors.redAccent,
                    ),
                    title: Container(
                      margin: EdgeInsets.only(bottom: 7),
                      child: Text(
                        place.placeName,
                        style: TextStyle(
                          fontSize: 17,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    subtitle: Text(place.formattedAddress),
                  ),
                );
              },
              onSuggestionSelected: (Place suggestion){
                final location = suggestion;
                setState(() {
                });
                // _placeTextFieldController.text = "${location.name} ${location.formattedAddress}";
              },
              noItemsFoundBuilder: (context) => Container(
                height: 50,
                child: Center(
                  child: Text(
                    'No location found',
                    style: TextStyle(fontSize: 24),
                  ),
                ),
              ),
            ),
          )
          ,

          ///Start of manual place enter
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                child: Text(
                  "Add places manually"
                  ,style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )
              ,

              Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                child: Text(
                    "Place name"
                ),
              )
              ,
              Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                child: TextField(
                  controller: _placeNameManualTextFieldController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Place name"
                  ),
                ),
              )
              ,

              Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                child: Text(
                    "Formatted address"
                ),
              )
              ,
              Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                child: TextField(
                  controller: _placeAddressManualTextFieldController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "e.g. 123 Tran Hung Dao, Phan Thiet, Vietnam"
                  ),
                ),
              ),
              Center(
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: TextButton(
                      child: Text("Add place"),
                      onPressed: ()=>{
                        editPlace(widget.place.placeId)
                      },
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
                          foregroundColor: MaterialStateProperty.all<Color>(Colors.black)
                      ),
                    ),
                  )
              )
            ],
          )
          ,
        ],
      )
    );
  }
}
