import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/Detail/Component/Milestone/ImageControler/AlbumImage.dart';

class MoreImagePopUp extends StatefulWidget {
  const MoreImagePopUp({Key key, @required this.milestone}) : super(key: key);
  final Milestones milestone;
  @override
  _MoreImagePopUpState createState() => _MoreImagePopUpState();
}

class _MoreImagePopUpState extends State<MoreImagePopUp> {
  @override
  Widget build(BuildContext context) {
    final title = widget.milestone.milestoneName;

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Colors.grey,
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.camera_enhance_sharp))
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.blue[800], Colors.greenAccent],
            ),
          ),
        ),
      ),
      body: GridView.count(
          crossAxisCount: 3,
          children: List.generate(
              widget.milestone.images.length,
              (index) => GestureDetector(
                    onTap: () {
                      setState(() {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) => AlbumImage(
                            milestone: widget.milestone,
                            selectedImages: index,
                          ),
                        );
                      });
                    },
                    child: SizedBox(
                      height: getWidth(0.1),
                      width: getProportionateScreenWidth(60),
                      child: Image.network(
                        widget.milestone.images[index].imageLink,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ))),
    );
  }
}
