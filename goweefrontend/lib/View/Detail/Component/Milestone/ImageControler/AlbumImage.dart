import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:goweefrontend/Model/Milestones.dart';

import '../../../../../SizeConfig.dart';
import '../MilestonePicture.dart';

class AlbumImage extends StatefulWidget {
  const AlbumImage(
      {Key key, @required this.milestone, @required this.selectedImages})
      : super(key: key);
  final Milestones milestone;
  final int selectedImages;
  @override
  _AlbumImageState createState() => _AlbumImageState();
}

class _AlbumImageState extends State<AlbumImage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedImage = widget.selectedImages;
  }


  final CarouselController _controller = CarouselController();
  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = widget.milestone.images
        .map((item) => Container(
              width: getProportionateScreenWidth(600),
              height: getProportionateScreenWidth(500),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(item.imageLink),
                      fit: BoxFit.fitWidth)),
            ))
        .toList();
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text(widget.milestone.milestoneName),
          backgroundColor: Colors.black,
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.blue[800],
                  // Color(0x78FFD6),
                  Colors.greenAccent
                ],
              ),
            ),
          ),

        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              CarouselSlider(
                items: imageSliders,
                options: CarouselOptions(
                    height: getProportionateScreenHeight(600),
                    viewportFraction: 1.0,
                    enlargeCenterPage: false,
                    initialPage: widget.selectedImages,
                    onPageChanged: (index, reason) {
                      setState(() {
                        selectedImage = index;
                      });
                    }),
                carouselController: _controller,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: <Widget>[
                    ...Iterable<int>.generate(widget.milestone.images.length).map(
                      (pageIndex) => RaisedButton(
                        color: Colors.black,
                        padding: EdgeInsets.zero,
                        onPressed: () {
                          _controller.animateToPage(pageIndex);
                          setState(() {
                            selectedImage = pageIndex;
                          });
                        },
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 250),
                          height: getProportionateScreenWidth(80),
                          width: getProportionateScreenWidth(80),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.pink.withOpacity(
                                    selectedImage == pageIndex ? 1 : 0)),
                          ),
                          child: Image.network(
                            widget.milestone.images[pageIndex].imageLink,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
