import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:goweefrontend/Model/Place.dart';

import '../../../../../../SizeConfig.dart';

class PlaceCard extends StatefulWidget {
  const PlaceCard({
    Key key,
    @required this.places,
  });
  final Place places;

  @override
  _PlaceCardState createState() => _PlaceCardState();
}

class _PlaceCardState extends State<PlaceCard> {
  double placeHeight = 0.0;
  GlobalKey _globalKeyPlace = GlobalKey();

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      placeHeight = _globalKeyPlace.currentContext.size.height;
      print('the new height is $placeHeight');
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              children: [
                Icon(
                  Icons.location_pin,
                  size: getWidth(0.05),
                ),
                SizedBox(
                  width: getWidth(0.01),
                ),
                Row(
                  children: [
                    SingleChildScrollView(
                      child: Text(widget.places.placeName,
                          style: TextStyle(
                              fontSize: getWidth(0.04), fontWeight: FontWeight.bold)),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: getHeight(0.01),
          ),
          Row(
            children: [
              SizedBox(width: getWidth(0.025),),
              Dash(
                  direction: Axis.vertical,
                  length: placeHeight,
                  dashLength: 5,
                  dashColor: Colors.black),
              Container(
                // width: getWidth(0.895),
                key: _globalKeyPlace,
                padding: const EdgeInsets.all(10.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // PlacePicture(places: widget.places),
                      SizedBox(
                        height: getWidth(0.02),
                      ),
                      Row(
                        children: [
                          widget.places.avgPrice != null
                              ? Text(
                                  "Price: ₫ ${widget.places.avgPrice}",
                                  style: TextStyle(
                                      fontSize: getWidth(0.035),
                                      fontWeight: FontWeight.bold),
                                )
                              : Text(
                                  "Price: ₫ 0",
                                  style: TextStyle(
                                      fontSize: getWidth(0.035),
                                      fontWeight: FontWeight.bold),
                                )
                        ],
                      ),
                      Container(
                        width: getWidth(0.7),
                        child: Text(
                          "Address: ${widget.places.address.addressLine},${widget.places.address.city},${widget.places.address.country}",
                          style: TextStyle(
                              fontSize: getWidth(0.035),
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ]),
              ),
            ],
          )
        ],
      ),
    );
  }
}
