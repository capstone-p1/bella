import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/Model/Place.dart';
import 'package:goweefrontend/View/Detail/Component/Milestone/DetailMilestone/Place/PlaceCard.dart';

import '../../../../../Services.dart';
import '../../../../../SizeConfig.dart';

class MilestonePlace extends StatefulWidget {
  const MilestonePlace({
    Key key,
    @required this.milestones,
  }) : super(key: key);
  final Milestones milestones;


  @override
  _MilestonePlaceState createState() => _MilestonePlaceState();
}

class _MilestonePlaceState extends State<MilestonePlace> {
  var places = new List<Place>();
  _getPlace() {
    PlaceAPI.getPlace().then((response) {
      setState(() {
        Iterable list = json.decode(response.body);
        places = list.map((model) => Place.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getPlace();
  }

  bool displayMilestone = false;

  void _showMilestone(bool visibility) {
    setState(() {
      displayMilestone = !displayMilestone;
    });
  }
  @override
  Widget build(BuildContext context) {
    List placeId = [];
    List.generate(
        widget.milestones.places.length >2
            ? displayMilestone? widget.milestones.places.length : 2
            : widget.milestones.places.length, (index) =>
        placeId.add(widget.milestones.places[index].placeId));
    return Padding(
        padding: EdgeInsets.only(
          left: SizeConfig.screenWidth * 0.05,
          bottom: getWidth(0.01),
          top: getWidth(0.015),
        ),

        child:  Container(
          child:
          widget.milestones.places.length < 3
          ? Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...List.generate( places.length , (index) {
                  if (placeId.contains(places[index].placeId)) {
                    return PlaceCard(
                      places: places[index],
                    );
                }
                return Container();
              }),

            ],
          )
          : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...List.generate(places.length, (index) {
                  if (placeId.contains(places[index].placeId)) {
                    return PlaceCard(
                      places: places[index],
                    );

                }
                return Container();
              }),
              FlatButton(
                onPressed: () {
                  _showMilestone(true);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(displayMilestone ? "Short View" :" More Place ", style: TextStyle(fontSize: getWidth(0.036),color:Colors.grey, fontWeight: FontWeight.bold)),
                    displayMilestone ?
                    Icon(
                      Icons.keyboard_arrow_up,
                      size: 20,
                      color: Colors.grey,
                    ):
                    Icon(
                      Icons.keyboard_arrow_down,
                      size: 20,
                      color: Colors.grey,
                    ),

                  ],
                ),
              ),
            ],
          ),
        ));

  }
}

