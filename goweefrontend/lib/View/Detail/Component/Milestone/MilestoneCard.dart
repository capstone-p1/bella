import 'dart:async';

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_dash/flutter_dash.dart';

import 'package:goweefrontend/Model/Journey.dart';

import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/Detail/Component/Milestone/DetailMilestone/Place/PlaceCard.dart';
import 'package:goweefrontend/View/Detail/Component/Milestone/MilestonePicture.dart';

import 'DetailMilestone/MilestonePlace.dart';

class MilestoneCard extends StatefulWidget {
  const MilestoneCard(
      {Key key,
      @required this.milestones,
      @required this.milestonePress,
      @required this.journey});
  final Milestones milestones;
  final Journey journey;
  final GestureTapCallback milestonePress;

  @override
  _MilestoneCardState createState() => _MilestoneCardState();
}

class _MilestoneCardState extends State<MilestoneCard> {
  double heightMilestone = 0.0;
  GlobalKey _globalKey = GlobalKey();
  var sumPlacePrice = 0;

  Timer timer;
  @override
  void initState() {

    timer = new Timer.periodic(Duration(seconds: 1), (Timer t) {
      if(this.mounted){
      SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
        heightMilestone = _globalKey.currentContext.size.height;
      });
    }});
    List.generate(widget.milestones.places.length, (index) {
      if (widget.milestones.places[index].avgPrice != null) {
        sumPlacePrice += widget.milestones.places[index].avgPrice;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(children: [
          Icon(
            Icons.flag,
            size: getWidth(0.1),
          ),
          Text(widget.milestones.milestoneName,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          Icon(Icons.motorcycle)
        ]),
        SizedBox(
          height: getHeight(0.01),
        ),
        Padding(
          padding: EdgeInsets.only(left: getWidth(0.04)),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                DottedLine(
                  dashLength: 10,
                  dashGapLength: 2,
                  lineThickness: 1,
                  direction: Axis.vertical,
                  lineLength: heightMilestone,
                ),
                Container(
                  key: _globalKey,
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MilestonePicture(
                        milestones: widget.milestones,
                        journeys: widget.journey,
                      ),
                      SizedBox(
                        height: getWidth(0.02),
                      ),
                      // Text("Days: Update Later", style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600),),
                      Text(
                        "Total Price: ₫  $sumPlacePrice",
                        style: TextStyle(
                            fontSize: getWidth(0.035),
                            fontWeight: FontWeight.w600),
                      ),
                      Container(
                        width: getWidth(0.9),
                        child: MilestonePlace(milestones: widget.milestones,)
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        // Text(milestones.images.length.toString()),
      ],
    );
  }
}
