import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/Milestones.dart';

import 'package:goweefrontend/View/Detail/Component/Milestone/ImageControler/MoreImagePopUp.dart';

import '../../../../Constants.dart';
import '../../../../SizeConfig.dart';

class MilestonePicture extends StatefulWidget {
  const MilestonePicture(
      {Key key, @required this.milestones, @required this.journeys});
  final Milestones milestones;
  final Journey journeys;
  @override
  _MilestonePictureState createState() => _MilestonePictureState();
}

int selectedImage = 0;

class _MilestonePictureState extends State<MilestonePicture> {
  final CarouselController _controller = CarouselController();

  _buildPopupImageDetail(BuildContext context) {
    final List<Widget> imageSliders = widget.milestones.images
        .take(4)
        .map((item) => Container(
              width: getWidth(0.9),
              height: getWidth(0.8),
              decoration: BoxDecoration(
                  color: Colors.grey[350],
                  image: DecorationImage(
                      image: NetworkImage(item.imageLink),
                      fit: BoxFit.fitWidth)),
            ))
        .toList();
    return new AlertDialog(
      backgroundColor: Colors.grey[350],
      insetPadding:
          EdgeInsets.only(left: getWidth(0.03), right: getWidth(0.03)),
      contentPadding: EdgeInsets.zero,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      title: Text(widget.milestones.milestoneName),
      content: Container(
        width: getWidth(0.92),
        height: getHeight(0.6),
        child: Flexible(
          child: CarouselSlider(
            items: imageSliders,
            options: CarouselOptions(
                height: getHeight(0.6),
                viewportFraction: 1.0,
                enlargeCenterPage: false,
                initialPage: selectedImage,
                onPageChanged: (index, reason) {
                  setState(() {
                    selectedImage = index;
                  });
                }),
            carouselController: _controller,
          ),
        ),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        // crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ...List.generate(
              widget.milestones.images.length <= 3
                  ? widget.milestones.images.length
                  : 3,
              (index) => buildSmallProductPreview(index)),
          widget.milestones.images.length >= 4 ? MoreImage() : Text(""),
        ],
      ),
    );
  }

  /// Trailer image
  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
          showDialog(
            context: context,
            builder: (BuildContext context) => _buildPopupImageDetail(context),
          );
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        margin: EdgeInsets.only(right: 3),
        height: getWidth(0.16),
        width: getWidth(0.16),
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(1),
          border: Border.all(
              color: kPrimaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: Image.network(
          widget.milestones.images[index].imageLink,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  /// Open Album
  GestureDetector MoreImage() {
    return GestureDetector(
        onTap: () {
          setState(() {
            selectedImage = 3;
            showDialog(
                context: context,
                builder: (BuildContext context) =>
                    widget.milestones.images.length != 4
                        ? MoreImagePopUp(
                            milestone: widget.milestones,
                          )
                        : _buildPopupImageDetail(context));
          });
        },
        child: Stack(children: [
          AnimatedContainer(
            duration: Duration(milliseconds: 300),
            padding: EdgeInsets.all(3),
            height: getWidth(0.16),
            width: getWidth(0.16),
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(1),
              border: Border.all(
                  color: kPrimaryColor.withOpacity(selectedImage == 3 ? 1 : 0)),
            ),
            child: Image.network(widget.milestones.images[3].imageLink,
                fit: BoxFit.cover),
          ),
          widget.milestones.images.length != 3
              ? ClipRRect(
                  // Clip it cleanly.
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 1, sigmaY: 1),
                    child: Container(
                      height: getWidth(0.16),
                      width: getWidth(0.16),
                      alignment: Alignment.center,
                      child: Text(
                        "${widget.milestones.images.length - 4} " "+",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                )
              : ClipRRect(// Clip it cleanly.
                  )
        ]));
  }
}
