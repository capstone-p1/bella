import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/View/Detail/Component/EditFunction/EditDetailJourney.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreen.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/SectionTitle.dart';
import 'package:http/http.dart' as http;
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';

import '../../Constants.dart';
import '../../Services.dart';
import 'Component/DetailBody.dart';

class DetailsJourneyScreen extends StatefulWidget {
  static String routeName = "/details";
  const DetailsJourneyScreen({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;

  static void restartData(BuildContext context) {
    context
        .findAncestorStateOfType<_DetailsJourneyScreenState>()
        .fetchJourney();
  }

  @override
  _DetailsJourneyScreenState createState() => _DetailsJourneyScreenState();
}

class _DetailsJourneyScreenState extends State<DetailsJourneyScreen> {
  Timer timer;
  var journeyDetails = new List<Journey>();

  Future fetchJourney() async {
    final response = await http.get(Uri.parse("$baseUrl/journeys"));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Iterable list = jsonDecode(response.body);
      return journeyDetails =
          list.map((model) => Journey.fromJson(model)).toList();
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Stream getJourney(Duration refreshTime) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await fetchJourney();
    }
  }

  initState() {
    super.initState();
    fetchJourney();
  }

  Future<http.Response> deleteJourney(int journeyId) async {
    String token = loggedUser.token; // how to get token?
    final http.Response response = await http.delete(
      Uri.parse('$baseUrl/journeys/$journeyId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
    );
    print(response.statusCode);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: getJourney(Duration(milliseconds: 1)),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            int index;
            for (index = 0; index <= journeyDetails.length; index++) {
              if (journeyDetails.length != 0) {
                // check if journey details not null
                if (journeyDetails[index].journeyId ==
                    widget.journey.journeyId) {
                  return Scaffold(
                    appBar: AppBar(
                        // backgroundColor: Colors.green[100],
                        title: Text(
                          "${journeyDetails[index].departure} "
                          "- "
                          "${journeyDetails[index].destination}",
                          style:
                              TextStyle(color: Colors.grey[300], fontSize: 25),
                        ),
                        iconTheme: IconThemeData(
                          color: Colors.black, //change your color here
                        ),
                        leading: IconButton(
                          icon: Icon(Icons.arrow_back, color: Colors.black),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        flexibleSpace: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              colors: [Colors.blue[800], Colors.greenAccent],
                            ),
                          ),
                        ),
                        actions: [
                          loggedUser.token == null
                              ? IconButton(
                                  icon: Icon(Icons.share), onPressed: () {})
                              : loggedUser.user.userId ==
                                      journeyDetails[index].author.userId
                                  ? IconButton(
                                      icon: Icon(Icons.menu),
                                      onPressed: () {
                                        showModalBottomSheet(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.vertical(
                                                top: Radius.circular(20),
                                              ),
                                            ),
                                            context: context,
                                            builder: (context) {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10, bottom: 10),
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    ListTile(
                                                      leading:
                                                          new Icon(Icons.edit),
                                                      title: new Text(
                                                          'Edit Journey'),
                                                      onTap: () {
                                                        // CODE CHUYỂN SANG EDIT Ở ĐÂY
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) =>
                                                                  EditDetailJourney(
                                                                      journey:
                                                                          journeyDetails[
                                                                              index])),
                                                        ).then((_) {
                                                          // This block runs when you have returned back to the 1st Page from 2nd.
                                                          Navigator.pop(
                                                              context);
                                                          setState(() {
                                                            fetchJourney();
                                                            // Call setState to refresh the page.
                                                          });
                                                        });
                                                      },
                                                    ),
                                                    ListTile(
                                                      leading:
                                                          new Icon(Icons.share),
                                                      title: new Text('Share'),
                                                      onTap: () {
                                                        // CODE SHARE JOURNEY Ở ĐÂY
                                                        Navigator.pop(context);
                                                      },
                                                    ),
                                                    ListTile(
                                                      leading: new Icon(
                                                          Icons.delete),
                                                      title: new Text(
                                                          'Delete Journey'),
                                                      onTap: () {
                                                        showDialog(
                                                            builder: (context) =>
                                                                CupertinoAlertDialog(
                                                                  title: Text(
                                                                      "Delete?"),
                                                                  content: Text(
                                                                      "Are you sure you want to remove this journey?"),
                                                                  actions: <
                                                                      Widget>[
                                                                    CupertinoDialogAction(
                                                                        isDefaultAction:
                                                                            true,
                                                                        onPressed:
                                                                            () {
                                                                          Navigator.pop(
                                                                              context);
                                                                        },
                                                                        child: Text(
                                                                            "Cancel")),
                                                                    CupertinoDialogAction(
                                                                        textStyle: TextStyle(
                                                                            color: Colors
                                                                                .red),
                                                                        isDefaultAction:
                                                                            true,
                                                                        onPressed:
                                                                            () async {
                                                                          Navigator.pop(
                                                                              context);
                                                                          deleteJourney(
                                                                              journeyDetails[index].journeyId);
                                                                          RestartWidget.restartApp(
                                                                              context);
                                                                        },
                                                                        child: Text(
                                                                            "Ok")),
                                                                  ],
                                                                ),
                                                            context: context);
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              );
                                            });
                                      })
                                  : IconButton(
                                      icon: Icon(Icons.share), onPressed: () {})
                        ]),
                    body: DetailBody(
                      journey: journeyDetails[index],
                    ),
                  );
                }
              } else {
                // then load circle indicator
                return Scaffold(
                  body: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              continue;
            }
            return Container();
          }
          return Scaffold(body: Center(child: CircularProgressIndicator()));
        });
  }
}
