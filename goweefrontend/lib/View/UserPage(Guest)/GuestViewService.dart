import 'dart:convert';

import 'package:goweefrontend/Model/User.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';

import '../../Constants.dart';
import 'package:http/http.dart' as http;

class GuestViewService {

  follow(userID) async {
    var res = await http.post(
      Uri.parse("$baseUrl/follows/$userID"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    if(res.statusCode==201){
      print("Followed");
    }
    var data = jsonDecode(res.body);
    return data;
  }

  unfollow(followID) async {
    var res = await http.delete(
      Uri.parse("$baseUrl/follows/$followID"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    if(res.statusCode==200){
      print("Unfollowed");
    }
  }
  checkFollowStatus(userID) async {
    var result ;
    var res = await http.get(
      Uri.parse("$baseUrl/follows/user=$userID"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    if(res.statusCode==200){
      var data = jsonDecode(res.body);
      if(data != null){
        for(var i in data){
          if(i["followerId"]==loggedUser.user.userId){
            result = i["followId"];
          }
        }
      }
    }
    return result;
  }

  followerCount(userID) async {
    var res = await http.get(
      Uri.parse("$baseUrl/follows/user=$userID"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    var data= jsonDecode(res.body);
    return data;
  }

  getFollowerById(followerID) async {
    var res = await http.get(
      Uri.parse("$baseUrl/users/$followerID"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    if(res.statusCode ==200){
      var data = jsonDecode(res.body);
      User newUser = new User.fromJson(data);
      return newUser;
    }
  }


}