import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../SizeConfig.dart';
import 'GuestView.dart';

class Following extends StatefulWidget {
  Following({@required this.data});
  final data;

  @override
  _FollowingState createState() => _FollowingState();
}

class _FollowingState extends State<Following> {
  bool visible = true;




  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.all(3),
            child: CircleAvatar(
              radius: 20,
              backgroundImage: NetworkImage(widget.data["userAvatar"] != null
                  ? widget.data["userAvatar"]["imageLink"]
                  : "https://www.pngitem.com/pimgs/m/256-2560208_person-icon-black-png-transparent-png.png") ,
            ),
          ),
          SizedBox(width: getWidth(0.01)),
          Container(
              width: getWidth(0.6),
              child: Text("${widget.data["lastName"]} ${widget.data["firstName"]}")),
          Container(
            child: TextButton(
                onPressed:() {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => GuestView(userId: widget.data["userId"])));
                },
                child: Text("Profile")),
          )

        ],
      ),
    );
  }
}
