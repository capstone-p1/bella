import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/User.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/UserPage(Guest)/Follower.dart';
import 'package:goweefrontend/View/UserPage(Guest)/GuestView.dart';
import 'package:http/http.dart' as http;

import '../../Constants.dart';
import '../../SizeConfig.dart';
import 'Following.dart';

class ViewFollowsPage extends StatefulWidget{
  ViewFollowsPage({@required this.followerIDList, @required this.followingIDList, @required this.index});
  final List followerIDList;
  final List followingIDList;
  final int index;

  @override
  _ViewFollowsPageState createState() => _ViewFollowsPageState();
}
class _ViewFollowsPageState extends State<ViewFollowsPage>{
  List followerList = [];
  List followingList = [];

  //get Follower Infor
  getFollower(followerID) async{
    var res = await http.get(
      Uri.parse("$baseUrl/users/$followerID"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    if(res.statusCode==200){
      var data = jsonDecode(res.body);
      setState(() {
        followerList.add(data);
      });
    }
  }

  getFollowing(followingID) async {
    var res = await http.get(
      Uri.parse("$baseUrl/users/$followingID"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    if(res.statusCode==200){
      var data = jsonDecode(res.body);
      setState(() {
        followingList.add(data);
      });
    }
  }

  addFollowerToList(){
    setState(() {
      followerList=[];
    });
    for(int i = 0; i<widget.followerIDList.length;i++){
      getFollower(widget.followerIDList[i]);
    }
  }

  addFollowingToList(){
    setState(() {
      followingList=[];
    });
    for(int i = 0; i<widget.followingIDList.length;i++){
      getFollowing(widget.followingIDList[i]);
    }
  }

  //loop prints follower UI in the list
  printFollowing(){
    List<Widget> list = [];
    for(var i in followingList){
      list.add(Following(data: i));
      list.add(SizedBox(height: getHeight(0.03)));
    }
    if (list.length!=0){
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: list,
          ),
        ),
      );
    }else{
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("No Following To Show")
        ],
      );
    }
  }

  printFollower(){
    List<Widget> list = [];
    for(var i in followerList){
      list.add(Follower(data: i));
      list.add(SizedBox(height: getHeight(0.03)));
    }
    if (list.length!=0){
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: list,
          ),
        ),
      );
    }else{
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("No Follower To Show")
        ],
      );
    }
  }

  @override
  void initState() {
    super.initState();
    addFollowerToList();
    addFollowingToList();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: kColorPalette4,
      appBar: AppBar(
        // toolbarHeight: getHeight(0.08),
        title: Text(
          "User Profile",
          style: TextStyle(color: kColorPalette4),
        ),
        centerTitle: true,
        backgroundColor: kBackgroundColor,
        elevation: 0.0,
        titleSpacing: 1.0,
      ),
      body: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                offset: Offset(0,8),
                blurRadius: 5,
                color: Colors.grey[300]
            ),
          ],
        ),
        child: DefaultTabController(
          initialIndex: widget.index,
          length: 2,
          child: Scaffold(
              backgroundColor: kColorPalette4,
              extendBody: true,
              appBar: AppBar(
                backgroundColor: kBackgroundColor,
                toolbarHeight: getHeight(0.08),
                excludeHeaderSemantics: false,
                bottom: TabBar(
                  labelColor: kColorPalette4,
                  indicatorColor: kColorPalette4,
                  tabs: [
                    Tab(text: "Follower"),
                    Tab(text: "Following"),
                  ],
                ),
              ),
              body:TabBarView(children: [
                Container(
                  child: printFollower(),
                ),
                Container(
                  child: printFollowing(),
                ),
              ])

          ),
        ),
      ),
    );
  }

}