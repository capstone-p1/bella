import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart' hide Image;
import 'package:flutter/material.dart' hide Image;
import 'package:goweefrontend/View/Detail/DetailJourney.dart';
import 'package:goweefrontend/View/UserPage(Guest)/GuestViewService.dart';
import 'package:goweefrontend/View/UserPage(Guest)/ViewFollowsPage.dart';
import 'package:intl/intl.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import '../../Constants.dart';
import 'package:http/http.dart' as http;
import '../../Model/Journey.dart';
import '../../SizeConfig.dart';

class GuestView extends StatefulWidget {
  const GuestView({Key key, @required this.userId}) : super(key: key);
  final int userId;

  @override
  _GuestViewState createState() => _GuestViewState();
}

class _GuestViewState extends State<GuestView> {
  bool upcomingIsVisible = false;
  bool draftIsVisible = false;
  bool passedIsVisible = false;
  String _firstName;
  String _lastName;
  int _age;
  String _phoneNumber;
  String _image;
  List<Journey> journeyList = [];
  Future _future;
  bool status = false;
  int followerCounter = 0;
  int followingCounter = 0;
  int followID;
  int followerID;
  List followerIdList;
  List followingIdList;

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
  }

  //function to print Journey Info Card
  Widget printCard(Journey journey, IconData flexIcon, page, visible) {
    return Visibility(
      visible: visible,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 8),
                        blurRadius: 5,
                        color: Colors.grey[300]),
                  ]),
              height: getHeight(0.1),
              padding: EdgeInsets.fromLTRB(5, 10, 0, 10),
              child: Row(
                  children: [
                    Padding(
                      //show journey image
                      padding: EdgeInsets.all(3),
                      child: CircleAvatar(
                        backgroundImage: journey.images.isNotEmpty ? NetworkImage(journey.images[0].imageLink)
                            : AssetImage("assets/images/Travel-WordPress-Themes.jpg.webp"),
                        radius: 20,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: getWidth(0.32),
                              child: AutoSizeText(
                                "${journey.departure} - ${journey.destination}",
                                style: TextStyle(color: Colors.black),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(width: getWidth(0.05)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: getWidth(0.27),
                              child: AutoSizeText(
                                "${journey.startDate.day}/${journey.startDate.month} - "
                                    "${journey.endDate.day}/${journey.endDate.month}/${journey.endDate.year}",
                                style:
                                TextStyle(color: Colors.grey, fontSize: 13),
                                maxLines: 1,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(width: getWidth(0.05)),
                    Column(
                      children: [
                        Container(
                          width: getWidth(0.1),
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => page));
                            },
                            child: FittedBox(
                              child: Icon(
                                flexIcon,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),

            ),
          )
        ],
      ),
    );
  }

  //print User Information
  printInfo(info1, info2) {
    return Container(
      width: getWidth(0.45),
      child:AutoSizeText(
          info1 + ":" + info2,
          style: TextStyle(
            decoration: TextDecoration.none,
            fontSize: 13,
            fontWeight: FontWeight.bold,
            color: Colors.black,
            letterSpacing: 1.0,
          ),
          overflow: TextOverflow.ellipsis,
        ),
    );
  }

  //function to show dialog User Profile Card
  showUserInfoCard() {
    return Stack(alignment: Alignment.bottomCenter, children: [
      Container(
          height: 200,
          decoration: kBoxDecorationStyle,
          child: Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
              child: Row(children: [
                Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        printInfo("NAME ", " $_lastName $_firstName"),
                        printInfo("AGE ", " $_age"),
                        printInfo("PHONE ", " $_phoneNumber"),
                      ]),
                Column(
                    children: [
                            CircleAvatar(
                              radius: 35,
                              backgroundImage: NetworkImage(_image != null
                                  ? _image
                                  : "https://www.pngitem.com/pimgs/m/256-2560208_person-icon-black-png-transparent-png.png"),
                            ),
                            SizedBox(height: 10),
                            Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: getWidth(0.2),
                                    child: TextButton(
                                      onPressed: (){
                                        Navigator.push(context,
                                            MaterialPageRoute(builder: (context) => ViewFollowsPage(
                                              followerIDList: followerIdList,followingIDList: followingIdList,index: 0,)));
                                      },
                                      child: AutoSizeText("$followerCounter Follower",maxLines:1,overflow: TextOverflow.ellipsis,style: TextStyle(fontSize: 12,color: kBackgroundColor),),
                                    ),
                                  ),
                                  SizedBox(width: getWidth(0.01)),
                                  Container(
                                    width: getWidth(0.2),
                                    child: TextButton(
                                      onPressed: (){
                                        Navigator.push(context,
                                            MaterialPageRoute(builder: (context) => ViewFollowsPage(
                                              followerIDList: followerIdList,followingIDList: followingIdList,index: 1,)));
                                      },
                                      child: AutoSizeText("$followingCounter Following",maxLines:1,overflow: TextOverflow.ellipsis,style: TextStyle(fontSize: 12,color: kBackgroundColor),),
                                    ),
                                  )
                                ],
                              ),
                            ),

                            Center(
                              child: Container(
                                height: getHeight(0.05),
                                width: getWidth(0.2),
                                decoration: status ==true ? kBoxDecorationUnfollow : kBoxDecorationFollow,
                                child: TextButton(
                                  onPressed: (){
                                    setState(() {
                                      status ==true ? unfollow() : follow();
                                    });
                                  },
                                  child: status ==true ? Text("Unfollow", style: TextStyle(color: Colors.black)) :
                                  Text("Follow", style: TextStyle(color: kColorPalette4)),
                                ),
                              ),
                            )
                          ],
                        ),
                    ],
                  ),
              )),
    ]);
  }

  Widget loopPrintUpComeTrip() {
    int counter = 0;
    List<Widget> list = [];
    for (var i in journeyList) {
      if (i.status == "Upcoming") {
        counter += 1;
        if (counter == 1) {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), true));
        } else {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), upcomingIsVisible));
        }
      }
    }
    if (counter == 0) {
      list.add(Text("No Upcoming Trip to show"));
    }
    return Column(children: list);
  }

  Widget loopPrintInProgressTrip() {
    int counter = 0;
    List<Widget> list = [];
    for (var i in journeyList) {
      if (i.status == "In progress") {
        counter += 1;
        if (counter == 1) {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), true));
        } else {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), draftIsVisible));
        }
      }
    }
    if (counter == 0) {
      list.add(Text("No In Progress Trip to show"));
    }
    return Column(children: list);
  }

  Widget loopPrintFinishedTrip() {
    int counter = 0;
    List<Widget> list = [];
    for (var i in journeyList) {
      if (i.status == "Finished") {
        counter += 1;
        if (counter == 1) {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), true));
        } else {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), passedIsVisible));
        }
      }
    }
    if (counter == 0) {
      list.add(Text(
        "No Finished Trip to show",
        style: TextStyle(color: Colors.grey),
      ));
    }
    return Column(children: list);
  }

  getProfile() async {
    var res = await http.get(
      Uri.parse("$baseUrl/users/${widget.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    var data = jsonDecode(res.body);
    setState(() {
      _firstName = data["firstName"];
      _lastName = data["lastName"];
      _age = data["age"];
      _phoneNumber = data["phoneNumber"];
      _image = data["userAvatar"]["imageLink"];
    });
  }

  getJourneyByUserId() async {
    var res = await http.get(
      Uri.parse("$baseUrl/journeys/userid=${widget.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    List data = jsonDecode(res.body);
    List idList = [];
    for (var i in data) {
      idList.add(i["journeyId"]);
    }

    for (var i in idList) {
      var res = await http.get(
        Uri.parse("$baseUrl/journeys/$i"),
      );
      var data = jsonDecode(res.body);
      Journey userJourney = new Journey.fromJson(data);
      setState(() {
        journeyList.add(userJourney);
      });
    }
    return journeyList;
  }

  Future functionForBuilder() async {
    return await getJourneyByUserId();
  }

  followingCount() async {
    var res = await http.get(
      Uri.parse("$baseUrl/users/${widget.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    var data = jsonDecode(res.body);
    setState(() {
      followingIdList = data["followings"].map((value) => (value["followingId"])).toList();
      followingCounter = followingIdList.length;
    });
  }

  followerCount() async {
    setState(() {
      followerIdList=[];
    });
    var data;
    await GuestViewService().followerCount(widget.userId).then(
        (value) {data = value;}
    );
    for(var i in data){
      if(i["followerId"]==loggedUser.user.userId){
        setState(() {
          followerID = i["followerId"];
        });
      }
    }
    setState(() {
      followerIdList = data.map((value) => (value["followerId"])).toList();
      followerCounter = followerIdList.length;
    });
  }
  follow() async{
    var data;
    await GuestViewService().follow(widget.userId).then(
        (value) {data = value;}
    );
    setState(() {
      followerIdList.add(data["followerId"]);
      followID = data["followId"];
      followerID = data["followerId"];
      status = !status;
      followerCounter = followerCounter+1;
    });
  }

  unfollow() async {
    await GuestViewService().unfollow(followID);
    setState(() {
      status = !status;
      followerCounter = followerCounter-1;
      followerIdList.remove(followerID);
    });
  }

  checkFollowStatus() async {
    var result;
    await GuestViewService().checkFollowStatus(widget.userId).then(
        (value) {result = value;}
    );
    if(result != null){
      setState(() {
        status=true;
        followID=result;
      });
    }else{
      setState(() {
        status=false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getProfile();
    followerCount();
    followingCount();
    checkFollowStatus();
    _future = functionForBuilder();
  }

  //screen
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kColorPalette4,
        appBar: AppBar(
          title: Text("User Profile"),
          centerTitle: true,
          backgroundColor: kBackgroundColor,
          elevation: 0.0,
          titleSpacing: 1.0,
        ),
        body: FutureBuilder(
          future: _future,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      Text("Loading...")
                    ]),
              );
            } else {
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      showUserInfoCard(),
                      SizedBox(height: 5),
                      //getJourneyByUserId(),
                      Row(
                        children: [
                          SizedBox(
                            width: getWidth(0.32),
                            child: AutoSizeText(
                              "Upcoming Trip",
                              style: TextStyle(
                                  color: kColorUpComing,
                                  letterSpacing: 1.0,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ),
                          Container(
                            height: getHeight(0.05),
                            decoration: kBoxDecorationView,
                            child: TextButton(
                              child: AutoSizeText(
                                "View All",
                                style: TextStyle(color: kColorPalette4),
                              ),
                              onPressed: () {
                                setState(() {
                                  upcomingIsVisible = !upcomingIsVisible;
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      //upcoming trip card
                      SizedBox(height: 10),
                      loopPrintUpComeTrip(),
                      Row(
                        children: [
                          SizedBox(
                            width: getWidth(0.32),
                            child: AutoSizeText(
                              "Finished Trip",
                              style: TextStyle(
                                  color: kColorFinished,
                                  letterSpacing: 1.0,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ),
                          Container(
                            height: getHeight(0.05),
                            decoration: kBoxDecorationView,
                            child: TextButton(
                              child: AutoSizeText(
                                "View All",
                                style: TextStyle(color: kColorPalette4),
                              ),
                              onPressed: () {
                                setState(() {
                                  passedIsVisible = !passedIsVisible;
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 5),
                      loopPrintFinishedTrip(),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          SizedBox(
                            width: getWidth(0.32),
                            child: AutoSizeText(
                              "In Progress Trip",
                              style: TextStyle(
                                  color: kColorInProgress,
                                  letterSpacing: 1.0,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ),
                          Container(
                            decoration: kBoxDecorationView,
                            height: getHeight(0.05),
                            child: TextButton(
                              child: AutoSizeText(
                                "View All",
                                style: TextStyle(color: kColorPalette4),
                              ),
                              onPressed: () => setState(
                                  () => draftIsVisible = !draftIsVisible),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 5),
                      loopPrintInProgressTrip(),
                      SizedBox(height: 5),
                    ],
                  ),
                ),
              );
            }
          },
        ));
  }
}
