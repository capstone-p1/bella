import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:goweefrontend/View/UserPage/MyRequest.dart';
import 'package:http/http.dart' as http;
import '../../Constants.dart';
import 'ReceivedRequest.dart';

class JoinRequest extends StatefulWidget {
  @override
  _JoinRequestState createState() => _JoinRequestState();
}


class _JoinRequestState extends State<JoinRequest> {
  List myList = [];
  List receivedList=[];

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
  }


  addRequestToMyList() async {
    var res = await http.get(
      Uri.parse("$baseUrl/users/${loggedUser.user.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
    setState(() {
      myList = [];
    });
    var data = jsonDecode(res.body);
    for(var i in data["requests"]){
      if(i["senderId"]==loggedUser.user.userId){
        setState(() {
          myList.add(i);
        });
      }
    }
  }

  addRequestToReceivedList() async {
    var res = await http.get(
      Uri.parse("$baseUrl/requests/user/${loggedUser.user.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
    setState(() {
      receivedList = [];
    });
    var data = jsonDecode(res.body);
    for(var i in data){
      if(i["status"]== "pending"){
        setState(() {
          receivedList.add(i);
        });
      }
    }
  }


  printMyRequests(){
    List<Widget> list =[];
    for(var i in myList){
      list.add(MyRequest(request: i));
      list.add(SizedBox(height: getHeight(0.02)));
    }
    if (list.length!=0){
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: list,
          ),
        ),
      );
    }else{
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("You have no request")
        ],
      );
    }

  }

  printReceivedRequests(){
    List<Widget> list =[];
    for(var i in receivedList){
      list.add(ReceivedRequest(request: i));
      list.add(SizedBox(height: getHeight(0.02)));
    }
    if(list.length !=0){
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: list,
          ),
        ),
      );
    }else{
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("You have no request")
        ],
      );
    }

  }

  @override
  void initState() {
    super.initState();
    loadUser();
    addRequestToMyList();
    addRequestToReceivedList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHeight(0.3),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              offset: Offset(0,8),
              blurRadius: 5,
              color: Colors.grey[300]
          ),
        ],
      ),
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
            backgroundColor: kColorPalette4,
            extendBody: true,
            appBar: AppBar(
              backgroundColor: kBackgroundColor,
              toolbarHeight: getHeight(0.08),
              excludeHeaderSemantics: false,
              bottom: TabBar(
                labelColor: kColorPalette4,
                indicatorColor: kColorPalette4,
                tabs: [
                  Tab(text: "My Request"),
                  Tab(text: "Received Request"),
                ],
              ),
            ),
            body:TabBarView(children: [
              Container(
                child: printMyRequests(),
              ),
              Container(
                child: printReceivedRequests(),
              ),
            ])

        ),
      ),
    );
  }
}
