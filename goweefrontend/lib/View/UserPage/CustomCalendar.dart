import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:http/http.dart' as http;

import '../../Constants.dart';

List<PickerDateRange> rangeList = [];

class CustomCalendar extends StatefulWidget {
  @override
  _CustomCalendarState createState() => _CustomCalendarState();
}

class _CustomCalendarState extends State<CustomCalendar> {
  List<Journey> journeyList = [];
  Future _future;

  getJourneyByUserId() async {
    var res = await http.get(
      Uri.parse("$baseUrl/journeys/userid=${loggedUser.user.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
    var data = jsonDecode(res.body);
    List idList = [];
    for (var i in data) {
      idList.add(i["journeyId"]);
    }
    for (var i in idList) {
      var res = await http.get(
        Uri.parse("$baseUrl/journeys/$i"),
      );
      var data = jsonDecode(res.body);
      Journey userJourney = new Journey.fromJson(data);
      setState(() {
        journeyList.add(userJourney);
      });
    }
    return journeyList;
  }

  addRanges(journeyList) {
    setState(() {
      rangeList = [];
    });
    for (int i = 0; i < journeyList.length; i++) {
      DateTime startDate = DateTime(journeyList[i].startDate.year,
          journeyList[i].startDate.month, journeyList[i].startDate.day);
      DateTime endDate = DateTime(journeyList[i].endDate.year,
          journeyList[i].endDate.month, journeyList[i].endDate.day);
      setState(() {
        rangeList.add(PickerDateRange(startDate, endDate));
      });
    }
    return rangeList;
  }

  returnRange() {
    List<PickerDateRange> list = [];
    if (rangeList.isNotEmpty) {
      for (int i = 0; i < rangeList.length; i++) {
        list.add(rangeList[i]);
      }
      return list;
    }
  }

  Future functionForBuilder() async {
    return Future.delayed(Duration(seconds: 1))
        .then((value) => "Loading Complete");
  }

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
    getJourneyByUserId().then((receivedList) {
      addRanges(receivedList);
    });
  }

  @override
  void initState() {
    super.initState();
    loadUser();
    _future = functionForBuilder();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    Text("Loading Calendar...")
                  ]),
            );
          } else {
            return Column(children: [
              Container(
                height: getHeight(0.3),
                child: SfDateRangePicker(
                  view: DateRangePickerView.month,
                  monthViewSettings: DateRangePickerMonthViewSettings(
                      firstDayOfWeek: 1,numberOfWeeksInView: 3),
                  selectionMode: DateRangePickerSelectionMode.multiRange,
                  initialSelectedRanges: returnRange(),
                  headerStyle:
                      DateRangePickerHeaderStyle(textAlign: TextAlign.center),
                ),
              ),
            ]);
          }
        },
      ),
    );
  }
}
