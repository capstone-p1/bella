import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/Detail/DetailJourney.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:http/http.dart' as http;
import '../../Constants.dart';
import '../../SizeConfig.dart';

class MyRequest extends StatefulWidget {
  MyRequest({@required this.request});
  final request;

  @override
  _MyRequestState createState() => _MyRequestState();
}

class _MyRequestState extends State<MyRequest> {
  bool visible = true;
  DateTime startDate ;
  DateTime endDate;
  String avatarLink;
  String name;
  String sendDate;
  String journeyImageLink;
  Journey sendingJourney;
  formatDate(){
    DateTime formattedStartDate = DateTime.parse(widget.request["journeyRequesting"]["startDate"]);
    DateTime formattedEndDate = DateTime.parse(widget.request["journeyRequesting"]["endDate"]);
    setState(() {
      startDate = DateTime(formattedStartDate.year,formattedStartDate.month,formattedStartDate.day);
      endDate = DateTime(formattedEndDate.year,formattedEndDate.month,formattedEndDate.day);
    });
  }

  getJourney() async {
    var res = await http.get(
        Uri.parse("$baseUrl/journeys/${widget.request["journeyId"]}"),
        headers: {
          'Content_Type': 'application/json; charset=UTF-8',
        });
    var data = jsonDecode(res.body);
    Journey journey = new Journey.fromJson(data);
    setState(() {
      sendingJourney = journey;
      if(journey.images.isNotEmpty){
        journeyImageLink = journey.images[0].imageLink;
      }
      });
    }


  getOwnerInformation() async {
    var res = await http.get(
      Uri.parse("$baseUrl/users/${widget.request["ownerId"]}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
    var data = jsonDecode(res.body);
    setState(() {
      avatarLink = data["userAvatar"]["imageLink"];
      name = data["lastName"]+" "+ data["firstName"];
    });
  }

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
  }

  removeRequest(requestId) async {
    var res = await http.delete(
      Uri.parse("$baseUrl/requests/$requestId"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
  }

  @override
  void initState() {
    super.initState();
    loadUser();
    formatDate();
    getOwnerInformation();
    getJourney();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Visibility(
      visible: visible,
      child: Container(
        padding: EdgeInsets.fromLTRB(5, 0, 0, 15),
        width: getWidth(0.93),
        height: getHeight(0.17),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: Colors.white,
            boxShadow: [
              BoxShadow(
                  offset: Offset(0,8),
                  blurRadius: 5,
                  color: Colors.grey[300]
              ),
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                Padding(padding: EdgeInsets.all(3),
                child: CircleAvatar(
                  backgroundImage:  journeyImageLink != null
                      ? NetworkImage(journeyImageLink)
                      : AssetImage(
                      "assets/images/Travel-WordPress-Themes.jpg.webp"),
                    radius: 20,
                )),
                SizedBox(width: getWidth(0.02)),
                Container(
                  width: getWidth(0.3),
                  child: TextButton(
                    onPressed: (){
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => DetailsJourneyScreen(journey: sendingJourney)));
                    },
                    child: AutoSizeText(
                        "${widget.request["journeyRequesting"]["departure"]} - "
                            "${widget.request["journeyRequesting"]["destination"]}",
                        style: TextStyle(color: Colors.black),
                        overflow: TextOverflow.ellipsis),
                  ),
                ),
                SizedBox(width: getWidth(0.1)),
                Container(
                  width: getWidth(0.3),
                  child: AutoSizeText(
                        "${startDate.day}/${startDate.month} - ${endDate.day}/${endDate.month}/${endDate.year}",
                        style: TextStyle(color: Colors.grey),
                        overflow: TextOverflow.ellipsis),
                )
              ],
            ),
            SizedBox(height: getHeight(0.01)),
            Container(
              width: getWidth(0.4),
              height: getHeight(0.06),
              decoration: kBoxDecorationView,
              alignment: Alignment.center,
              child: TextButton(
                onPressed: () async {
                  await removeRequest(widget.request["id"]);
                  setState(() {
                    visible = !visible;
                  });
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.cancel,
                        color: Colors.white,
                      ),
                      AutoSizeText(
                        "Cancel",
                        style: TextStyle(color: Colors.white),
                      )
                    ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
