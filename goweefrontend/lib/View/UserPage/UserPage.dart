import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart' hide Image;
import 'package:flutter/material.dart' hide Image;
import 'package:flutter/widgets.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/Detail/DetailJourney.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/UserPage/CustomCalendar.dart';
import 'package:goweefrontend/View/UserPage/JoinRequest.dart';
import 'package:goweefrontend/View/UserPage/UserPageService.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import '../../Constants.dart';
import 'package:http/http.dart' as http;
import '../../Model/Journey.dart';

class UserPage extends StatefulWidget {
  static String routeName = "/userPage";

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  CalendarFormat format = CalendarFormat.twoWeeks;
  DateTime focusedDay = DateTime.now();
  DateTime selectedDay = DateTime.now();
  bool upcomingIsVisible = false;
  bool draftIsVisible = false;
  bool passedIsVisible = false;
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _ageController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  String _firstName;
  String _lastName;
  int _age;
  String _phoneNumber;
  String _imageURL;
  List<Journey> journeyList = [];
  Future _future;

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
  }
  //http APIs

  editProfile(
      String lastName, String firstName, String age, String phoneNumber) async {
    await UserPageService().editProfile(lastName, firstName, age, phoneNumber);
  }

  getProfile() async {
    var data;
    await UserPageService().getProfile().then((value) {
      data = value;
    });
    setState(() {
      _firstName = data["firstName"];
      _lastName = data["lastName"];
      _age = data["age"];
      _phoneNumber = data["phoneNumber"];
      _imageURL = data["userAvatar"]["imageLink"];
    });
  }

  getJourneyByUserId() async {
    setState(() {
      journeyList = [];
    });
    var res = await http.get(
      Uri.parse("$baseUrl/journeys/userid=${loggedUser.user.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    var data = jsonDecode(res.body);
    List idList = [];
    for (var i in data) {
      idList.add(i["journeyId"]);
    }
    for (var i in idList) {
      var res = await http.get(
        Uri.parse("$baseUrl/journeys/$i"),
      );
      var data = jsonDecode(res.body);
      Journey userJourney = new Journey.fromJson(data);
      setState(() {
        journeyList.add(userJourney);
      });
    }
    return journeyList;
  }

  logOut() async {
    await UserPageService().logOut();
    setState(() {
      rangeList.clear();
      journeyList.clear();
    });
  }

  Future functionForBuilder() async {
    return await getJourneyByUserId();
  }

  //function to print Journey Card
  Widget printCard(Journey journey, IconData flexIcon, page, visible) {
    return Visibility(
      visible: visible,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
            child: Container(
              height: getHeight(0.1),
              padding: EdgeInsets.fromLTRB(5, 10, 0, 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 8),
                        blurRadius: 5,
                        color: Colors.grey[300]),
                  ]),
              child: Row(
                children: [
                  Padding(
                    //show journey image
                    padding: EdgeInsets.all(3),
                    child: CircleAvatar(
                      backgroundImage: journey.images.isNotEmpty
                          ? NetworkImage(journey.images[0].imageLink)
                          : AssetImage(
                              "assets/images/Travel-WordPress-Themes.jpg.webp"),
                      radius: 20,
                    ),
                  ),
                  SizedBox(width: getWidth(0.02)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: getWidth(0.32),
                            child: AutoSizeText(
                              "${journey.departure} - ${journey.destination}",
                              style: TextStyle(color: Colors.black),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(width: getWidth(0.05)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: getWidth(0.27),
                            child: AutoSizeText(
                              "${journey.startDate.day}/${journey.startDate.month} - "
                              "${journey.endDate.day}/${journey.endDate.month}/${journey.endDate.year}",
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 13),
                              maxLines: 1,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(width: getWidth(0.05)),
                  Column(
                    children: [
                      Container(
                        width: getWidth(0.1),
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => page));
                          },
                          child: FittedBox(
                            child: Icon(
                              flexIcon,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  //function to show dialog User Profile Card
  showDialogFunc1(context) {
    return showDialog(
        context: context,
        builder: (context) {
          return Stack(alignment: Alignment.bottomCenter, children: [
            Container(
                margin: EdgeInsets.fromLTRB(10, 0, 10, 270),
                height: getHeight(0.4),
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(30.0)),
                child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 30, 0, 10),
                    child: Row(children: [
                      Expanded(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              printInfo("NAME ", "$_lastName $_firstName"),
                              printInfo("AGE ", "$_age"),
                              printInfo("PHONE ", "$_phoneNumber"),
                              Center(
                                child: Container(
                                  height: getHeight(0.05),
                                  decoration: kBoxDecorationView,
                                  child: TextButton(
                                    onPressed: () {
                                      showDialogFunc2(context);
                                    },
                                    child: Text(
                                      "Edit Profile",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              )
                            ]),
                        flex: 5,
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 40, 0),
                              child: Column(
                                children: [
                                  CircleAvatar(
                                    radius: 35,
                                    backgroundImage: NetworkImage(loggedUser
                                                .user.userAvatar.imageLink !=null
                                        ? loggedUser.user.userAvatar.imageLink
                                        : "https://www.pngitem.com/pimgs/m/256-2560208_person-icon-black-png-transparent-png.png"),
                                  ),
                                  SizedBox(height: 15),
                                  FittedBox(
                                    child: Text(
                                      "${loggedUser.user.email}",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: 1.0,
                                          decoration: TextDecoration.none),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        flex: 5,
                      )
                    ]))),
          ]);
        });
  }

  //print User Information
  printInfo(info1, info2) {
    return Container(
      width: getWidth(0.45),
      child:AutoSizeText(
        info1 + ":" + info2,
        style: TextStyle(
          decoration: TextDecoration.none,
          fontSize: 13,
          fontWeight: FontWeight.bold,
          color: Colors.black,
          letterSpacing: 1.0,
        ),
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget loopPrintUpComeTrip() {
    int counter = 0;
    List<Widget> list = [];
    for (var i in journeyList) {
      if (i.status == "Upcoming") {
        counter += 1;
        if (counter == 1) {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), true));
        } else {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), upcomingIsVisible));
        }
      }
    }
    if (counter == 0) {
      list.add(Text("No Upcoming Trip to show"));
    }
    return Column(children: list);
  }

  Widget loopPrintInProgressTrip() {
    int counter = 0;
    List<Widget> list = [];
    for (var i in journeyList) {
      if (i.status == "In progress") {
        counter += 1;
        if (counter == 1) {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), true));
        } else {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), draftIsVisible));
        }
      }
    }
    if (counter == 0) {
      list.add(Text("No In Progress Trip to show"));
    }
    return Column(children: list);
  }

  Widget loopPrintFinishedTrip() {
    int counter = 0;
    List<Widget> list = [];
    for (var i in journeyList) {
      if (i.status == "Finished") {
        counter += 1;
        if (counter == 1) {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), true));
        } else {
          list.add(printCard(i, Icons.arrow_forward_ios,
              DetailsJourneyScreen(journey: i), passedIsVisible));
        }
      }
    }
    if (counter == 0) {
      list.add(Text(
        "No Finished Trip to show",
        style: TextStyle(color: Colors.grey),
      ));
    }
    return Column(children: list);
  }

  //print edit form
  printFormField(info, controller) {
    return Container(
      height: getHeight(0.06),
      child: TextButton(
        onPressed: () {},
        child: TextField(
          controller: controller,
          style: TextStyle(fontSize: 15),
          decoration: InputDecoration(
            hintText: "New $info",
            contentPadding: EdgeInsets.fromLTRB(15, 5, 0, 5),
            isDense: true,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
              borderSide: BorderSide(color: kTextColor),
              gapPadding: 0,
            ),
          ),
        ),
      ),
    );
  }

  //show dialog edit profile
  showDialogFunc2(context) {
    return showDialog(
        context: context,
        builder: (context) {
          return Stack(alignment: Alignment.bottomCenter, children: [
            Container(
                margin: EdgeInsets.fromLTRB(10, 0, 10, 270),
                height: getHeight(0.4),
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(30.0)),
                child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 30, 0, 10),
                    child: Row(children: [
                      Expanded(
                        child: Column(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            //mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              printFormField("Last Name", _lastNameController),
                              printFormField(
                                  "First Name", _firstNameController),
                              printFormField("Age", _ageController),
                              printFormField(
                                  "Phone Number", _phoneNumberController),
                              SizedBox(height: getHeight(0.01)),
                              Center(
                                  child: Container(
                                height: getHeight(0.05),
                                decoration: kBoxDecorationView,
                                child: TextButton(
                                  onPressed: () async {
                                    await editProfile(
                                        _lastNameController.text,
                                        _firstNameController.text,
                                        _ageController.text,
                                        _phoneNumberController.text);
                                    _lastNameController.clear();
                                    _firstNameController.clear();
                                    _ageController.clear();
                                    _phoneNumberController.clear();
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    setState(() {
                                      getProfile();
                                    });
                                  },
                                  child: Text(
                                    "Save",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ))
                            ]),
                        flex: 6,
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 40, 0),
                              child: Column(
                                children: [
                                  CircleAvatar(
                                    radius: 35,
                                    backgroundImage: NetworkImage(
                                        loggedUser.user.userAvatar.imageLink),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    "Upload Image",
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 1.0,
                                        decoration: TextDecoration.none),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        flex: 4,
                      )
                    ]))),
          ]);
        });
  }

  @override
  void initState() {
    super.initState();
    loadUser();
    getProfile();
    _future = getJourneyByUserId();
  }

  //screen
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kColorPalette4,
        appBar: AppBar(
          title: Text(
            "User Profile",
            style: TextStyle(color: kColorPalette4),
          ),
          centerTitle: true,
          backgroundColor: kBackgroundColor,
          elevation: 0.0,
          titleSpacing: 1.0,
        ),
        body: Container(
          child: FutureBuilder(
            future: _future,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        Text("Loading...")
                      ]),
                );
              } else {
                return SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                            //user avatar
                            child: TextButton(
                          onPressed: () {
                            showDialogFunc1(context);
                            getProfile();
                          },
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(_imageURL != null
                                ? _imageURL
                                : "https://www.pngitem.com/pimgs/m/256-2560208_person-icon-black-png-transparent-png.png"),
                            radius: 50,
                          ),
                        )),
                        SizedBox(height: 5),
                        CustomCalendar(),
                        Text("Trip Requests",
                            style: TextStyle(
                                color: kBackgroundColor,
                                letterSpacing: 1.0,
                                fontWeight: FontWeight.bold,
                                fontSize: 15)),
                        SizedBox(height: 10),
                        JoinRequest(),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Container(
                              width: getWidth(0.35),
                              child: AutoSizeText(
                                "Upcoming Trips",
                                style: TextStyle(
                                    color: kColorUpComing,
                                    letterSpacing: 1.0,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                                maxLines: 1,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                              child: Container(
                                height: getHeight(0.05),
                                decoration: kBoxDecorationView,
                                child: TextButton(
                                  child: Text(
                                    "View All",
                                    style: TextStyle(color: kColorPalette4),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      upcomingIsVisible = !upcomingIsVisible;
                                    });
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                        //upcoming trip card
                        SizedBox(height: 5),
                        loopPrintUpComeTrip(),
                        //Calendar Field
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Container(
                              width: getWidth(0.35),
                              child: AutoSizeText(
                                "Finished Trips",
                                style: TextStyle(
                                    color: kColorFinished,
                                    letterSpacing: 1.0,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                                maxLines: 1,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                              child: Container(
                                height: getHeight(0.05),
                                decoration: kBoxDecorationView,
                                child: TextButton(
                                  child: Text(
                                    "View All",
                                    style: TextStyle(color: kColorPalette4),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      passedIsVisible = !passedIsVisible;
                                    });
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 5),
                        loopPrintFinishedTrip(),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Container(
                              width: getWidth(0.35),
                              child: AutoSizeText(
                                "In Progress Trips",
                                style: TextStyle(
                                    color: kColorInProgress,
                                    letterSpacing: 1.0,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                                maxLines: 1,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Container(
                                height: getHeight(0.05),
                                decoration: kBoxDecorationView,
                                child: TextButton(
                                  child: Text(
                                    "View All",
                                    style: TextStyle(color: kColorPalette4),
                                  ),
                                  onPressed: () => setState(
                                      () => draftIsVisible = !draftIsVisible),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 5),
                        loopPrintInProgressTrip(),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: getHeight(0.06),
                              decoration: kBoxDecorationView,
                              alignment: Alignment.center,
                              child: TextButton(
                                onPressed: () async {
                                  await logOut();
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LoginScreen()));
                                },
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.logout,
                                      color: kColorPalette4,
                                      size: 20,
                                    ),
                                    AutoSizeText(
                                      "Log Out",
                                      style: TextStyle(
                                        color: kColorPalette4,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: getHeight(0.01),
                        )
                      ],
                    ),
                  ),
                );
              }
            },
          ),
        ));
  }
}
