import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/Model/User.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/Detail/DetailJourney.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:goweefrontend/View/UserPage(Guest)/GuestView.dart';
import 'package:http/http.dart' as http;
import '../../Constants.dart';
import '../../SizeConfig.dart';

class ReceivedRequest extends StatefulWidget {
  ReceivedRequest({@required this.request});
  final request;

  @override
  _ReceivedRequestState createState() => _ReceivedRequestState();
}

class _ReceivedRequestState extends State<ReceivedRequest> {
  bool visible = true;
  String senderName;
  String departure;
  String destination;
  Journey requestJourney;
  String startDate;
  String endDate;
  String journeyImageLink;

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
  }

  getSenderName() async {
    var res = await http.get(
        Uri.parse("$baseUrl/users/${widget.request["senderId"]}"),
        headers: {
          'Content_Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${loggedUser.token}',
        });
    var data = jsonDecode(res.body);
    setState(() {
      senderName =
          data["lastName"].toString() + " " + data["firstName"].toString();
    });
  }

  getJourney() async {
    var res = await http.get(
        Uri.parse("$baseUrl/journeys/${widget.request["journeyId"]}"),
        headers: {
          'Content_Type': 'application/json; charset=UTF-8',
        });
    var data = jsonDecode(res.body);
    Journey journey = new Journey.fromJson(data);
    setState(() {
      requestJourney = journey;
      startDate =
          "${requestJourney.startDate.day}/${requestJourney.startDate.month}"
              .toString();
      endDate =
          "${requestJourney.endDate.day}/${requestJourney.endDate.month}/${requestJourney.endDate.year}"
              .toString();
      departure = requestJourney.departure;
      destination = requestJourney.destination;
      if(requestJourney.images.isNotEmpty){
        journeyImageLink = requestJourney.images[0].imageLink;
      }
    });
  }

  approveRequest(requestId) async {
    var res =
        await http.patch(Uri.parse("$baseUrl/requests/$requestId"), headers: {
      'Content_Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ${loggedUser.token}',
    }, body: {
      'status': 'approved',
    });
  }

  removeRequest(requestId) async {
    var res = await http.delete(
      Uri.parse("$baseUrl/requests/$requestId"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
  }

  @override
  void initState() {
    super.initState();
    loadUser();
    getSenderName();
    getJourney();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Visibility(
        visible: visible,
        child: Container(
          padding: EdgeInsets.fromLTRB(5, 0, 0, 15),
          width: getWidth(0.93),
          height: getHeight(0.2),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 8),
                    blurRadius: 5,
                    color: Colors.grey[300]),
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Row(
                      children: [
                        Padding(
                            padding: EdgeInsets.all(3),
                            child: CircleAvatar(
                              radius: 20,
                              backgroundImage: journeyImageLink != null ?
                                  NetworkImage(journeyImageLink)
                                :
                                AssetImage(
                                    "assets/images/Travel-WordPress-Themes.jpg.webp"),),
                        ),
                        SizedBox(width: getWidth(0.02)),
                        Container(
                          width: getWidth(0.3),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: getHeight(0.05),
                                child: TextButton(
                                  onPressed: (){
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) => DetailsJourneyScreen(journey: requestJourney)));
                                  },
                                  child: AutoSizeText(
                                      "$departure - $destination",
                                      style: TextStyle(color: Colors.black),
                                      overflow: TextOverflow.ellipsis),
                                ),
                              ),
                              // SizedBox(height: getHeight(0.01)),
                              Container(
                                height: getHeight(0.05),
                                child: TextButton(
                                  onPressed: (){
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) => GuestView(userId: widget.request["senderId"],)));
                                  },
                                    child: AutoSizeText("$senderName",style: TextStyle(color: Colors.black),)),
                              )
                            ],
                          ),
                        ),
                        SizedBox(width: getWidth(0.1)),
                        Container(
                          width: getWidth(0.3),
                          child: AutoSizeText("$startDate - $endDate",
                              style: TextStyle(color: Colors.grey),
                              overflow: TextOverflow.ellipsis),
                        )
                      ],
                    )),
              SizedBox(height: getHeight(0.005)),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: getHeight(0.06),
                      width: getWidth(0.4),
                      decoration: kBoxDecorationView,
                      alignment: Alignment.center,
                      child: TextButton(
                          onPressed: () async {
                            await approveRequest(widget.request["id"]);
                            setState(() {
                              visible = !visible;
                            });
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.check_circle,
                                color: Colors.white,
                              ),
                              Text("Accept",
                                  style: TextStyle(color: Colors.white))
                            ],
                          )),
                    ),
                    SizedBox(width: getWidth(0.05)),
                    Container(
                      alignment: Alignment.center,
                      width: getWidth(0.4),
                      height: getHeight(0.06),
                      decoration: kBoxDecorationView,
                      child: TextButton(
                          onPressed: () async {
                            await removeRequest(widget.request["id"]);
                            setState(() {
                              visible = !visible;
                            });
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.cancel,
                                  color: Colors.white,
                                ),
                                Text(
                                  "Decline",
                                  style: TextStyle(color: Colors.white),
                                )
                              ])),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
