import 'dart:convert';

import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:http/http.dart' as http;

import '../../Constants.dart';

class UserPageService {

  editProfile(
      String lastName, String firstName, String age, String phoneNumber) async {
        await http.patch(Uri.parse("$baseUrl/users/${loggedUser.user.userId}"),
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'Authorization': 'Bearer ${loggedUser.token}',
            },
            body: jsonEncode({
              "lastName": lastName,
              "firstName": firstName,
              "age": age,
              "phoneNumber": phoneNumber
            }));
  }

  logOut() async {
    await http.post(
      Uri.parse("$baseUrl/auth/logout/${loggedUser.user.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
    loggedUser.token = null;
    storage.deleteAll();
  }

  approveRequest(requestId) async{
    await http.patch(
        Uri.parse("$baseUrl/requests/$requestId"),
        headers: {
          'Content_Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${loggedUser.token}',
        },
        body: {
          'status' : 'approved',
        }
    );
  }

  removeRequest(requestId) async {
    var res = await http.delete(
      Uri.parse("$baseUrl/requests/$requestId"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
      },
    );
  }

  getProfile() async {
    var res = await http.get(
      Uri.parse("$baseUrl/users/${loggedUser.user.userId}"),
      headers: {
        'Content_Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${loggedUser.token}',
        'Connection': 'Keep-Alive'
      },
    );
    var data = jsonDecode(res.body);
    return data;
  }
}
