import 'dart:convert';

class City {
  final String name;

  const City ({this.name,});

  static City fromJson(Map<String, dynamic> json) => City(
    name:json['name']
  );
}
class MockVietnamCityApi{
  static Future<List<City>> getSuggestions(String query) async {

    //fix the rawJson receive http response.body
    //example code
    // final url = Uri.parse('https://jsonplaceholder.typicode.com/users');
    // final response = await http.get(url);
    //
    // if (response.statusCode == 200) {
    //   final List users = json.decode(response.body);
    //
    //   return users.map((json) => User.fromJson(json)).where((user) {
    //     final nameLower = user.name.toLowerCase();
    //     final queryLower = query.toLowerCase();
    //
    //     return nameLower.contains(queryLower);
    //   }).toList();
    // } else {
    //   throw Exception();
    // }

    String rawJson = '[{"name":"Hà Nội"},{"name":"Hồ Chí Minh"},{"name":"An Giang"},{"name":"Bà Rịa - Vũng Tàu"},{"name":"Bạc Liêu"},{"name":"Bắc Giang"},{"name":"Bắc Kạn"},{"name":"Bắc Ninh"},{"name":"Bến Tre"},{"name":"Bình Dương"},{"name":"Bình Định"},{"name":"Bình Phước"},{"name":"Bình Thuận"},{"name":"Cao Bằng"},{"name":"Cà Mau"},{"name":"Cần Thơ"},{"name":"Hải Phòng"},{"name":"Đà Nẵng"},{"name":"Gia Lai"},{"name":"Hòa Bình"},{"name":"Hà Giang"},{"name":"Hà Nam"},{"name":"Hà Tĩnh"},{"name":"Hưng Yên"},{"name":"Hải Dương"},{"name":"Hậu Giang"},{"name":"Điện Biên"},{"name":"Đắk Lắk"},{"name":"Đắk Nông"},{"name":"Đồng Nai"},{"name":"Đồng Tháp"},{"name":"Khánh Hòa"},{"name":"Kiên Giang"},{"name":"Kon Tum"},{"name":"Lai Châu"},{"name":"Long An"},{"name":"Lào Cai"},{"name":"Lâm Đồng"},{"name":"Lạng Sơn"},{"name":"Nam Định"},{"name":"Nghệ An"},{"name":"Ninh Bình"},{"name":"Ninh Thuận"},{"name":"Phú Thọ"},{"name":"Phú Yên"},{"name":"Quảng Bình"},{"name":"Quảng Nam"},{"name":"Quảng Ngãi"},{"name":"Quảng Ninh"},{"name":"Quảng Trị"},{"name":"Sóc Trăng"},{"name":"Sơn La"},{"name":"Thanh Hóa"},{"name":"Thái Bình"},{"name":"Thái Nguyên"},{"name":"Thừa Thiên - Huế"},{"name":"Tiền Giang"},{"name":"Trà Vinh"},{"name":"Tuyên Quang"},{"name":"Tây Ninh"},{"name":"Vĩnh Long"},{"name":"Vĩnh Phúc"},{"name":"Yên Bái"}]';

    //final List cities = json.decode(rawJson);

    List<City> myCities;

    myCities=(json.decode(rawJson) as List).map((i) =>
        City.fromJson(i)).where((city){
          final queryLower = query.toLowerCase();
          final cityLower = city.name.toLowerCase();
          return cityLower.contains(queryLower);
    }).toList();

    //only return 5 entries
    return myCities.take(5).toList();

  }
}