import 'dart:convert';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/Image.dart' as Gowee;
import 'package:goweefrontend/Model/Like.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/Model/User.dart';
import 'package:goweefrontend/SizeConfig.dart';
import 'package:goweefrontend/View/AddJourney/GoogleMapApi.dart';
import 'package:goweefrontend/View/AddJourney/MockVietnamCityApi.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:goweefrontend/View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/Model/Comment.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart';
import 'AddMilestone.dart';

/// To see google map, replace API key in AppDelegate.swift on iOS simulator
/// replace API key in AndroidManifest.xml on Android simulator
/// To use milestone, replace API key in GoogleMapApi.dart

class Journey{
  int journeyId;
  User author;
  String departure;

  String destination;
  DateTime startDate;
  DateTime endDate;
  String status;
  String companion;
  String description;
  List<Like> likes; //array
  List<Image> images;
  List<Milestones> milestones;
  List<Comment> comments;


  Journey();

  @override
  String toString() {
    return '{"departure": "$departure", "destination": "$destination", "startDate": "$startDate", "endDate": "$endDate", "milestones": $milestones, "description": "$description", "status": "$status"}';
  }
}
class JourneyForm extends StatefulWidget {
  final String departure;
  final String destination;
  final DateTime startDate;
  final DateTime endDate;
  final List<Milestones> milestones;

  // final String status;
  // final List<User> companion;
  // final String description;

  JourneyForm(this.departure, this.destination, this.startDate,
      this.endDate, this.milestones);


  @override
  _JourneyFormState createState() => _JourneyFormState();
}



class _JourneyFormState extends State<JourneyForm> {
  final _departureTextFieldController = TextEditingController();
  final _destinationTextFieldController = TextEditingController();
  final _descriptionTextFieldController = TextEditingController();
  String _selectedStatus = "Upcoming";
  var milestoneList = <Milestones>[];
  var imageList = <Gowee.Image>[];
  bool showLoader = false;


  DateTime selectedStartDate = DateTime.now();
  DateTime selectedEndDate = DateTime.now().add(Duration(days: 1));
  // LatLng(10.776530,106.700981) Saigon
  // LatLng(16.054407,108.202164) Da Nang

  static const _initialCameraPosition = CameraPosition(
    target: LatLng(14.058324, 108.277199), // Vietnam
    zoom: 5,
  );

  Marker _origin;
  Marker _destination;

  GoogleMapController _googleMapController;

  String journeyImageUrl;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _googleMapController.dispose();
    super.dispose();
  }

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedStartDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedStartDate)
      setState(() {
        selectedStartDate = picked;
      });
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedEndDate)
      setState(() {
        selectedEndDate = picked;
      });
  }

  _awaitReturnValueFromAddMilestone(BuildContext context) async{
    // start the AddMilestone and wait for it to finish with a result
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddMilestone(milestones: milestoneList),
        ));

    // after the AddMilestone result comes back update the milestoneList
    if (result.places.isNotEmpty || result.milestoneName.isNotEmpty){ //have places or have milestone name
      setState(() {
        milestoneList.add(result);
      });
    }
  }

  void _debug(){
    // for (final milestone in milestoneList){
    //   print(milestone.toString());
    // }
    print(milestoneList.toString());

  }

  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);

  }


  //HTTP post to API
  Future<void> postJourney(String departure, String destination, String startDate, String endDate, String description, String status,List<Milestones> milestones, List<Gowee.Image> imageList) async{
    String token = loggedUser.token;
    // String url = 'http://localhost:3000/journeys';
    String url = '$baseUrl/journeys';


    // Journey journey = new Journey();
    // journey.departure = departure;
    // journey.destination = destination;
    // journey.milestones = milestones;


    final response = await http.post(Uri.parse(url),
        headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    },
        body: jsonEncode({
          'departure' : departure,
          'destination': destination,
          'startDate': startDate,
          'endDate': endDate,
          'description': description,
          'status': status,
          'images': jsonDecode(imageList.toString()),
          'milestones' : jsonDecode(milestones.toString())
    })
    );
    // print(journey.toString());

    print(response.statusCode);
    print(response.body);
  }

  uploadImage() async{
    final _storage = FirebaseStorage.instance;
    final _picker = ImagePicker();
    PickedFile image;
    //Check permissions
    await Permission.photos.request();

    var permissionStatus = Permission.photos.status;

    if (permissionStatus.isGranted != null){
      //Select image
      image = await _picker.getImage(source: ImageSource.gallery);
      var file = File(image.path);
      String filename = basename(file.path);

      setState(() {
        print("show loader true");
        showLoader = true;
      });

      if(image != null){
        //upload to firebase
        var snapshot = await _storage.ref().child('journeyImage/$filename').putFile(file);

        var downloadUrl = await snapshot.ref.getDownloadURL();

        setState(() {
          // journeyImageUrl = downloadUrl;
          showLoader = false;
          Gowee.Image image = new Gowee.Image.noID(downloadUrl);
          imageList.add(image);
        });
      }
      else{
        print("No path received");
      }
    }
    else{
      print("permission not granted");
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ///Start of Departure
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: Text(
                    "Departure"
                    ,style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )
                ,
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: TypeAheadField(
                    textFieldConfiguration: TextFieldConfiguration(
                      controller: _departureTextFieldController,
                      autofocus: false,
                      style: DefaultTextStyle.of(context)
                          .style
                          .copyWith(fontStyle: FontStyle.italic),
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Type in your departure location'
                      ),
                    ),
                    suggestionsCallback: (pattern) async {
                      // Here you can call http call
                      return await MockVietnamCityApi.getSuggestions(pattern);
                    },
                    itemBuilder: (context,City suggestion){
                      final city = suggestion;
                      return ListTile(
                          leading: Icon(Icons.location_city_rounded),
                          title: Text(city.name)
                      );
                    },
                    onSuggestionSelected: (City suggestion){
                      final city = suggestion;
                      _departureTextFieldController.text = city.name;
                      setState(() {
                        _origin = Marker(
                            markerId: const MarkerId("Departure"),
                            infoWindow: const InfoWindow(title: "Departure"),
                            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
                            position: LatLng(16.054407,108.202164)
                        );
                      });
                    },
                    noItemsFoundBuilder: (context) => Container(
                      height: 50,
                      child: Center(
                        child: Text(
                          'No city found',
                          style: TextStyle(fontSize: 24),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
          ,
          ///Start of Destination
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: Text(
                    "Destination"
                    ,style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )
                ,
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: TypeAheadField(
                    textFieldConfiguration: TextFieldConfiguration(
                      controller: _destinationTextFieldController,
                      autofocus: false,
                      style: DefaultTextStyle.of(context)
                          .style
                          .copyWith(fontStyle: FontStyle.italic),
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Type in your destination location'
                      ),
                    ),
                    suggestionsCallback: (pattern) async {
                      // Here you can call http call
                      return await MockVietnamCityApi.getSuggestions(pattern);
                    },
                    itemBuilder: (context,City suggestion){
                      final city = suggestion;
                      return ListTile(
                          leading: Icon(Icons.location_city_rounded),
                          title: Text(city.name)
                      );
                    },
                    onSuggestionSelected: (City suggestion){
                      final city = suggestion;
                      _destinationTextFieldController.text = city.name;
                      setState(() {
                        _destination = Marker(
                            markerId: const MarkerId("Destination"),
                            infoWindow: const InfoWindow(title: "Destination"),
                            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
                            position: LatLng(10.776530,106.700981)
                        );
                      });
                    },
                    noItemsFoundBuilder: (context) => Container(
                      height: 50,
                      child: Center(
                        child: Text(
                          'No city found',
                          style: TextStyle(fontSize: 24),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
          ,

          ///Start of description
          Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: Text(
                        "Description"
                        ,style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                    ,
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                      child: TextFormField(
                        controller: _descriptionTextFieldController,
                        minLines: 2,
                        maxLines: 5,
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          hintText: 'Description',
                          hintStyle: TextStyle(
                              color: Colors.grey
                          ),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    )

                  ]
              )
          )
          ,

          ///Start of MapScreen
          // Stack(
          //   children: [SizedBox(
          //     height: getProportionateScreenHeight(300),
          //     child: GoogleMap(
          //       initialCameraPosition: _initialCameraPosition,
          //       myLocationButtonEnabled: false,
          //       zoomControlsEnabled: false,
          //       onMapCreated: (controller) => _googleMapController = controller,
          //       markers: {
          //         if (_origin != null) _origin,
          //         if (_destination != null)_destination,
          //       },
          //     ),
          //   )],
          // )
          ///End of MapScreen
          // ,

          /// Start of dates
          Padding(
            padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
            child: Container(
              child:
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Stack(
                      children: <Widget>[
                        SizedBox(
                          height: 10.0,
                        ),
                        TextButton(
                          onPressed: () => {
                            _selectStartDate(context)
                          },
                          child: Center(
                            child: Container(
                              width: getWidth(0.45),
                              height: getHeight(0.05),
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: [
                                    Colors.blue[800],
                                    // Color(0x78FFD6),
                                    Colors.greenAccent
                                  ],
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[300],
                                    blurRadius: 4,
                                    offset: Offset(0, 8), // Shadow position
                                  ),
                                ],
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Text('From: ${selectedStartDate.toLocal().toString().split(' ')[0]}',
                                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                        ,
                      ],
                    )
                    ,
                    Stack(
                      children: <Widget>[
                        SizedBox(
                          height: 10.0,
                        ),
                        TextButton(
                          onPressed: () => {
                            _selectEndDate(context)
                          },
                          child: Center(
                            child: Container(
                              width: getWidth(0.40),
                              height: getHeight(0.05),
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: [
                                    Colors.blue[800],
                                    // Color(0x78FFD6),
                                    Colors.greenAccent
                                  ],
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[300],
                                    blurRadius: 4,
                                    offset: Offset(0, 8), // Shadow position
                                  ),
                                ],
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Text('to: ${selectedEndDate.toLocal().toString().split(' ')[0]}',
                                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ]
              ),
            )
          )
          ,

          ///Dropdown status menu
          Container(
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: Text(
                    "Journey status :"
                    ,style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )
                ,
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                  child: DropdownButton<String>(
                    hint: Text(_selectedStatus, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: getProportionateScreenHeight(14)),),
                    items: <String>['Upcoming', 'In progress', 'Finished'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: getProportionateScreenHeight(14)),),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _selectedStatus = value;
                      });
                    },
                  ),
                )
              ],
            )
          )
          ,

          ///List of milestones
          Padding(
            padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
            child: Text(
              "Milestones"
              ,style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
          ,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...List.generate(milestoneList.length, (milestoneIndex) =>
                  Column(
                    children: [
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                          child: Container(
                            child: ListTile(
                              leading: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  milestoneList[milestoneIndex].images.length == 0 ?
                                  Icon(
                                    Icons.flag_rounded,
                                    color: Colors.greenAccent,
                                  )
                                  :Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                          width: getWidth(0.11),
                                          height: getHeight(0.025),
                                          child: Image.network(milestoneList[milestoneIndex].images[0].imageLink)
                                      )
                                      ,
                                      milestoneList[milestoneIndex].images.length > 1 ? Text("+${milestoneList[milestoneIndex].images.length - 1}") : SizedBox()
                                    ],
                                  )
                                  ,
                                ],
                              ),
                              trailing: IconButton(
                                icon: Icon(
                                  Icons.highlight_remove_rounded,
                                  color: Colors.redAccent,
                                ),
                                onPressed: ()=>{
                                  //Remove that milestone
                                  setState((){milestoneList.removeAt(milestoneIndex);})
                                },
                              ),
                              title: Container(
                                //margin: EdgeInsets.only(bottom: getProportionateScreenHeight(7)),
                                child: Text(
                                  milestoneList[milestoneIndex].milestoneName,
                                  style: TextStyle(
                                    fontSize: getProportionateScreenHeight(17),
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              ///List of places
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ...List.generate(milestoneList[milestoneIndex].places.length, (placeIndex) =>
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(bottom: getProportionateScreenHeight(5),top: getProportionateScreenHeight(5)),
                                            child: Icon(
                                              Icons.location_on,
                                              color: Colors.redAccent,
                                              size: getProportionateScreenWidth(15),
                                            ),
                                          )
                                          ,
                                          Text(milestoneList[milestoneIndex].places[placeIndex].placeName ?? '',
                                              style: TextStyle(
                                                fontSize: getProportionateScreenHeight(15),
                                                color: Colors.black,
                                              )
                                          )
                                          ,
                                          Text(milestoneList[milestoneIndex].places[placeIndex].formattedAddress ?? ''),
                                        ],
                                      )
                                  )
                                ],
                              ),
                            ),
                          )
                      )
                      ,
                      Divider()
                    ],
                  )
              )
            ],
          )

          ,


          ///New Milestone
          Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
                child: TextButton(
                  onPressed: () => {
                    _awaitReturnValueFromAddMilestone(context)
                  },
                  child: Center(
                    child: Container(
                      width: getWidth(0.5),
                      height: getHeight(0.05),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            Colors.blue[800],
                            // Color(0x78FFD6),
                            Colors.greenAccent
                          ],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 4,
                            offset: Offset(0, 8), // Shadow position
                          ),
                        ],
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.add_box_outlined,color : Colors.black)
                          ,
                          Center(
                            child: Text(milestoneList.length == 0 ? "Add milestone" : "Add more milestones",
                                style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              )
          )
          ,

          ///List of Images
          Padding(
            padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10),vertical: getProportionateScreenHeight(5)),
            child: Text(
              "Images"
              ,style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
          ,
          Column(
            children: [
              ...List.generate(imageList.length, (index) =>
                  Column(
                    children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(
                                width: getProportionateScreenWidth(300),
                                child: Image.network(imageList[index].imageLink)
                            )
                            ,
                            Center(
                                child: IconButton(
                                  icon: Icon(
                                    Icons.highlight_remove_rounded,
                                    color: Colors.redAccent,
                                  ),
                                  onPressed: ()=>{
                                    //Remove that image
                                    setState((){imageList.removeAt(index);})
                                  },
                                )
                            )
                          ]
                      )
                      ,Divider()
                    ],
                  )
              )
              ,

              Center(child: Visibility(visible: showLoader,child: CircularProgressIndicator()))
            ],
          )
          ,

          // Column(
          //   children: <Widget>[
          //     (journeyImageUrl != null)
          //         ? Row(
          //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //     children: [
          //       SizedBox(width: getProportionateScreenWidth(300), child: Image.network(journeyImageUrl))
          //       ,
          //       Center(
          //         child: IconButton(
          //           icon: Icon(
          //             Icons.highlight_remove_rounded,
          //             color: Colors.redAccent,
          //           ),
          //           onPressed: ()=>{
          //             //Remove that milestone
          //             print("press")
          //           },
          //         )
          //       )
          //     ]
          //     )
          //         : Placeholder(fallbackHeight: 50.0,fallbackWidth: double.infinity),
          //     SizedBox(height: 20.0,),
          //   ],
          // )
          // ,
          ///Add photos button
          Center(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextButton(
                  onPressed: () => {
                    uploadImage()
                  },
                  child: Center(
                    child: Container(
                      width: getWidth(0.5),
                      height: getHeight(0.05),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            Colors.blue[800],
                            // Color(0x78FFD6),
                            Colors.greenAccent
                          ],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 4,
                            offset: Offset(0, 8), // Shadow position
                          ),
                        ],
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.add_photo_alternate_outlined,color: Colors.black)
                          ,
                          Center(
                            child: Text(imageList.length == 0 ? "Add photo" : "Add more photos",
                                style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
          )
          ,

          ///Create button
          Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: TextButton(
                  onPressed: () => {
                    postJourney(
                        _departureTextFieldController.text,
                        _destinationTextFieldController.text,
                        selectedStartDate.toLocal().toString().split(' ')[0],
                        selectedEndDate.toLocal().toString().split(' ')[0],
                        _descriptionTextFieldController.text,
                        _selectedStatus,
                        milestoneList,
                        imageList
                    )
                  },
                  child: Center(
                    child: Container(
                      width: getWidth(0.45),
                      height: getHeight(0.05),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            Colors.blue[800],
                            // Color(0x78FFD6),
                            Colors.greenAccent
                          ],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[300],
                            blurRadius: 4,
                            offset: Offset(0, 8), // Shadow position
                          ),
                        ],
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.add_circle_outline, color: Colors.black),
                                    Text('Create journey',
                                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                                  ]
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                )
                ,
              )
          )
          ,


          // Center(
          //     child: Padding(
          //       padding: const EdgeInsets.all(5.0),
          //       child: TextButton(
          //         child: Text("Debug"),
          //         onPressed: ()=>{
          //           _debug()
          //         },
          //         style: ButtonStyle(
          //             backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
          //             foregroundColor: MaterialStateProperty.all<Color>(Colors.black)
          //         ),
          //       ),
          //     )
          // )
        ],
      ),
    );
  }
}

