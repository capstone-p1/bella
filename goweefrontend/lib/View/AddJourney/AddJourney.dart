import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/View/AddJourney/JourneyForm.dart';

import 'AddMilestone.dart';

class AddJourney extends StatelessWidget {

  final String departure;
  final String destination;
  final DateTime startDate;
  final DateTime endDate;
  final List<Milestones> milestones;

  const AddJourney({Key key, this.departure, this.destination, this.startDate, this.endDate, this.milestones}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
              "Create new journey"
          ),
        ),
        body: Center(
            child: JourneyForm(departure,destination,startDate,endDate,milestones)
        )
    );
  }
}