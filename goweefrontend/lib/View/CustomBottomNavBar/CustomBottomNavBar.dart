import 'dart:convert';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/LoggedUser.dart';
import 'package:goweefrontend/View/LoginFunction/FullLogin.dart';
import 'package:goweefrontend/View/LoginFunction/LoginComponent/LoginScreen.dart';
import '../AddJourney/AddJourney.dart';

import '../HomeScreen/HomeScreen.dart';

class CustomBottomNavBar extends StatefulWidget {
  static String routeName = "/bottomBar";
  @override
  _CustomBottomNavBarState createState() => _CustomBottomNavBarState();
}

var loggedUser = new LoggedUser();

class _CustomBottomNavBarState extends State<CustomBottomNavBar> {
  Future loadUser() async {
    String jsonString = await storage.read(key: "jwt");
    final jsonResponse = json.decode(jsonString);
    loggedUser = new LoggedUser.fromJson(jsonResponse);
  }

  Stream getLoggedUser(Duration refreshTime) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await loadUser();
    }
  }

  Stream upDate;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    upDate = getLoggedUser(Duration(seconds: 1));
  }

  int _page = 1;
  GlobalKey _bottomNavigationKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: upDate,
        builder: (context, snapshot) {
          return Scaffold(
            body: Builder(builder: (context) {
              if (_page == 1) {
                return HomeScreen();
              }
              if (_page == 0) {
                if (loggedUser.token != null){
                return AddJourney();
                } else{
                  return LoginScreen();
                }
              }
              if (_page == 2) {
                return FullLogin();
              }
              return Container();
            }),
            bottomNavigationBar: CurvedNavigationBar(
              color: Colors.grey,
              backgroundColor: Colors.white,
              buttonBackgroundColor: Colors.white,
              key: _bottomNavigationKey,
              height: 50.0,
              index: 1,
              items: <Widget>[
                Icon(Icons.add, size: 30),
                Icon(Icons.home, size: 30),
                loggedUser.token != null
                    ? CircleAvatar(
                        backgroundImage:
                            NetworkImage(loggedUser.user.userAvatar.imageLink))
                    : Icon(Icons.person, size: 30)
              ],
              animationDuration: Duration(milliseconds: 200),
              animationCurve: Curves.bounceInOut,
              onTap: (index) {
                setState(() {
                  _page = index;
                });
              },
            ),
          );
        });
  }
}
