import 'package:flutter/material.dart';
import 'package:goweefrontend/Constants.dart';
import '../../SizeConfig.dart';
import 'dart:async';
import 'dart:convert';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:http/http.dart' as http;
import '../../Constants.dart';
import '../../SizeConfig.dart';
import 'HomeScreenComponent/HomeHeader.dart';
import 'HomeScreenComponent/JourneyScreen/ListJourney.dart';
import 'HomeScreenComponent/Hottest/Hottest.dart';

class HomeScreen extends StatefulWidget {
  static String routeName = "/home";
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _value = 1;
  var journeys = new List<Journey>();
  Timer timer;
  bool upcomingColor = false;
  bool finishedColor = false;
  bool inProgressColor = false;

  /// GET ALL JOURNEY

  Future fetchJourney() async {
    // print("have fetch");
    // var authorIdLike =[];
    final response = await http.get(Uri.parse("$baseUrl/journeys"));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Iterable list = jsonDecode(response.body);
      return journeys = list.map((model) => Journey.fromJson(model)).toList();
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Stream getJourney(Duration refreshTime) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await fetchJourney();
    }
  }

  /// GET JOURNEY BY STATUS

  Future fetchJourneyByStatus(String status) async {
    final response =
    await http.get(Uri.parse("$baseUrl/journeys/status=$status"));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List data = jsonDecode(response.body);
      List idList = [];
      for (var i in data) {
        idList.add(i["journeyId"]);
      }
      var journeysStatus = new List<Journey>();
      for (var i in idList) {
        var res = await http.get(
          Uri.parse("$baseUrl/journeys/$i"),
        );
        var data = jsonDecode(res.body);
        Journey userJourney = new Journey.fromJson(data);
        setState(() {
          journeysStatus.add(userJourney);
        });
      }
      return journeys = journeysStatus;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load list');
    }
  }

  Stream getJourneyStatus(Duration refreshTime, String status) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await fetchJourneyByStatus(status);
    }
  }

  /// GET JOURNEY BY Date
  /// Get Journey By Date Up
  Future fetchSearchJourneyUp(String keyDate) async {
    final response =
    await http.get(Uri.parse("$baseUrl/journeys/sortbyasc=$keyDate"));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List data = jsonDecode(response.body);
      List idList = [];
      for (var i in data) {
        idList.add(i["journeyId"]);
      }
      var journeysUp = new List<Journey>();
      for (var i in idList) {
        var res = await http.get(
          Uri.parse("$baseUrl/journeys/$i"),
        );
        var data = jsonDecode(res.body);
        Journey userJourney = new Journey.fromJson(data);
        setState(() {
          journeysUp.add(userJourney);
        });
      }
      return journeys = journeysUp;
      // Iterable list = json.decode(response.body);
      // print( list.map((model) => Journey.fromJson(model).journeyId).toList());
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Stream getJourneyUp(Duration refreshTime, String keyDate) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await fetchSearchJourneyUp(keyDate);
    }
  }

  /// Get Journey By Date Down
  Future fetchSearchJourneyDown(String keyDate) async {
    journeys = [];
    final response =
    await http.get(Uri.parse("$baseUrl/journeys/sortbydesc=$keyDate"));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List data = jsonDecode(response.body);
      List idList = [];
      for (var i in data) {
        idList.add(i["journeyId"]);
      }
      var journeysDown = new List<Journey>();
      for (var i in idList) {
        var res = await http.get(
          Uri.parse("$baseUrl/journeys/$i"),
        );
        var data = jsonDecode(res.body);
        Journey userJourney = new Journey.fromJson(data);
        setState(() {
          journeysDown.add(userJourney);
        });
      }
      return journeys = journeysDown;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Stream getJourneyDown(Duration refreshTime, String keyDate) async* {
    while (true) {
      await Future.delayed(refreshTime);
      yield await fetchSearchJourneyDown(keyDate);
    }
  }

  Stream futureJourney;
  initState() {
    super.initState();
    futureJourney = getJourney(Duration(seconds: 1));
    print("bug here");
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: kColorPalette4,
      body: SafeArea(
        child: SingleChildScrollView(
          // controller: controller,
          child: StreamBuilder(
              stream: futureJourney,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  // print(snapshot.data[0].toString());
                  return Column(
                    children: [
                      SizedBox(height: getHeight(0.05)),
                      HomeHeader(),
                      SizedBox(height: getWidth(0.01)),
                      Hottest(),
                      SizedBox(height: getHeight(0.01)),
                      Container(
                        width: SizeConfig.screenWidth,
                        decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            )),
                        child: Padding(
                          padding: EdgeInsets.only(left: getWidth(0.05)),
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  // padding: EdgeInsets.only(left: getWidth(0.05)),
                                  width: getWidth(0.21),
                                  child: DropdownButton(
                                      isExpanded: true,
                                      value: _value,
                                      items: [
                                        DropdownMenuItem(
                                          child: Text("All",
                                              style: TextStyle(
                                                  fontSize: getWidth(0.035))),
                                          value: 1,
                                        ),
                                        DropdownMenuItem(
                                          child: Row(
                                            children: [
                                              Flexible(
                                                  child: Text("Start Date",
                                                      style: TextStyle(
                                                          fontSize:
                                                          getWidth(0.035)))),
                                              Icon(Icons.keyboard_arrow_up)
                                            ],
                                          ),
                                          value: 3,
                                        ),
                                        DropdownMenuItem(
                                          child: Row(
                                            children: [
                                              Flexible(
                                                  child: Text("Start Date",
                                                      style: TextStyle(
                                                          fontSize:
                                                          getWidth(0.035)))),
                                              Icon(Icons.keyboard_arrow_down)
                                            ],
                                          ),
                                          value: 4,
                                        ),
                                        DropdownMenuItem(
                                          child: Text(
                                            "New Journey",
                                            style: TextStyle(
                                                fontSize: getWidth(0.035)),
                                          ),
                                          value: 5,
                                        ),
                                      ],
                                      onChanged: (value) {
                                        setState(() {
                                          _value = value;
                                          if (_value == 1) {
                                            futureJourney =
                                                getJourney(Duration(seconds: 1));
                                            upcomingColor = false;
                                            inProgressColor = false;
                                            finishedColor = false;
                                          }
                                          if (_value == 3) {
                                            futureJourney = getJourneyUp(
                                                Duration(seconds: 1),
                                                "startDate");
                                            upcomingColor = false;
                                            inProgressColor = false;
                                            finishedColor = false;
                                          }
                                          if (_value == 4) {
                                            futureJourney = getJourneyDown(
                                                Duration(seconds: 1),
                                                "startDate");
                                            upcomingColor = false;
                                            inProgressColor = false;
                                            finishedColor = false;
                                          }
                                          if (_value == 5) {
                                            futureJourney = getJourneyDown(
                                                Duration(seconds: 1),
                                                "created_at");
                                            upcomingColor = false;
                                            inProgressColor = false;
                                            finishedColor = false;
                                          }
                                          print("$_value");
                                        });
                                      },
                                      hint: Text("Select item")),
                                ),
                              ),
                              FlatButton(
                                  onPressed: () {
                                    futureJourney = getJourneyStatus(
                                        Duration(seconds: 1), "Upcoming");
                                    upcomingColor = !upcomingColor;
                                    inProgressColor = false;
                                    finishedColor = false;
                                    if (this.mounted) {
                                      setState(() {});
                                    }
                                  },
                                  color: upcomingColor ? Colors.blue[200] : null,
                                  child: Text(
                                    "Upcoming",
                                    style: TextStyle(fontSize: getWidth(0.035)),
                                  )),
                              FlatButton(
                                  onPressed: () {
                                    futureJourney = getJourneyStatus(
                                        Duration(seconds: 1), "In progress");
                                    inProgressColor = !inProgressColor;
                                    upcomingColor = false;
                                    finishedColor = false;
                                    if (this.mounted) {
                                      setState(() {});
                                    }
                                  },
                                  color:
                                  inProgressColor ? Colors.green[300] : null,
                                  child: Text(
                                    "In Progress",
                                    style: TextStyle(fontSize: getWidth(0.035)),
                                  )),
                              FlatButton(
                                  onPressed: () {
                                    futureJourney = getJourneyStatus(
                                        Duration(seconds: 1), "Finished");
                                    finishedColor = !finishedColor;
                                    upcomingColor = false;
                                    inProgressColor = false;
                                    if (this.mounted) {
                                      setState(() {});
                                    }
                                  },
                                  color: finishedColor ? Colors.red[300] : null,
                                  child: Text(
                                    "Finished",
                                    style: TextStyle(fontSize: getWidth(0.035)),
                                  )),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: getHeight(0.02),
                      ),
                      JourneyHomePage(
                        journey: journeys,
                      ),
                      SizedBox(height: getHeight(0.03)),
                    ],
                  );
                }
                return Center(child: CircularProgressIndicator());
              }),
        ),
      ),
    );
  }
}
