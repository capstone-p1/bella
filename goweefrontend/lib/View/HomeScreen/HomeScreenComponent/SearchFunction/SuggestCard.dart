import 'package:flutter/material.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/Journey.dart';

import '../../../../SizeConfig.dart';

class SuggestCard extends StatelessWidget {
  const SuggestCard({
    Key key,
    @required this.journey,
    @required this.press,
  }) : super(key: key);
  final Journey journey;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(
              color: kColorPalette4,
              width: 1.0,
            ),
          )),
          child: Row(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(bottom: getProportionateScreenHeight(5)),
                child: Container(
                  height: getProportionateScreenWidth(50),
                  width: getProportionateScreenWidth(50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        image: journey.images.isEmpty
                            ? AssetImage("assets/images/Travel-WordPress-Themes.jpg.webp")
                            : NetworkImage(journey.images[0].imageLink), fit: BoxFit.cover,),
                  ),
                ),
              ),
              SizedBox(
                width: getProportionateScreenWidth(10),
              ),
              Container(
                width: getProportionateScreenWidth(150),
                child: RichText(
                    text: TextSpan(children: [
                  TextSpan(
                    style: TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                        text: "${journey.departure}"
                            " - "
                            "${journey.destination}\n",
                        style: TextStyle(
                            fontSize: getProportionateScreenWidth(15),
                            fontWeight: FontWeight.bold,
                            color: kColorPalette4),
                      ),
                      TextSpan(
                        text:
                            "${journey.startDate.day}/${journey.startDate.month}/${journey.startDate.year}"
                            "- "
                            "${journey.endDate.day}/${journey.endDate.month}/${journey.endDate.year}",
                        style: TextStyle(
                            color: kColorPalette4.withOpacity(0.5),
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  )
                ])),
              ),
              SizedBox(width: getProportionateScreenWidth(10)),
              if (journey.status == "In progress")
                Container(
                  width: getProportionateScreenWidth(90),
                  height: getProportionateScreenWidth(30),
                  decoration: BoxDecoration(
                    color: Colors.green[300],
                    border: Border.all(
                      color: Colors.green[300],
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Center(
                    child: Text("In progress",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[850])),
                  ),
                ),
              if (journey.status == "Finished")
                Container(
                  width: getProportionateScreenWidth(90),
                  height: getProportionateScreenWidth(30),
                  decoration: BoxDecoration(
                    color: Colors.red[300],
                    border: Border.all(
                      color: Colors.red[300],
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Center(
                    child: Text("Finished",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[850])),
                  ),
                ),
              if (journey.status == "Upcoming")
                Container(
                  width: getProportionateScreenWidth(90),
                  height: getProportionateScreenWidth(30),
                  decoration: BoxDecoration(
                    color: Colors.blue[200],
                    border: Border.all(
                      color: Colors.blue[200],
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Center(
                      child: Text(
                    "Upcoming",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.grey[850]),
                  )),
                ),
            ],
          )),
    );
  }
}
