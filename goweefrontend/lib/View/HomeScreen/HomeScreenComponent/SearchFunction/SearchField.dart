import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/View/Detail/DetailJourney.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/SearchFunction/SuggestCard.dart';
import 'package:http/http.dart' as http;
import '../../../../Constants.dart';
import '../../../../SizeConfig.dart';

class SearchField extends StatefulWidget {
  const SearchField({Key key}) : super(key: key);

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  var journeys = new List<Journey>();

  fetchSearchJourney(String keySearch) async {
    final response =
        await http.get(Uri.parse("$baseUrl/journeys/search=$keySearch"));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List data = jsonDecode(response.body);
      List idList = [];
      for (var i in data) {
        idList.add(i["journeyId"]);
      }
      for (var i in idList) {
        var res = await http.get(
          Uri.parse("$baseUrl/journeys/$i"),
        );
        var data = jsonDecode(res.body);
        Journey userJourney = new Journey.fromJson(data);
        setState(() {
          journeys.add(userJourney);
        });
      }
      // Iterable list = json.decode(response.body);
      // print( list.map((model) => Journey.fromJson(model).journeyId).toList());
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  @override
  void initState() {
    super.initState();
  }

  final _searchTextFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        width: SizeConfig.screenWidth * 0.88,
        decoration: BoxDecoration(
          color: kColorPalette2,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          children: [
            TextField(
              onChanged: (value) {
                if (value != "") {
                  fetchSearchJourney(value);
                }
                journeys = new List<Journey>();
                setState(() {});
              },
              style: TextStyle(
                color: kColorPalette4
              ),
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(10),
                      vertical: getProportionateScreenHeight(20)),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  hintText: "Search product",
                  hintStyle: TextStyle(
                    color: kColorPalette4
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    color: kColorPalette4,
                  )),
            ),
            Container(
                height: journeys.length != 0
                    ? getProportionateScreenHeight(200)
                    : null,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      ...List.generate(journeys.length, (index) {
                        return Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: getProportionateScreenWidth(10)),
                              child: SuggestCard(
                                  journey: journeys[index],
                                  press: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailsJourneyScreen(
                                                    journey: journeys[index])),
                                      )),
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(10),
                            )
                          ],
                        );
                      }),
                    ],
                  ),
                ))
          ],
        ));
  }
}
