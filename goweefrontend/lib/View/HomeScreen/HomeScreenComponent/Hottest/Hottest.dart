
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';

import 'package:goweefrontend/View/Detail/DetailJourney.dart';


import '../../../../Services.dart';
import '../../../../SizeConfig.dart';
import 'HottestCard.dart';

class Hottest extends StatefulWidget {

  static void restartDataHottest(BuildContext context) {
    context.findAncestorStateOfType<_HottestState>()._getHotJourney();
  }

  @override
  _HottestState createState() => _HottestState();
}




class _HottestState extends State<Hottest> {


  var hottestJourney = new List<Journey>();
  void _getHotJourney() {
    JourneyAPI.getJourney().then((response) {
      if (this.mounted) {
        setState(() {
          Iterable list = json.decode(response.body);
          hottestJourney = list.map((model) => Journey.fromJson(model)).toList();
          hottestJourney.sort((b, a) => a.likes.length.compareTo(b.likes.length));
        });
      }
    });
  }

  initState() {
    super.initState();
    _getHotJourney();
  }

  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getWidth(0.02)),
          child: Padding(
            padding:EdgeInsets.only(right: getWidth(0.5)),
            child: Text("Hottest", style: TextStyle(fontSize: getWidth(0.1), fontWeight: FontWeight.bold,
              shadows: <Shadow>[
              Shadow(
                // offset: Offset(10.0, 10.0),
                blurRadius: 2.0,
                color: Colors.grey,
              ),
            ],),),
          )
        ),
        SizedBox(height: getHeight(0.02)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(5, (index){
                if(hottestJourney.length !=0) {
                  return HottestCard(
                      journey: hottestJourney[index],
                      press: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    DetailsJourneyScreen(
                                        journey: hottestJourney[index])),
                          );
                      });
                } return CircularProgressIndicator();
                }),
            ],
          ),
        )
      ],
    );
  }
}
