import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/LikeButton.dart';

import '../../../../SizeConfig.dart';




class HottestCard extends StatelessWidget {
  const HottestCard({
    Key key,
    @required this.journey,
    @required this.press,
  }) : super(key: key);

  final Journey journey;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getWidth(0.05)),
      child: GestureDetector(
        onTap: press,
        child: Container(
          width: getWidth(0.88),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
              image: journey.images.isEmpty
                  ? AssetImage("assets/images/Travel-WordPress-Themes.jpg.webp")
                  : NetworkImage(journey.images[0].imageLink), fit: BoxFit.cover,
            ),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF343434).withOpacity(0.4),
                Color(0xFF343434).withOpacity(0.15),
              ],
            ),
          ),
          // height: getWidth(0.4),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Stack(
              children: [
                ClipRect(
                  child: BackdropFilter(
                    filter: new ImageFilter.blur(sigmaX: 0.1, sigmaY: 0.1),
                    child: Container(
                      decoration: new BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        colors: [
                          Colors.black.withOpacity(0.5),
                          Colors.black.withOpacity(0.4),
                          Colors.black.withOpacity(0.2),
                          Colors.grey.shade200.withOpacity(0.1),
                          Colors.grey.shade200.withOpacity(0.2),
                          Colors.black.withOpacity(0.4),
                          Colors.black.withOpacity(0.5)],),),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: getWidth(0.04),
                          vertical: getHeight(0.0125),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: getWidth(0.58),
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                        backgroundImage: NetworkImage(journey.author.userAvatar.imageLink),
                                      ),
                                      SizedBox(
                                        width: getWidth(0.02),
                                      ),
                                      Text.rich(
                                        TextSpan(
                                          style: TextStyle(color: Colors.white),
                                          children: [
                                            TextSpan(
                                              text: "${journey.author.firstName+ " " + journey.author.lastName}\n",
                                              style: TextStyle(
                                                color: kColorPalette4,
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            TextSpan(text:
                                            "${journey.startDate.day}/${journey.startDate.month}/${journey.startDate.year}"
                                                "- "
                                                "${journey.endDate.day}/${journey.endDate.month}/${journey.endDate.year}",
                                            style: TextStyle(color: kColorPalette4,))
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                                if(journey.status == "In progress")
                                  Container(
                                    width: getWidth(0.22),
                                    height: getWidth(0.08),
                                    decoration: kBoxDecorationInProgress,
                                    child: Center(child: Text("In progress",style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey[850])),
                                  ),),
                                if(journey.status == "Finished")
                                  Container(
                                    width: getWidth(0.22),
                                    height: getWidth(0.08),
                                    decoration: kBoxDecorationFinished,
                                    child: Center(child: Text("Finished",style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey[850])),
                                  ),),
                                if(journey.status == "Upcoming")
                                  Container(
                                    width: getWidth(0.22),
                                    height: getWidth(0.08),
                                    decoration: kBoxDecorationUpcoming,
                                    child: Center(child: Text("Upcoming",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey[850]),)),
                                  ),
                              ],
                            ),
                            // Title Journey
                            Padding(
                              padding: EdgeInsets.only(top: getHeight(0.1)),
                              child: Row(
                                children: [
                                  Flexible(
                                    child: Container(
                                      width: getWidth(0.8),
                                      child: Text(
                                          "${journey.departure}" " - " "${journey.destination}",
                                          style: TextStyle(color: kColorPalette4,
                                                          fontSize: getWidth(0.05),
                                                          fontWeight: FontWeight.bold),
                                          ),
                                    ),
                                  ),
                                  LikeButtonJourney(journey: journey)
                                ],
                              ),
                            ),

                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}