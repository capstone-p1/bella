

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:goweefrontend/SizeConfig.dart';

import '../../../../Model/Journey.dart';





class CarouselJourney extends StatefulWidget {
  const CarouselJourney({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final Journey journey;
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselJourney> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = widget.journey.images.map((item) => Container(
      child: Container(
        height: getWidth(0.5),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(item.imageLink),fit: BoxFit.cover
          ),
        ),
      ),
    )).toList();
    return Column(
       children: [
         Stack(
           alignment: Alignment.topRight,
           children: [
             CarouselSlider(
               items: imageSliders,
               options: CarouselOptions(
                   height: getWidth(0.5),
                   autoPlay: false,
                   enlargeCenterPage: true,
                   enableInfiniteScroll: false,
                   viewportFraction: 1.0,
                   aspectRatio: 2.0,
                   onPageChanged: (index, reason) {
                     setState(() {
                       _current = index;
                     });
                   }
               ),
             ),
           ],
         ),
         
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: widget.journey.images.map((url) {
             int index = widget.journey.images.indexOf(url);
             return Container(
               width: getWidth(0.02),
               height: getWidth(0.02),
               margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
               decoration: BoxDecoration(
                 shape: BoxShape.circle,
                 color: _current == index
                     ? Color.fromRGBO(0, 0, 0, 0.9)
                     : Color.fromRGBO(0, 0, 0, 0.4),
               ),
             ) ;
           }).toList(),
         ),
       ]
      );
  }
}



