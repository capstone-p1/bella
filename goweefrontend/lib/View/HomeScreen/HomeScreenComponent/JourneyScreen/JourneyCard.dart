import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:goweefrontend/Constants.dart';
import 'package:goweefrontend/Model/Journey.dart';
import 'package:goweefrontend/Model/Place.dart';

import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/JourneyScreen/CarouselJourney.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/LikeButton.dart';

import '../../../../Services.dart';
import '../../../../SizeConfig.dart';

class JourneyCard extends StatefulWidget {
  const JourneyCard({
    Key key,
    @required this.journey,
    @required this.press,
  }) : super(key: key);
  final Journey journey;
  final GestureTapCallback press;

  @override
  _JourneyCardState createState() => _JourneyCardState();
}

class _JourneyCardState extends State<JourneyCard> {
  var places = new List<Place>();

  void _getPlace() {
    PlaceAPI.getPlace().then((response) {
      if (this.mounted) {
        setState(() {
          Iterable list = json.decode(response.body);
          places = list.map((model) => Place.fromJson(model)).toList();
          _getPrice();
        });
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getPlace();
  }

  List placeId = [];
  List price = [];
  var sum = 0;
  void _getPrice() {
    List.generate(widget.journey.milestones.length, (index) {
      List.generate(widget.journey.milestones[index].places.length, (index2) {
        placeId.add(widget.journey.milestones[index].places[index2].placeId);
      });
    });

    List.generate(places.length, (index) {
      if (placeId.contains(places[index].placeId)) {
        if (places[index].avgPrice != null) {
          price.add(places[index].avgPrice);
        }
        price.add(0);
      }
    });
    for (var i = 0; i < price.length; i++) {
      sum += price[i];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: GestureDetector(
        onTap: widget.press,
        child: Container(
          decoration: kJourneyCardDecoration,
          width: getWidth(0.95),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // name  + avatar
              widget.journey.images.length > 1
                  ? CarouselJourney(
                      journey: widget.journey,
                    )
                  : Center(
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: [
                          Container(
                            height: getWidth(0.5),
                            width: getWidth(0.9),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: widget.journey.images.isEmpty
                                        ? AssetImage(
                                            "assets/images/Travel-WordPress-Themes.jpg.webp")
                                        : NetworkImage(
                                            widget.journey.images[0].imageLink),
                                    fit: BoxFit.cover)),
                          ),
                        ],
                      ),
                    ),
              // Journey destination and Status
              Padding(
                  padding: EdgeInsets.only(
                      left: getWidth(0.02), top: getHeight(0.01)),
                  child: SizedBox(
                    child: Row(
                      children: [
                        SizedBox(
                          width: getWidth(0.7),
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(
                              style: TextStyle(color: Colors.lightBlueAccent),
                              children: [
                                TextSpan(
                                  text: "${widget.journey.departure}"
                                      " - "
                                      "${widget.journey.destination}\n",
                                  style: TextStyle(
                                    fontSize: getWidth(0.05),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      "${widget.journey.startDate.day}/${widget.journey.startDate.month}/${widget.journey.startDate.year}"
                                      "- "
                                      "${widget.journey.endDate.day}/${widget.journey.endDate.month}/${widget.journey.endDate.year}",
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.5),
                                      fontSize: getWidth(0.035),
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                          ])),
                        ),
                        if (widget.journey.status == "In progress")
                          Container(
                            width: getWidth(0.2),
                            height: getWidth(0.075),
                            decoration: kBoxDecorationInProgress,
                            child: Center(
                              child: Text("In progress",
                                  style: TextStyle(
                                      fontSize: getWidth(0.03),
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[850])),
                            ),
                          ),
                        if (widget.journey.status == "Finished")
                          Container(
                            width: getWidth(0.2),
                            height: getWidth(0.075),
                            decoration: kBoxDecorationFinished,
                            child: Center(
                              child: Text("Finished",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[850])),
                            ),
                          ),
                        if (widget.journey.status == "Upcoming")
                          Container(
                            width: getWidth(0.2),
                            height: getWidth(0.075),
                            decoration: kBoxDecorationUpcoming,
                            child: Center(
                                child: Text(
                              "Upcoming",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[850]),
                            )),
                          ),
                      ],
                    ),
                  )),
              SizedBox(
                height: 5,
              ),
              // Padding(
              //   padding: EdgeInsets.only(left: 10, right: 15),
              //   child: Text(
              //     "Day la 1 chiec title cho Journey",
              //     style: TextStyle(
              //         color: Colors.black,
              //         fontSize: 20,
              //         fontWeight: FontWeight.bold),
              //   ),
              // ),
              Row(children: <Widget>[
                LikeButtonJourney(
                  journey: widget.journey,
                ),
                widget.journey.likes.length > 0
                    ? Text("${widget.journey.likes.length} " "likes",
                        style: TextStyle(fontSize: getWidth(0.035)))
                    : Text(
                        "0 like",
                        style: TextStyle(fontSize: getWidth(0.035)),
                      ),
                SizedBox(width: getWidth(0.02)),
                Row(
                  children: [
                    SvgPicture.asset(
                      "assets/images/comment_icon.svg",
                      color: Colors.black,
                      width: getWidth(0.06),
                    ),
                    Text(
                        " ${widget.journey.comments.length.toString()} Comment ",
                        style: TextStyle(fontSize: getWidth(0.035)))
                  ],
                ),
              ]),

              SizedBox(height: getHeight(0.02)),
            ],
          ),
        ),
      ),
    );
  }
}
