import 'package:flutter/material.dart';
import 'package:goweefrontend/Model/Journey.dart';

import 'package:goweefrontend/View/Detail/DetailJourney.dart';

import '../../../../SizeConfig.dart';
import 'JourneyCard.dart';

class JourneyHomePage extends StatefulWidget {
  const JourneyHomePage({
    Key key,
    @required this.journey,
  }) : super(key: key);
  final List journey;

  static void restartDataBody(BuildContext context) {
    context.findAncestorStateOfType<_JourneyHomePageState>().loadMore();
  }

  @override
  _JourneyHomePageState createState() => _JourneyHomePageState();
}

class _JourneyHomePageState extends State<JourneyHomePage> {
  List journeyList = List<Journey>();
  int numJourney = 3;
  bool _isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isLoading = true;
    List.generate(
        numJourney, (index) => journeyList.add(widget.journey[index]));
    controller = new ScrollController()..addListener(_scrollListener);
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      print("get more data");
      loadMore();
    }
  }

  ScrollController controller;
  void loadMore() {
    numJourney = journeyList.length + 2;
    journeyList = [];
    List.generate(
        numJourney, (index) => journeyList.add(widget.journey[index]));
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHeight(0.8),
      child: SingleChildScrollView(
        controller: controller,
        child: Column(
          children: [
            SizedBox(height: getHeight(0.01)),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Column(
                children: [
                  ...List.generate(journeyList.length, (index) {
                    return Container(
                      child: Column(children: [
                        JourneyCard(
                            journey: journeyList[index],
                            press: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailsJourneyScreen(
                                        journey: journeyList[index])),
                              );
                            }),
                        SizedBox(height: getHeight(0.02)),
                      ]),
                    );
                  }),
                ],
              ),
            ),
            _isLoading ? Container() : CircularProgressIndicator()
          ],
        ),
      ),
    );
  }
}
