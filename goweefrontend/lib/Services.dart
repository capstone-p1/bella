import 'package:http/http.dart' as http;
import 'dart:async';

import 'Constants.dart';

class JourneyAPI {
  static Future getJourney() {
    var url = baseUrl + "/journeys";
    return http.get(Uri.parse(url));
  }
}

class JourneyAPIbyId {
  static Future getJourneyById(int id) {
    var url = baseUrl + "/journeys/$id";
    return http.get(Uri.parse(url));
  }
}

class UserAPI {
  static Future getUser() {
    var url = baseUrl + "/users";
    return http.get(Uri.parse(url));
  }
}

class MilestoneAPI {
  static Future getMilestone() {
    var url = baseUrl + "/milestones";
    return http.get(Uri.parse(url));
  }
}

class PlaceAPI {
  static Future getPlace() {
    var url = baseUrl + "/places";
    return http.get(Uri.parse(url));
  }
}

class LikeAPI {
  static Future getLike() {
    var url = baseUrl + "/likes";
    return http.get(Uri.parse(url));
  }
}

class CommentAPI {
  static Future getComment() {
    var url = baseUrl + "/comments";
    return http.get(Uri.parse(url));
  }

  static Future getJourneyByID(int id) {
    var url = baseUrl + "/journeys/$id";
    return http.get(Uri.parse(url));
  }
}
