import 'package:flutter/material.dart';

import 'SizeConfig.dart';

import 'dart:io' show Platform;

const kPrimaryColor = Color(0xFFFF7643);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

// https://socketing-gowee-1.herokuapp.com
final socketUrl = Platform.isIOS ? 'http://localhost:5000' : 'http://10.0.2.2:5000';
// final baseUrl =
    // Platform.isIOS ? 'http://localhost:3000' : 'http://10.0.2.2:3000';
// final socketUrl = "https://socketing-gowee-1.herokuapp.com" ;
final baseUrl = "https://goweepj.herokuapp.com";

//LOGIN
final kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: 'OpenSans',
);

final kText = TextStyle(
    color: Colors.black, fontFamily: 'OpenSans', fontWeight: FontWeight.bold);

final kLabelStyle = TextStyle(
  color: Color(0xffEEEEEE),
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);
final kColorFinished = Color(0xffbfbfbf);
final kColorUpComing = Color(0xff4da6ff);
final kColorInProgress = Color(0xffff6666);
final kBackgroundColor = Color(0xff334257);
final kColorPalette2 = Color(0xff476072);
final kColorPalette3 = Color(0xff548CA8);
final kColorPalette4 = Color(0xffEEEEEE);

final kBoxDecorationView = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: kColorPalette2,
    boxShadow: [
      BoxShadow(offset: Offset(0, 8), blurRadius: 5, color: Colors.grey[300]),
    ]);

final kBoxDecorationStyle = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: Colors.white,
    boxShadow: [
      BoxShadow(offset: Offset(0, 8), blurRadius: 5, color: Colors.grey[300]),
    ]);

final kBoxDecorationInProgress = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: kColorInProgress,
    boxShadow: [
      BoxShadow(offset: Offset(0, 8), blurRadius: 5, color: Colors.grey[300]),
    ]);

final kBoxDecorationFinished = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: kColorFinished,
    boxShadow: [
      BoxShadow(offset: Offset(0, 8), blurRadius: 5, color: Colors.grey[300]),
    ]);

final kBoxDecorationUpcoming = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: kColorUpComing,
    boxShadow: [
      BoxShadow(offset: Offset(0, 8), blurRadius: 5, color: Colors.grey[300]),
    ]);

final kJourneyCardDecoration = BoxDecoration(color: Colors.white, boxShadow: [
  BoxShadow(offset: Offset(0, 8), blurRadius: 5, color: Colors.grey[300]),
]);

final kFormFieldDecoration = BoxDecoration(
  borderRadius: BorderRadius.circular(15),
  color: kColorPalette2,
);

final kBoxDecorationFollow = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: kColorPalette2,
    boxShadow: [
      BoxShadow(offset: Offset(0, 8), blurRadius: 5, color: Colors.grey[300]),
    ]);

final kBoxDecorationUnfollow = BoxDecoration(
  borderRadius: BorderRadius.circular(15),
  color: Colors.grey[300],
);
