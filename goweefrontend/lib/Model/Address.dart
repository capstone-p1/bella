class Address{
  int addressId;
  String addressLine;
  String city;
  String country;
  double lat;
  double long;

  Address({this.addressId, this.addressLine, this.city, this.country,this.lat,this.long});


  Address.noID(this.addressLine, this.city, this.country);

  Address.fromFormattedAddress(String formattedAddress) {
    if (formattedAddress != null){
      var strArray = formattedAddress.split(',');
      print('Splitting array $strArray');
      var addressLine = '';
      for( var i = 0; i < strArray.length - 2; i++){
        addressLine += strArray[i];
        if (i != strArray.length - 3){
          addressLine += ',';
        }
      }
      this.addressLine = addressLine;
      this.country = strArray[strArray.length - 1].trim();
      this.city = strArray[strArray.length - 2].trim();
    }
  }

  @override
  String toString() {
    return '{"addressLine": "$addressLine", "city": "$city", "country": "$country"}';
  }

  factory Address.fromJson(Map<String, dynamic> parsedJson) {
    return new Address(
        addressId :parsedJson['addressID'],
        addressLine : parsedJson['addressLine'],
        city :parsedJson['city'],
        country: parsedJson['country'],
        lat : parsedJson['la'] == null ? double.parse("1.1") : double.parse(parsedJson['la']) ,
        long :parsedJson['long'] == null ? double.parse("1.1") : double.parse(parsedJson['long'])
    );

  }

}