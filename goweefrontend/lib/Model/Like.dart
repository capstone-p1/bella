class Like {
  int likeId;

  Like({
    this.likeId,
  });

  factory Like.fromJson(Map<String, dynamic> parsedJson) {
    return new Like(
      likeId: parsedJson['likeId'],
    );
  }
}
