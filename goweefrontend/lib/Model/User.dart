

import 'package:goweefrontend/Model/Image.dart';

class User {
  int userId;
  String firstName;
  String lastName;
  String email;
  String password;
  int age;
  String phoneNumber;
  String country;
  Image userAvatar;

  User({this.userId, this.firstName, this.lastName, this.email, this.password, this.age,
      this.phoneNumber, this.country,this.userAvatar});
//Address address

  factory User.fromJson(Map<String, dynamic> parsedJson) {

    return new User(
        userId: parsedJson['userId'],
        firstName: parsedJson['firstName'],
        lastName: parsedJson['lastName'],
        email: parsedJson['email'],
        password: parsedJson['password'],
        age: parsedJson['age'],
        phoneNumber: parsedJson['phoneNumber'],
        // userAvatar: Image.fromJson(parsedJson['userAvatar']),

        userAvatar: parsedJson['userAvatar'] != null ?
        Image.fromJson(parsedJson['userAvatar'])
            : Image(imageLink:"http://lofrev.net/wp-content/photos/2017/05/user_black_logo.jpg"),
    );
  }

}

