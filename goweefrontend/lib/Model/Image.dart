class Image {
  int imageId;
  String imageLink;

  Image({this.imageId, this.imageLink});


  Image.noID(this.imageLink);


  @override
  String toString() {
    //return '{"milestoneName": "$milestoneName", "images": $images, "places": $places}';
    return '{"imageLink": "$imageLink"}';
  }

  factory Image.fromJson(Map<String, dynamic> parsedJson) {
    return new Image(
        imageId: parsedJson['imageId'], imageLink: parsedJson['imageLink']);
  }
}
