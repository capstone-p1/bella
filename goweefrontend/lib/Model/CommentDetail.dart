import 'package:goweefrontend/Model/User.dart';

class CommentDetail {
  int cmtId;
  String content;
  User author;

  CommentDetail({this.cmtId, this.content, this.author});

  factory CommentDetail.fromJson(Map<String, dynamic> parsedJson) {
    return new CommentDetail(
      cmtId: parsedJson['cmtId'],
      content: parsedJson['content'],
      author: User.fromJson(parsedJson['author']),
    );
  }
}
