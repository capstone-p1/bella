import 'package:goweefrontend/Model/Comment.dart';
import 'package:goweefrontend/Model/Image.dart';
import 'package:goweefrontend/Model/Milestones.dart';
import 'package:goweefrontend/Model/User.dart';

import 'Like.dart';
import 'Place.dart';

class Journey {
  int journeyId;
  User author;
  String departure;

  String destination;
  DateTime startDate;
  DateTime endDate;
  String status;
  List<User> companion;
  String description;
  List<Like> likes; //array
  List<Image> images;
  List<Milestones> milestones;
  List<Comment> comments;
  List<Place> places;

  Journey(
      {this.journeyId,
      this.milestones,
      this.departure,
      this.images,
      this.destination,
      this.likes,
      this.comments,
      this.places,
      this.startDate,
      this.endDate,
      this.status,
      this.companion,
      this.description,
      this.author});

  factory Journey.fromJson(Map<String, dynamic> parsedJson) {
    // ignore: non_constant_identifier_names
    var startDateFromJson = parsedJson['startDate'];
    DateTime startDate = DateTime.parse(startDateFromJson.toString());
    var endDateFromJson = parsedJson['endDate'];
    DateTime endDate = DateTime.parse(endDateFromJson.toString());

    var listLike = parsedJson['likes'] as List;
    List<Like> likesList = listLike.map((i) => Like.fromJson(i)).toList();

    return new Journey(
      journeyId: parsedJson['journeyId'],
      likes: likesList,
      images: parsedJson["images"] != null
          ? new List<Image>.from(
              parsedJson["images"].map((x) => Image.fromJson(x)))
          : List<Image>(),
      milestones: parsedJson["milestones"] != null
          ? new List<Milestones>.from(
              parsedJson["milestones"].map((x) => Milestones.fromJson(x)))
          : List<Milestones>(),
      places: parsedJson["places"] != null
          ? new List<Place>.from(
              parsedJson["places"].map((x) => Milestones.fromJson(x)))
          : List<Place>(),
      companion: parsedJson["companions"] != null
          ? new List<User>.from(
              parsedJson["companions"].map((x) => User.fromJson(x)))
          : List<User>(),
      author: User.fromJson(parsedJson['author']),
      departure: parsedJson['departure'],
      destination: parsedJson['destination'],
      startDate: startDate,
      endDate: endDate,
      description: parsedJson['description'],
      status: parsedJson['status'],
      comments: parsedJson["comments"] != null
          ? new List<Comment>.from(
              parsedJson["comments"].map((x) => Comment.fromJson(x)))
          : List<Comment>(),
    );
  }
}

// Just for test
