import 'dart:async';

import 'package:goweefrontend/Model/Address.dart';
import 'package:http/http.dart';

import 'Image.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class Place {
  int placeId;
  String placeName;
  String formattedAddress;
  Address address;
  int avgPrice;
  List<Image>images;
  double lat;
  double lng;

  Place({this.placeId, this.placeName, this.avgPrice, this.images, this.address});

  Place.fromGoogleAPI({this.placeName, this.formattedAddress, this.lat, this.lng});

  Place.noID(this.placeName, this.address);

  factory Place.fromJson(Map<String, dynamic> parsedJson) {
    return new Place(
      placeId: parsedJson['placeId'],
      placeName: parsedJson['placeName'],
      avgPrice: parsedJson['avgPrice'],
      images: parsedJson["images"] != null ?
      new List<Image>.from(parsedJson["images"].map((x) => Image.fromJson(x)))
          : List<Image>(),
      address: Address.fromJson(parsedJson['address']),
    );
  }

  static Place fromGoogleAPItoJson(Map<String, dynamic> map) {
    return Place.fromGoogleAPI(
      placeName: map['name'],
      formattedAddress: map['formatted_address'],
      lat: map['geometry']['location']['lat'],
      lng: map['geometry']['location']['lng'],
    );
  }

  static List<Place> parseLocationList(map) {
    var list = map['results'] as List;
    return list.map((movie) => Place.fromGoogleAPItoJson(movie)).toList();
  }

  @override
  String toString() {
    Address returnAddress = new Address.fromFormattedAddress(formattedAddress);
    // return '{"placeName": "$placeName", "formattedAddress": "$formattedAddress", "address": $returnAddress, "avgPrice": $avgPrice, "images": $images ,"lat": "$lat", "lng": "$lng"}';
    return '{"placeName": "$placeName", "address": $address, "avgPrice": $avgPrice, "images": $images ,"lat": "$lat", "lng": "$lng"}';
  }
}

class GoogleMapApi{
  static const apiKey = 'AIzaSyCD34C1fD6wJOEDCo5oD2TzLVES9mLAbGY';
  static Future<List<Place>> search(String query) async {
    String url =
        "https://maps.googleapis.com/maps/api/place/textsearch/json?query=$query&key=$apiKey";
    Response response = await http.get(Uri.parse(url));

    print(response.body);
    /*
    flutter: {
      "error_message" : "You must enable Billing on the Google Cloud Project at https://console.cloud.google.com/project/_/billing/enable Learn more at https://developers.google.com/maps/gmp-get-started",
    "html_attributions" : [],
    "results" : [],
    "status" : "REQUEST_DENIED"
    }
    */
    return Place.parseLocationList(json.decode(response.body));
  }
  static StreamController<Place> locationController = StreamController<Place>.broadcast();
  static locationSelected(Place place) {
    locationController.sink.add(place);
  }
}