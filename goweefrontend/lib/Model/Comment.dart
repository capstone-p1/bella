class Comment {
  int cmtId;
  String content;

  Comment({this.cmtId, this.content});

  factory Comment.fromJson(Map<String, dynamic> parsedJson) {
    return new Comment(
        cmtId: parsedJson['cmtId'],
        content: parsedJson['content']
    );
  }
}