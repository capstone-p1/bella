import 'User.dart';

class LoggedUser {
  User user;
  String token;

  LoggedUser({this.user, this.token});

  factory LoggedUser.fromJson(Map<String, dynamic> parsedJson) {
    return new LoggedUser(
        user: User.fromJson(parsedJson['user']), token: parsedJson['token']);
  }

  @override
  String toString() {
    return '{"user": "$user", "token": "$token"}';
  }
}
