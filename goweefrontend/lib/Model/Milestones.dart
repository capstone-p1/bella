import 'package:goweefrontend/Model/Place.dart';

import 'Image.dart';

class Milestones {
  int milestoneId;
  String milestoneName;
  List<Image> images;
  List<Place> places;

  Milestones({this.milestoneId, this.milestoneName, this.images, this.places});

  Milestones.noID(this.milestoneName, this.images, this.places);

  static fromMilestoneIdJson(dynamic parsedJson) {
    return new Milestones(
        milestoneId: parsedJson['milestoneId'],
        milestoneName: parsedJson['milestoneName'],
        images: parsedJson["images"] != null
            ? new List<Image>.from(
                parsedJson["images"].map((x) => Image.fromJson(x)))
            : List<Image>(),
        places: parsedJson["places"] != null
            ? new List<Place>.from(
                parsedJson["places"].map((x) => Place.fromJson(x)))
            : List<Place>());
  }

  factory Milestones.fromJson(Map<String, dynamic> parsedJson) {
    return new Milestones(
      milestoneId: parsedJson['milestoneId'],
      milestoneName: parsedJson['milestoneName'],
      images: parsedJson["images"] != null
          ? new List<Image>.from(
              parsedJson["images"].map((x) => Image.fromJson(x)))
          : List<Image>(),
      // places: placeList
      places: parsedJson["places"] != null
          ? new List<Place>.from(
              parsedJson["places"].map((x) => Place.fromJson(x)))
          : List<Place>(),
    );
  }

  @override
  String toString() {
    return '{"milestoneName": "$milestoneName", "images": $images, "places": $places}';
  }
}
