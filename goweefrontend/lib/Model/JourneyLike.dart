class JourneyLike {
  int journeyId;

  String departure;

  String destination;
  DateTime startDate;
  DateTime endDate;
  String status;

  String description;



  JourneyLike({
    this.journeyId,

    this.departure,

    this.destination,

    this.startDate,
    this.endDate,
    this.status,

    this.description,

  });

  factory JourneyLike.fromJson(Map<String, dynamic> parsedJson) {
    // ignore: non_constant_identifier_names
    var StartDateFromJson = parsedJson['startDate'];
    DateTime startDate = DateTime.parse(StartDateFromJson.toString());
    var EndDateFromJson = parsedJson['endDate'];
    DateTime endDate = DateTime.parse(EndDateFromJson.toString());


    return new JourneyLike(
      journeyId: parsedJson['journeyId'],

      departure: parsedJson['departure'],
      destination: parsedJson['destination'],
      startDate: startDate,
      endDate: endDate,
      description: parsedJson['description'],
      status: parsedJson['status'],



    );
  }
}

// Just for test
