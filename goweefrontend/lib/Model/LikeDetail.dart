import 'JourneyLike.dart';
import 'User.dart';

class LikeDetail {
  int likeId;
  User author;
  JourneyLike journeyLike;

  LikeDetail({this.likeId, this.author, this.journeyLike});

  factory LikeDetail.fromJson(Map<String, dynamic> parsedJson) {
    return new LikeDetail(
        likeId: parsedJson['likeId'],
        author: User.fromJson(parsedJson['author']),
        journeyLike: JourneyLike.fromJson(parsedJson['journey']));
  }
}
