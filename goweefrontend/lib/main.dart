

import 'package:flutter/material.dart';
import 'package:goweefrontend/View/HomeScreen/HomeScreenComponent/SectionTitle.dart';

import 'Routes.dart';
import 'View/CustomBottomNavBar/CustomBottomNavBar.dart';

import 'package:firebase_core/firebase_core.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(RestartWidget(child:MyApp(),)
      );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gowee Demo',
      // home: SplashScreen(),
      // We use routeName so that we don't need to remember the name
      initialRoute: CustomBottomNavBar.routeName,
      routes: routes,
    );
  }
}
