import 'package:flutter/material.dart';
import 'package:goweefrontend/View/Detail/DetailJourney.dart';

import 'View/CustomBottomNavBar/CustomBottomNavBar.dart';
import 'View/HomeScreen/HomeScreen.dart';

final Map<String, WidgetBuilder> routes = {
  HomeScreen.routeName: (context) => HomeScreen(),
  CustomBottomNavBar.routeName: (context) => CustomBottomNavBar(),
  DetailsJourneyScreen.routeName: (context) => DetailsJourneyScreen(),
};
